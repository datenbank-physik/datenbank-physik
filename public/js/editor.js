//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

(function ($, undefined) {
    $.fn.getCursorPosition = function () {
        var el = $(this).get(0);
        var pos = 0;
        if ('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})($);

function insertAtCursor(myField, startValue, endValue) {
    var txt = myField.get(0);
    var start = txt.selectionStart;
    var end = txt.selectionEnd;
    var new_text =
        myField.val().slice(0, start)
        + startValue
        + myField.val().slice(start, end)
        + endValue
        + myField.val().slice(end);
    txt.value = new_text;
    txt.focus();
    txt.selectionStart = start+startValue.length;
    txt.selectionEnd = end+startValue.length;
}


function switchEditorPreview(self, render, textbox) {

    if(self.checked) {
        $.post( "/bbcode_preview", { content: textbox.val() })
        .done(function( data ) {
            render.html(data);
            if(self.checked) {
                render.show();
                textbox.hide();
            }
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        });
    } else {
        render.hide();
        textbox.show();
    }
}
