<?php

declare(strict_types = 1);

if (!file_exists('.git/hooks')) {
    echo '.git/hooks not found, skipping git pre-commit hooks installation';
    exit(0);
}

echo 'Installing git hooks';
copy('contrib/pre-commit', '.git/hooks/pre-commit');
copy('contrib/pre-push', '.git/hooks/pre-push');
chmod('.git/hooks/pre-commit', 0755);
chmod('.git/hooks/pre-push', 0755);
exit(0);
