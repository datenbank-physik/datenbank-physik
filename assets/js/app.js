//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.less');
require('../css/basic.less');
require('../css/form.less');
require('../css/header.less');
require('../css/button_container.less');
require('../css/preview_card.less');
require('../css/list_entry.less');
require('../css/label.less');
require('../css/tagify.scss');
require('../css/jstree.less');
require('../css/slick.less');
require('../css/tab.less');
require('../css/sign_selector.less');
require('../css/drop_down_menu.less');
require('../css/themes/default/style.less');
require('../css/delete_confirm.less');
require('../css/editor.less');
require('../css/comments.less');
require('../css/calendar.less');
require('../css/flatpickr.less');
require('../css/reservation.less');
require('../css/search.less');
require('../css/welcome.less');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
// create global $ and jQuery variables
global.$ = global.jQuery = $;

$(document).ready(function () {
    $('.switch :input').on('change', function (e) {
        var xhttp = new XMLHttpRequest();
        xhttp.open('POST', '/changeOption', true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xhttp.send(JSON.stringify({'listView': e.currentTarget.checked}));
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                location.reload();
            }
        };
    });
});
