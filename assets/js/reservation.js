//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

require('flatpickr');
import { German } from "flatpickr/dist/l10n/de.js"

import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';

var calendar_private;
var calendar_public;

document.addEventListener('DOMContentLoaded', function() {
    var calendarPrivateEl = document.getElementById('calendar_private');
    if(calendarPrivateEl != null){
        calendar_private = new Calendar(calendarPrivateEl, {
            plugins: [ dayGridPlugin, timeGridPlugin, listPlugin ],
            locale: 'de',
            header: {
                left: 'listWeek,dayGridMonth',
                center: 'title',
                right: 'prev,today,next'
            },
            buttonText: {
                month: 'Monat',
                week: 'Woche',
                today: 'Heute',
                prev: '<',
                next: '>'
            },
            firstDay: 1,
            events: existing_events_private,
            defaultView: 'listWeek',
        });
        calendar_private.render();
    }

    var calendarPublicEl = document.getElementById('calendar_public');
    if(calendarPublicEl != null) {
        calendar_public = new Calendar(calendarPublicEl, {
            plugins: [dayGridPlugin, timeGridPlugin, listPlugin],
            locale: 'de',
            header: {
                left: 'listWeek,dayGridMonth',
                center: 'title',
                right: 'prev,today,next'
            },
            buttonText: {
                month: 'Monat',
                week: 'Woche',
                today: 'Heute',
                prev: '<',
                next: '>'
            },
            firstDay: 1,
            events: existing_events_public
        });
        calendar_public.render();
    }

    $(".flatpickr").flatpickr({
        enableTime: true,
        dateFormat: "d.m.Y H:i",
        time_24hr: true,
        locale: German,
        onChange: function (selectedDates) {
            var experiments = $('.experiment_lead_time_warning').toArray();
            this.selectedDates = selectedDates;
            this.disableButton = false;
            experiments.forEach(function (experiment) {
                var selectedDate = Date.parse(this.selectedDates)
                if (isNaN(selectedDate)) {
                    selectedDate = 0;
                }
                var dateDiff = selectedDate - Date.now();
                var minutes = Math.floor(dateDiff / 60000);
                var prepTime = parseInt(experiment.dataset.preparationTime);
                if (isNaN(prepTime)) {
                    prepTime = 0;
                }
                if (minutes < prepTime) {
                    $(experiment).show();
                    this.disableButton = true;
                } else {
                    $(experiment).hide();
                }
            }, this);
            $('.bookbutton')[0].disabled = this.disableButton;
            if(this.disableButton) {
                $('#lead_time_warning').show();
            }
            else {
                $('#lead_time_warning').hide();
            }
        },
    });
});

$(document).ready(function () {
    $('#lesson').on('change', function (event) {
        if(this.value === 'new') {
            $(".flatpickr")[0]._flatpickr.clear();
            $('#extra_experiments').hide();
            $('#new_lesson_info').show();
        }
        else {
            var experiments = $('.experiment_lead_time_warning').toArray();
            var selected = $(this).find('option:selected');
            this.selectedDates = selected.data('lectureTime');
            experiments.forEach(function (experiment) {
                var dateDiff = Date.parse(this.selectedDates) - Date.now();
                var minutes = Math.floor(dateDiff / 60000);
                var prepTime = parseInt(experiment.dataset.preparationTime);
                if (isNaN(prepTime)) {
                    prepTime = 0;
                }
                if (minutes < prepTime) {
                    $(experiment).show();
                    this.disableButton = true;
                } else {
                    $(experiment).hide();
                }
            }, this);
            $('.bookbutton')[0].disabled = this.disableButton;
            if(this.disableButton) {
                $('#lead_time_warning').show();
            }
            else {
                $('#lead_time_warning').hide();
            }
            $('#extra_experiments').show();
            $('#new_lesson_info').hide();
        }
    });
});
