//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

require('../js/jstree.min.js');

$(document).ready(function () {
    var tree = $('#jstree');

    tree.jstree({
        'core' : {
            'check_callback' : true,
            'themes' : {'icons' : false}
        },
        'plugins': ['html_data', 'ui', 'wholerow'],
    });
    tree.on('changed.jstree', function (e,data) {
        e.preventDefault();
        if (data.action === "ready") {
            return;
        }
        $('#items').hide();
        $('.loading_icon').show();
        var url =  '/experiments/filterByCategory/' + data.node.id;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var jsonthing = JSON.parse(this.responseText);
                $('#items').html(jsonthing);
                $('.loading_icon').hide();
                $('#items').show();
                window.history.pushState('', data.node.id, '/experiment/category/' + data.node.id);
            }
        };

        xhttp.open('GET', url, true);
        xhttp.send();
        var tree = $('#jstree');
        if (tree.jstree('is_open', data.node.id)) {
            tree.jstree('close_node', data.node.id);
        } else {
            tree.jstree('close_all');
            tree.jstree('_open_to', data.node.id);
            tree.jstree('open_node', data.node.id);
        }

    });
    $('#jstree a').click(function (event) {
        event.preventDefault();
    });

    tree.jstree('_open_to', window.location.pathname.split("/").pop());
    tree.jstree('open_node', window.location.pathname.split("/").pop());
    tree.jstree('select_node', window.location.pathname.split("/").pop(), true);

    tree.show();
});
