//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

$(document).ready(addListernes());

function addListernes()
{
    var newComment = $('#new_comment');

    newComment.on('submit', function (e) {
        e.preventDefault();
        var data = $('#new_comment :input').serializeArray();
        var formData = new FormData();
        formData.append('message', data[0].value);
        formData.append('experiment', data[1].value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var jsonthing = JSON.parse(this.responseText);
                $('#comments').html(jsonthing);
                addListernes();
            }
        };
        xhttp.open('POST', '/addComment', true);
        xhttp.send(formData);
    });

    var replyComment = $('.reply_comment');

    replyComment.on('submit', function (e) {
        e.preventDefault();
        var data = $('#' + e.currentTarget.id + ' :input').serializeArray();
        var formData = new FormData();
        formData.append('message', data[0].value);
        formData.append('experiment', data[1].value);
        formData.append('parent', data[2].value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var jsonthing = JSON.parse(this.responseText);
                $('#comments').html(jsonthing);
                addListernes();
            }
        };
        xhttp.open('POST', '/addComment', true);
        xhttp.send(formData);
    });

    $('.open_reply_comment').on('click', function (e) {
        e.preventDefault();
        $('form[id=' + e.currentTarget.parentElement.id + ']').toggle();
    });

    $('.delete_comment').on('click', function (e) {
        e.preventDefault();
        debugger;
        var data = e.currentTarget.parentElement.id;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var jsonthing = JSON.parse(this.responseText);
                $('#comments').html(jsonthing);
                addListernes();
            }
        };
        xhttp.open('GET', '/deleteComment/' + data, true);
        xhttp.send();
    })
}
