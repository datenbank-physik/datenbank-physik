<?php

namespace App\Tests\Security;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

//see OWASP Testing Guide v4: Testing for SQL Injection (OTG-INPVAL-005)
//Queries partially taken from https://www.w3schools.com/sql/sql_injection.asp
class SQLInjectionsTest extends WebTestCase
{

    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testAgainstURLInjection()
    {
        $this->logIn(["ROLE_ADMIN"]);

        //Search Devices
        //SQL Statements should be ignored
        $crawler = $this->client->request('GET', '/search/devices/Testger%C3%A4t');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testger\\u00e4t<\\', $this->client->getResponse()->getContent());

        //We don't want SQL-Statements to be exectued so while 1=1 is true it should be still a search string and
        //return 0 results.
        $crawler = $this->client->request('GET', '/search/devices/Testgerät%20OR%201=1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Keine Ergebnisse', $this->client->getResponse()->getContent());

        //Search Experiments
        //SQL Statements should be ignored
        $crawler = $this->client->request('GET', '/search/experiments/Testexperiment');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $this->client->getResponse()->getContent());

        //We don't want SQL-Statements to be exectued so while 1=1 is true it should be still a search string and
        //return 0 results.
        $crawler = $this->client->request('GET', '/search/experiments/Testexperiment%2OR%201=1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Keine Ergebnisse', $this->client->getResponse()->getContent());

        //Experiments
        //SQL Statements should be ignored
        $crawler = $this->client->request('GET', '/experiment/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());

        $crawler = $this->client->request('GET', '/experiment/1%20AND%201=2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());

        //Devices
        //SQL Statements should be ignored
        $crawler = $this->client->request('GET', '/device/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        $crawler = $this->client->request('GET', '/device/1%20AND%201=2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Labels
        //SQL Statements should be ignored
        $crawler = $this->client->request('GET', '/labels/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('LabelzumEntfernen', $crawler->html());


        $crawler = $this->client->request('GET', '/labels/1%20AND%201=2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('LabelzumEntfernen', $crawler->html());
    }

    //Note: The Login-Forms are the only forms which could be SQLInjection-Attacked
    public function testAgainstFormInjection()
    {
        //LogIn-Form
        //invalid login is invalid (and always true statements don't make it true)
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = '" or ""="';
        $form['password'] = '" or ""="';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //Login-Widget
        //invalid login is invalid (and always true statements don't make it true)
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = '" or ""="';
        $form['password'] = '" or ""="';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
