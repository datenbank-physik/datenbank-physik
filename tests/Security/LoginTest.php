<?php

namespace App\Tests\Security;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    //test the login widget
    public function testLoginWidget()
    {
        //invalid login is invalid (invalid user)
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin12';
        $form['password'] = '1234567890';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //invalid login is invalid (empty user)
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = '';
        $form['password'] = '1234567890';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //invalid login is invalid (invalid password)
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '230';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //invalid login is invalid (empty password)
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //invalid login is invalid (everything empty)
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = '';
        $form['password'] = '';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302);

        //valid login is valid
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/', 302); //invalid login sends to login form
    }

    //test the separate login page
    public function testLoginPage()
    {
        //invalid login is invalid (invalid user)
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin12';
        $form['password'] = '1234567890';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //invalid login is invalid (empty user)
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = '';
        $form['password'] = '1234567890';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //invalid login is invalid (invalid password)
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '230';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //invalid login is invalid (empty password)
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form
        //$this->assertContains('Bitte füllen Sie dieses Feld aus', $crawler->html()); //invalid login sends to login form

        //invalid login is invalid (everything empty)
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = '';
        $form['password'] = '';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/login', 302); //invalid login sends to login form

        //valid login is valid
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/', 302);
        //we can now we our profil
        $this->client->request('GET', '/profil');
        $this->assertResponseIsSuccessful();

        //Now we can logout and can't reach our profile anymore…
        $this->client->request('GET', '/logout');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $this->client->request('GET', '/profil');
        //Device page should redirect to login
        $this->assertTrue(
            $this->client->getResponse()->isRedirect('/login')
        );
    }

    //when accessing a page that you can't access anonymously you get redirected to the login form
    public function testLoginAfterRedirect()
    {
        $this->client->restart();
        $this->client->request('GET', '/device');
        $this->assertResponseRedirects('/login', 302);
        $this->client->followRedirect();
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertSame(302, $this->client->getResponse()->getStatusCode());
        $this->assertContains('/device', $this->client->getResponse()->headers->get('Location'));
    }
}
