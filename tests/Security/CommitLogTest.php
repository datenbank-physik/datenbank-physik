<?php

namespace App\Tests\Security;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class CommitLogTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    //Experiments

    //Checks that Namechanges are logged together with Username and Date
    public function testCommitNameExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Geänderter Titel';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains(" Änderte den <strong>Namen</strong> von <strong>Testexperiment</strong> auf <strong>Geänderter Titel</strong>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //who made it date("Y.m.d")
    }

    //Checks that Descriptionchanges are logged together with Username and Date
    //Needs Approval
    public function testCommitDescriptionExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[description]'] = 'Geänderte Beschreibung';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains(" Änderte die <strong>Beschreibung</strong>", $crawler->html()); //Change that was made
        $this->assertContains("  <code><del>Di</del><ins>Geändert</ins>e Beschreibung</code>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
        $this->assertContains("Bestätigen", $crawler->html()); //when was change made
        $this->assertContains("Ablehnen", $crawler->html()); //when was change made

        //Now need to reject it, this has to work always
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();

        $link = $crawler
            ->filter('a:contains("Ablehnen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertContains("abgelehnt", $crawler->html());

        //everything should still be as it was
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());
    }

    //Checks that Commentchanges are logged together with Username and Date
    //Needs Approval
    public function testCommitCommentExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[comment]'] = 'Geänderter Kommentar';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte die <strong>Bemerkung</strong>", $crawler->html()); //Change that was made
        $this->assertContains("<code><ins>Geänderter </ins>Kommentar<del> dazu</del></code>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
        $this->assertContains("Bestätigen", $crawler->html()); //when was change made
        $this->assertContains("Ablehnen", $crawler->html()); //when was change made

        //Now need to reject it, this has to work always
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();

        $link = $crawler
            ->filter('a:contains("Ablehnen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertContains("abgelehnt", $crawler->html());

        //everything should still be as it was
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());
    }

    //Checks that Changes in Category are logged together with Username and Date
    public function testCommitCategoryExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[category]'] = '3';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte die <strong>Kategorie</strong>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made?
    }

    //Checks that Changes in Experimentnumber are logged together with Username and Date
    public function testCommitNumberExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[experimentNumber]'] = '11880';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte das Feld <strong>experimentNumber</strong>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in preparationTime are logged together with Username and Date
    public function testCommitpreparationTimeExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[preparationTime]'] = '11880';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte die <strong>Vorbereitungszeit</strong> von <strong>60</strong> auf <strong>11880", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Status are logged together with Username and Date
    public function testCommitStatusExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[status]'] = 'notavailable';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte den <strong>Status</strong> von <strong>available</strong> auf <strong>notavailable</strong>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in Labels are logged together with Username and Date
    public function testCommitLabelExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[labels]'] = '[{"value":"Neues Label 1"},{"value":"Neues Label 2"}]';

        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Hat", $crawler->html());
        $this->assertContains("die <strong>Labels</strong>            <span class=\"item_collection\">", $crawler->html());
        $this->assertContains("<a class=\"card label\" style=\"background-color: ;\" href=\"/labels/", $crawler->html());
        $this->assertContains("\">Neues Label 1</a>", $crawler->html());
        $this->assertContains("<span class=\"item_collection\">", $crawler->html());
        $this->assertContains("<a class=\"card label\" style=\"background-color: ;\" href=\"/labels/", $crawler->html());
        $this->assertContains("\">Neues Label 2</a>", $crawler->html());
        $this->assertContains("</span>", $crawler->html());
        $this->assertContains("hinzugefügt", $crawler->html());
        $this->assertContains("und             <span class=\"item_collection\">", $crawler->html());
        $this->assertContains("<a class=\"card label\" style=\"background-color: ;\" href=\"/labels/1\">LabelzumEntfernen</a>", $crawler->html());
        $this->assertContains("entfernt", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in ExecutionTime are logged together with Username and Date
    public function testCommitExecutionTimeExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[execution_time]'] = '11880';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte die <strong>Durchführungszeit</strong> von <strong>100</strong> auf <strong>11880</strong>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in Safety signs are logged together with Username and Date
    public function testCommitSafetysignsExperiment()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        // $form['experiment[safety_signs][]'] = 'W001';
        $form->setValues(['experiment[safety_signs]' => ['W001']]);

        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Hat", $crawler->html());
        $this->assertContains("<strong>Sicherheitszeichen</strong>            <span class=\"item_collection\">", $crawler->html());
        $this->assertContains(" <img src=\"/images/icons/safety_signs/ISO_7010_W001.svg\" height=\"35px\" style=\"vertical-align:middle;\">", $crawler->html());
        $this->assertContains("hinzugefügt", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Devices

    //Checks that Namechanges are logged together with Username and Date
    public function testCommitNameDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[name]'] = 'Kein Testgerät';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte den <strong>Namen</strong> von <strong>Testgerät</strong> auf <strong>Kein Testgerät</strong>", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Descriptionchanges are logged together with Username and Date
    //Needs Approval
    public function testCommitDescriptionDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[description]'] = 'Geänderte Beschreibung';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte die <strong>Beschreibung</strong>", $crawler->html()); //Change that was made
        $this->assertContains("<code><ins>Geänderte </ins>Beschreibung</code>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
        $this->assertContains("Bestätigen", $crawler->html()); //when was change made
        $this->assertContains("Ablehnen", $crawler->html()); //when was change made

        //Now need to reject it, this has to work always
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();

        $link = $crawler
            ->filter('a:contains("Ablehnen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertContains("abgelehnt", $crawler->html());

        //everything should still be as it was
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
    }

    //Checks that Comment changes are logged together with Username and Date
    //Needs Approval
    public function testCommitCommentDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[comment]'] = 'Geänderter Kommentar';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte die <strong>Bemerkung</strong>", $crawler->html()); //Change that was made
        $this->assertContains("<code><ins>Geänderter Kommentar</ins></code>", $crawler->html()); //Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
        $this->assertContains("Bestätigen", $crawler->html()); //when was change made
        $this->assertContains("Ablehnen", $crawler->html()); //when was change made

        //Now need to reject it, this has to work always
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();

        $link = $crawler
            ->filter('a:contains("Ablehnen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertContains("abgelehnt", $crawler->html());

        //everything should still be as it was
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
    }

    //Checks that Changes in InventoryNumber are logged together with Username and Date
    public function testCommitInventoryNumberDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[inventorynumber]'] = '11880';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Fügte die <strong>Inventarnummer</strong> <strong>11880</strong> hinzu", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes ind Odernumber are logged together with Username and Date
    public function testCommitOrderNumberDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[ordernumber]'] = '4711';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Fügte die <strong>Bestellnummer</strong> <strong>4711</strong> hinzu", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in Manufacture are logged together with Username and Date
    public function testCommitManufacturDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[supplier]'] = 'Musterfirma';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Fügte den <strong>Hersteller</strong> <strong>Musterfirma</strong> hinzu", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in IFP-Number are logged together with Username and Date
    public function testCommitIFPNumberDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[ifpcode]'] = 'IFPNUMMER';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Fügte die <strong>IFP Kennziffer</strong> <strong>IFPNUMMER</strong> hinzu", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in Labels are logged together with Username and Date
    public function testCommitLabelDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[labels]'] = '[{"value":"Neues Label 1"},{"value":"Neues Label 2"}]';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Hat", $crawler->html());
        $this->assertContains("die <strong>Labels</strong>            <span class=\"item_collection\">", $crawler->html());
        $this->assertContains("<a class=\"card label\" style=\"background-color: ;\" href=\"/labels/", $crawler->html());
        $this->assertContains("\">Neues Label 1</a>", $crawler->html());
        $this->assertContains("<span class=\"item_collection\">", $crawler->html());
        $this->assertContains("<a class=\"card label\" style=\"background-color: ;\" href=\"/labels/", $crawler->html());
        $this->assertContains("\">Neues Label 2</a>", $crawler->html());
        $this->assertContains("</span>", $crawler->html());
        $this->assertContains("hinzugefügt", $crawler->html());
        $this->assertContains("und             <span class=\"item_collection\">", $crawler->html());
        $this->assertContains("<a class=\"card label\" style=\"background-color: ;\" href=\"/labels/1\">LabelzumEntfernen</a>", $crawler->html());
        $this->assertContains("entfernt", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in TotalNumber are logged together with Username and Date
    public function testCommitTotalNumberDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[amount]'] = '11880';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte die <strong>Anzahl</strong> von <strong>13</strong> auf <strong>11880</strong>", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in loanedNumber are logged together with Username and Date
    public function testCommitLoanedNumberDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[loaned]'] = '4711';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte das Feld <strong>Ausgeliehen</strong> von <strong>0</strong> auf <strong>4711</strong>", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that Changes in repairNumber are logged together with Username and Date
    public function testCommitRepairNumberDevice()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[repair]'] = '4711';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Now check that changes have been recorded
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Änderte das Feld <strong>In Reparatur</strong> von <strong>0</strong> auf <strong>4711</strong>", $crawler->html());//Change that was made
        $this->assertContains("Admin", $crawler->html()); //who made it
        $this->assertContains(date("d.m.Y"), $crawler->html()); //when was change made
    }

    //Checks that in case of invalid Commits an Error is thrown
    public function testInvalidCommits()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/commits/1009/approve');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('GET', '/commits/1000/reject');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
