<?php

namespace App\Tests\Security;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class AccessRightsTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testAnonymous()
    {
        //Anonymous can view Front Page
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Anonymous can view version history
        $this->client->request('GET', '/version');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Anonymous can view experiments
        $this->client->request('GET', '/experiment');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Anonymous can view categorys
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Anonymous can view the description of a single experiment
        $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Anonymous can filter Experiments bei Category and show Categorys
        $this->client->request('GET', '/experiments/filterByCategory/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Anonymous can search for Experiments
        $this->client->request('GET', '/search/experiments/Testexperiment');
        $this->assertContains("Testexperiment", $this->client->getResponse()->getContent());


        //everything else: nope

        //Anonymous can search for Devices but no result will show up
        $this->client->request('GET', '/search/devices/Testger%C3%A4t');
        $this->assertContains("Keine Ergebnisse", $this->client->getResponse()->getContent());

        //No Create Experiment
        $this->client->request('GET', '/experiment/create');
        $this->assertResponseRedirects('/login', 302);

        //No Edit Experiment
        $this->client->request('GET', '/experiment/1/edit');
        $this->assertResponseRedirects('/login', 302);

        //No Delete Experiment
        $this->client->request('GET', '/experiment/1/delete');
        $this->assertResponseRedirects('/login', 302);

        //No Experiment Devices
        $this->client->request('GET', '/experiment/1/devices');
        $this->assertResponseRedirects('/login', 302);

        //No Experiment Comments
        $this->client->request('GET', '/experiment/1/comments');
        $this->assertResponseRedirects('/login', 302);

        //No New Comments
        $this->client->request('GET', '/addComment');
        $this->assertResponseRedirects('/login', 302);

        //No Comment Deletion
        $this->client->request('GET', '/deleteComment/1');
        $this->assertResponseRedirects('/login', 302);

        //No Overview about Comments
        $this->client->request('GET', '/comments');
        $this->assertResponseRedirects('/login', 302);

        //No Experiment Documents
        $this->client->request('GET', '/experiment/1/documents');
        $this->assertResponseRedirects('/login', 302);

        //No Experiment Commits
        $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseRedirects('/login', 302);

        //No Commit Approve
        $this->client->request('GET', '/commits/1/approve');
        $this->assertResponseRedirects('/login', 302);

        //No Commit Reject
        $this->client->request('GET', '/commits/1/reject');
        $this->assertResponseRedirects('/login', 302);

        //No Commit Overview
        $this->client->request('GET', '/commits');
        $this->assertResponseRedirects('/login', 302);

        //No overview about changes pending release
        $this->client->request('GET', '/releases');
        $this->assertResponseRedirects('/login', 302);

        //No Category Create
        $this->client->request('GET', '/categorys/create');
        $this->assertResponseRedirects('/login', 302);

        //No Category Edit
        $this->client->request('GET', '/categorys/1/edit');
        $this->assertResponseRedirects('/login', 302);

        //No Categorys Delete
        $this->client->request('GET', '/categorys/1/delete');
        $this->assertResponseRedirects('/login', 302);

        //No devices
        $this->client->request('GET', '/device');
        $this->assertResponseRedirects('/login', 302);

        //No Create devices
        $this->client->request('GET', '/device/create');
        $this->assertResponseRedirects('/login', 302);

        //No Delete devices
        $this->client->request('GET', '/device/1/delete');
        $this->assertResponseRedirects('/login', 302);

        //No Edit devices
        $this->client->request('GET', '/device/1/edit');
        $this->assertResponseRedirects('/login', 302);

        //No devices view description
        $this->client->request('GET', '/device/1/description');
        $this->assertResponseRedirects('/login', 302);

        //No devices view Experiment association
        $this->client->request('GET', '/device/1/experiments');
        $this->assertResponseRedirects('/login', 302);

        //No devices view commits
        $this->client->request('GET', '/device/1/commits');
        $this->assertResponseRedirects('/login', 302);

        //No devices view documents
        $this->client->request('GET', '/device/1/documents');
        $this->assertResponseRedirects('/login', 302);

        //No locations
        $this->client->request('GET', '/location');
        $this->assertResponseRedirects('/login', 302);

        //No locations create
        $this->client->request('GET', '/location/create');
        $this->assertResponseRedirects('/login', 302);

        //No locations details
        $this->client->request('GET', '/location/1');
        $this->assertResponseRedirects('/login', 302);

        //No locations edit
        $this->client->request('GET', '/location/1/edit');
        $this->assertResponseRedirects('/login', 302);

        //No locations delete
        $this->client->request('GET', '/location/1/delete');
        $this->assertResponseRedirects('/login', 302);

        //No Labels
        $this->client->request('GET', '/labels/1');
        $this->assertResponseRedirects('/login', 302);

        //No Labels edit
        $this->client->request('GET', '/labels/1/edit');
        $this->assertResponseRedirects('/login', 302);

        //No Labels delete
        $this->client->request('GET', '/labels/1/delete');
        $this->assertResponseRedirects('/login', 302);

        //no user overview
        $this->client->request('GET', '/user');
        $this->assertResponseRedirects('/login', 302);

        //No user create
        $this->client->request('GET', '/user/create');
        $this->assertResponseRedirects('/login', 302);

        //No user edit
        $this->client->request('GET', '/user/1/edit');
        $this->assertResponseRedirects('/login', 302);

        //No user deletion
        $this->client->request('GET', '/user/1/delete');
        $this->assertResponseRedirects('/login', 302);

        //Anonymous has no user profile …
        $this->client->request('GET', '/profil');
        $this->assertResponseRedirects('/login', 302);

        //No Reservations
        $this->client->request('GET', '/remember/1');
        $this->assertResponseRedirects('/login', 302);

        //No Booking page
        $this->client->request('GET', '/book');
        $this->assertResponseRedirects('/login', 302);

        //No Experiment Canecling
        $this->client->request('GET', '/cancel_experiment/1');
        $this->assertResponseRedirects('/login', 302);

        //No Reservation Canceling
        $this->client->request('GET', '/cancel/1');
        $this->assertResponseRedirects('/login', 302);

        //No Reservation Detail
        $this->client->request('GET', '/reservation/1');
        $this->assertResponseRedirects('/login', 302);

        //No overview about own reservations
        $this->client->request('GET', '/reservation/private');
        $this->assertResponseRedirects('/login', 302);

        //No overview about all reservations
        $this->client->request('GET', '/reservation/all');
        $this->assertResponseRedirects('/login', 302);

        //No overview about reservations statistics
        $this->client->request('GET', '/reservation/statistics');
        $this->assertResponseRedirects('/login', 302);
    }

    public function testUserRole()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'User';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        //User can view Front Page
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can view version history
        $this->client->request('GET', '/version');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can view experiments
        $this->client->request('GET', '/experiment');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can view categorys
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can view the description of a single experiment
        $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can filter Experiments bei Category and show Categorys
        $this->client->request('GET', '/experiments/filterByCategory/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can search for Experiments
        $this->client->request('GET', '/search/experiments/Testexperiment');
        $this->assertContains("Testexperiment", $this->client->getResponse()->getContent());

        //User can view Experiment Comments
        $this->client->request('GET', '/experiment/1/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can write New Comments
        $this->client->request('GET', '/addComment');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //We can do this, but since we provided no comment data, it will give 500

        //Users can view Experiment Documents
        $this->client->request('GET', '/experiment/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can view it's own profile …
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //User can make Reservations
        $this->client->request('GET', '/remember/1');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //we can do this, but it will throw an error, because the action is out of context

        //User can view Booking page
        $this->client->request('GET', '/book');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        //User can cancel Experiments
        $this->client->request('GET', '/cancel_experiment/1');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //User can see reservation Detail
        $this->client->request('GET', '/reservation/1');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode()); //we can do this, but it will throw an error, because the action is out of context

        //User can see overview about own reservations
        $this->client->request('GET', '/reservation/private');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //everything else: nope

        //User can search for Devices but no result will show up
        $this->client->request('GET', '/search/devices/Testger%C3%A4t');
        $this->assertContains("Keine Ergebnisse", $this->client->getResponse()->getContent());

        //No Create Experiment
        $this->client->request('GET', '/experiment/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Edit Experiment
        $this->client->request('GET', '/experiment/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Delete Experiment
        $this->client->request('GET', '/experiment/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Experiment Devices
        $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Comment Deletion
        $this->client->request('GET', '/deleteComment/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Overview about Comments
        $this->client->request('GET', '/comments');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Experiment Commits
        $this->client->request('GET', '/experiment/1/commits');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Commit Approve
        $this->client->request('GET', '/commits/1/approve');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Commit Reject
        $this->client->request('GET', '/commits/1/reject');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Commit Overview
        $this->client->request('GET', '/commits');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No overview about changes pending release
        $this->client->request('GET', '/releases');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Category Create
        $this->client->request('GET', '/categorys/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Category Edit
        $this->client->request('GET', '/categorys/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Categorys Delete
        $this->client->request('GET', '/categorys/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No devices
        $this->client->request('GET', '/device');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Create devices
        $this->client->request('GET', '/device/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Delete devices
        $this->client->request('GET', '/device/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Edit devices
        $this->client->request('GET', '/device/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No devices view description
        $this->client->request('GET', '/device/1/description');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No devices view Experiment association
        $this->client->request('GET', '/device/1/experiments');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No devices view commits
        $this->client->request('GET', '/device/1/commits');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No devices view documents
        $this->client->request('GET', '/device/1/documents');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations
        $this->client->request('GET', '/location');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations create
        $this->client->request('GET', '/location/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations details
        $this->client->request('GET', '/location/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations edit
        $this->client->request('GET', '/location/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations delete
        $this->client->request('GET', '/location/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Labels
        $this->client->request('GET', '/labels/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Labels edit
        $this->client->request('GET', '/labels/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Labels delete
        $this->client->request('GET', '/labels/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //no user overview
        $this->client->request('GET', '/user');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user create
        $this->client->request('GET', '/user/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user edit
        $this->client->request('GET', '/user/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user deletion
        $this->client->request('GET', '/user/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No overview about all reservations
        $this->client->request('GET', '/reservation/all');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No overview about reservations statistics
        $this->client->request('GET', '/reservation/statistics');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    public function testDemoRole()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Demo';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Demo can view Front Page
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can view version history
        $this->client->request('GET', '/version');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can view experiments
        $this->client->request('GET', '/experiment');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can view categorys
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can view the description of a single experiment
        $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can filter Experiments bei Category and show Categorys
        $this->client->request('GET', '/experiments/filterByCategory/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can search for Experiments
        $this->client->request('GET', '/search/experiments/Testexperiment');
        $this->assertContains("Testexperiment", $this->client->getResponse()->getContent());

        //Demo can view Experiment Comments
        $this->client->request('GET', '/experiment/1/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can write New Comments
        $this->client->request('GET', '/addComment');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //We can do this, but since we provided no comment data, it will give 500

        //Demo can view Experiment Documents
        $this->client->request('GET', '/experiment/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can view it's own profile …
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can search for Devices
        $this->client->request('GET', '/search/devices/Testger%C3%A4t');
        $this->assertContains("Testger\\u00e4t<\\", $this->client->getResponse()->getContent());

         //Demo can view Experiment Devices
        $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

         //Demo can view devices
        $this->client->request('GET', '/device');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can view devices view description
        $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Demo can view devices Experiment association
        $this->client->request('GET', '/device/1/experiments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

         //Demo can view devices documents
        $this->client->request('GET', '/device/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //everything else: nope

        //No Create Experiment
        $this->client->request('GET', '/experiment/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Edit Experiment
        $this->client->request('GET', '/experiment/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Delete Experiment
        $this->client->request('GET', '/experiment/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Comment Deletion
        $this->client->request('GET', '/deleteComment/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Overview about Comments
        $this->client->request('GET', '/comments');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Experiment Commits
        $this->client->request('GET', '/experiment/1/commits');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Commit Approve
        $this->client->request('GET', '/commits/1/approve');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Commit Reject
        $this->client->request('GET', '/commits/1/reject');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Commit Overview
        $this->client->request('GET', '/commits');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No overview about changes pending release
        $this->client->request('GET', '/releases');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Category Create
        $this->client->request('GET', '/categorys/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Category Edit
        $this->client->request('GET', '/categorys/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Categorys Delete
        $this->client->request('GET', '/categorys/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Create devices
        $this->client->request('GET', '/device/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Delete devices
        $this->client->request('GET', '/device/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Edit devices
        $this->client->request('GET', '/device/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No devices view commits
        $this->client->request('GET', '/device/1/commits');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No view locations
        $this->client->request('GET', '/location');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations create
        $this->client->request('GET', '/location/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations details
        $this->client->request('GET', '/location/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations edit
        $this->client->request('GET', '/location/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations delete
        $this->client->request('GET', '/location/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Labels edit
        $this->client->request('GET', '/labels/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No view Labels
        $this->client->request('GET', '/labels/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Labels delete
        $this->client->request('GET', '/labels/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //no user overview
        $this->client->request('GET', '/user');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user create
        $this->client->request('GET', '/user/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user edit
        $this->client->request('GET', '/user/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user deletion
        $this->client->request('GET', '/user/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Reservations
        $this->client->request('GET', '/remember/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Booking page
        $this->client->request('GET', '/book');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Experiment Canecling
        $this->client->request('GET', '/cancel_experiment/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Reservation Canceling
        $this->client->request('GET', '/cancel/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Reservation Detail
        $this->client->request('GET', '/reservation/1');
         $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No overview about own reservations
        $this->client->request('GET', '/reservation/private');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No overview about all reservations
        $this->client->request('GET', '/reservation/all');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No overview about reservations statistics
        $this->client->request('GET', '/reservation/statistics');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    public function testVlaRole()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'VLA';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //VLA can view Front Page
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view version history
        $this->client->request('GET', '/version');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view experiments
        $this->client->request('GET', '/experiment');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view categorys
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view the description of a single experiment
        $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can filter Experiments bei Category and show Categorys
        $this->client->request('GET', '/experiments/filterByCategory/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can search for Experiments
        $this->client->request('GET', '/search/experiments/Testexperiment');
        $this->assertContains("Testexperiment", $this->client->getResponse()->getContent());

        //VLA can view Experiment Comments
        $this->client->request('GET', '/experiment/1/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can write New Comments
        $this->client->request('GET', '/addComment');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //We can do this, but since we provided no comment data, it will give 500

        //VLA can view Experiment Documents
        $this->client->request('GET', '/experiment/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view it's own profile …
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can search for Devices
        $this->client->request('GET', '/search/devices/Testger%C3%A4t');
        $this->assertContains("Testger\\u00e4t<\\", $this->client->getResponse()->getContent());

        //VLA can view Experiment Devices
        $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view devices
        $this->client->request('GET', '/device');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view devices view description
        $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view devices Experiment association
        $this->client->request('GET', '/device/1/experiments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view devices documents
        $this->client->request('GET', '/device/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can Edit Experiment
        $this->client->request('GET', '/experiment/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can delete Experiment
        $this->client->request('GET', '/experiment/1/delete');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //VLA can Overview about Comments
        $this->client->request('GET', '/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can view Experiment Commits
        $this->client->request('GET', '/experiment/1/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can Approve commits
        $this->client->request('GET', '/commits/1/approve');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //VLA can reject Commits
        $this->client->request('GET', '/commits/1/reject');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //VLA can view Commit Overview
        $this->client->request('GET', '/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can overview about changes pending release
        $this->client->request('GET', '/releases');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can Edit devices
        $this->client->request('GET', '/device/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can Delete devices
        $this->client->request('GET', '/device/1/delete');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //VLA can view device commits
        $this->client->request('GET', '/device/1/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can edit Labels
        $this->client->request('GET', '/labels/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can  view Labels
        $this->client->request('GET', '/labels/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA can make Reservations
        $this->client->request('GET', '/remember/1');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //we can but it will fail since no Data is provided

        //VLA can view Booking page
        $this->client->request('GET', '/book');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //we can but it will fail since no Data is provided

        //VLA can cancel Experiments
        $this->client->request('GET', '/cancel_experiment/1');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode()); //we are allowed but nothing meaningful comes out of it … (does life has a meaning anyways?)

        //VLA can view Reservation Detail
        $this->client->request('GET', '/reservation/1');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode()); //we are allowed but nothing meaningful comes out of it … (does life has a meaning anyways?)

        //VLA can view overview about own reservations
        $this->client->request('GET', '/reservation/private');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA has overview about all reservations
        $this->client->request('GET', '/reservation/all');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //VLA has overview about reservations statistics
        $this->client->request('GET', '/reservation/statistics');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //everything else: nope

        //No Create Experiment
        $this->client->request('GET', '/experiment/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Comment Deletion
        $this->client->request('GET', '/deleteComment/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Category Create
        $this->client->request('GET', '/categorys/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No edit Categorys
        $this->client->request('GET', '/categorys/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Categorys Delete
        $this->client->request('GET', '/categorys/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Create devices
        $this->client->request('GET', '/device/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations create
        $this->client->request('GET', '/location/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations view
        $this->client->request('GET', '/location');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations details
        $this->client->request('GET', '/location/1');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations editing
        $this->client->request('GET', '/location/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No locations delete
        $this->client->request('GET', '/location/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No Labels delete
        $this->client->request('GET', '/labels/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        //no user overview
        $this->client->request('GET', '/user');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user create
        $this->client->request('GET', '/user/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user edit
        $this->client->request('GET', '/user/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());

        //No user deletion
        $this->client->request('GET', '/user/1/delete');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());
    }

    public function testAdminRole()
    {
        //Admins can do everything :)
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Admin can view Front Page
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view version history
        $this->client->request('GET', '/version');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view experiments
        $this->client->request('GET', '/experiment');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view categorys
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view the description of a single experiment
        $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can filter Experiments bei Category and show Categorys
        $this->client->request('GET', '/experiments/filterByCategory/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can search for Experiments
        $this->client->request('GET', '/search/experiments/Testexperiment');
        $this->assertContains("Testexperiment", $this->client->getResponse()->getContent());

        //Admin can view Experiment Comments
        $this->client->request('GET', '/experiment/1/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can write New Comments
        $this->client->request('GET', '/addComment');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //We can do this, but since we provided no comment data, it will give 500

        //Admin can view Experiment Documents
        $this->client->request('GET', '/experiment/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view it's own profile …
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can search for Devices
        $this->client->request('GET', '/search/devices/Testger%C3%A4t');
        $this->assertContains("Testger\\u00e4t<\\", $this->client->getResponse()->getContent());

        //Admin can view Experiment Devices
        $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view devices
        $this->client->request('GET', '/device');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view devices view description
        $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view devices Experiment association
        $this->client->request('GET', '/device/1/experiments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view devices documents
        $this->client->request('GET', '/device/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can Edit Experiment
        $this->client->request('GET', '/experiment/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can delete Experiment
        $this->client->request('GET', '/experiment/1/delete');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Admin can Overview about Comments
        $this->client->request('GET', '/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view Experiment Commits
        $this->client->request('GET', '/experiment/1/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can Approve commits
        $this->client->request('GET', '/commits/1/approve');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Admin can reject Commits
        $this->client->request('GET', '/commits/1/reject');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Admin can view Commit Overview
        $this->client->request('GET', '/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can overview about changes pending release
        $this->client->request('GET', '/releases');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can Edit devices
        $this->client->request('GET', '/device/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can Delete devices
        $this->client->request('GET', '/device/1/delete');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Admin can view device commits
        $this->client->request('GET', '/device/1/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can edit Labels
        $this->client->request('GET', '/labels/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can  view Labels
        $this->client->request('GET', '/labels/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can make Reservations
        $this->client->request('GET', '/remember/1');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //we can but it will fail since no Data is provided

        //Admin can view Booking page
        $this->client->request('GET', '/book');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //we can but it will fail since no Data is provided

        //Admin can cancel Experiments
        $this->client->request('GET', '/cancel_experiment/1');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode()); //we are allowed but nothing meaningful comes out of it … (does life has a meaning anyways?)

        //Admin can view Reservation Detail
        $this->client->request('GET', '/reservation/1');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode()); //we are allowed but nothing meaningful comes out of it … (does life has a meaning anyways?)

        //Admin can view overview about own reservations
        $this->client->request('GET', '/reservation/private');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin has overview about all reservations
        $this->client->request('GET', '/reservation/all');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin has overview about reservations statistics
        $this->client->request('GET', '/reservation/statistics');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can Create Experiment
        $this->client->request('GET', '/experiment/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can delete Comments
        $this->client->request('GET', '/deleteComment/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can create Category
        $this->client->request('GET', '/categorys/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can edit Categorys
        $this->client->request('GET', '/categorys/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can delete Categorys
        $this->client->request('GET', '/categorys/1/delete');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can Create devices
        $this->client->request('GET', '/device/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can create locations
        $this->client->request('GET', '/location/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view locations
        $this->client->request('GET', '/location');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can view locations
        $this->client->request('GET', '/location/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can edit locations
        $this->client->request('GET', '/location/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can delete locations
        $this->client->request('GET', '/location/1/delete');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode()); //that doesn't work because the location is still in use! - but in theory we can

        //Admin can delete Labels
        $this->client->request('GET', '/labels/1/delete');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Admin can view user overview
        $this->client->request('GET', '/user');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can create users
        $this->client->request('GET', '/user/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can edit users
        $this->client->request('GET', '/user/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Admin can delete users
        $this->client->request('GET', '/user/2/delete');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
    }


    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
