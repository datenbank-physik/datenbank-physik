<?php

// tests/bootstrap.php
if (isset($_ENV['BOOTSTRAP_CLEAR_CACHE_ENV'])) {
    // executes the "php bin/console cache:clear" command
    passthru(sprintf(
        'APP_ENV=%s php "%s/../bin/console" cache:clear --no-warmup',
        $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV'],
        __DIR__
    ));
}

if (isset($_ENV['BOOTSTRAP_PURGE_DB_ENV'])) {
    passthru(sprintf(
        'APP_ENV=%s php "%s/../bin/console" doctrine:database:drop --force --env=test -q',
        $_ENV['BOOTSTRAP_PURGE_DB_ENV'],
        __DIR__
    ));
}

if (isset($_ENV['BOOTSTRAP_CREATE_DB_ENV'])) {
    passthru(sprintf(
        'APP_ENV=%s php "%s/../bin/console" doctrine:database:create --env=test -q',
        $_ENV['BOOTSTRAP_CREATE_DB_ENV'],
        __DIR__
    ));
}

if (isset($_ENV['BOOTSTRAP_MIGRATE_DB_ENV'])) {
    passthru(sprintf(
        'APP_ENV=%s php "%s/../bin/console" doctrine:migrations:migrate --env=test -q',
        $_ENV['BOOTSTRAP_MIGRATE_DB_ENV'],
        __DIR__
    ));
}

if (isset($_ENV['BOOTSTRAP_FIXTURES_DB_ENV'])) {
    passthru(sprintf(
        'APP_ENV=%s php "%s/../bin/console" doctrine:fixtures:load --env=test -q',
        $_ENV['BOOTSTRAP_FIXTURES_DB_ENV'],
        __DIR__
    ));
}

require __DIR__.'/../config/bootstrap.php';
