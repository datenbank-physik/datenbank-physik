<?php

namespace App\Tests\Controller;

use App\Controller\ReservationController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReservationControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    //All other functions than "Remember" are JS-dependant and need to be front end tested.
    public function testRemember()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->clickLink('Buchen');
        $this->assertResponseIsSuccessful();

        $crawler = $this->client->request('GET', '/remember/100');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }
}
