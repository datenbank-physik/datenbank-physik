<?php

namespace App\Tests\Controller;

use App\Entity\Label;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class LabelControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowLabel()
    {
        $this->logIn(['ROLE_ADMIN']);
        $this->client->request('GET', '/labels/2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('html h1', 'Testlabel');

        //the Labelpage for "Testlabel" includes not yet Experiment 1 (Testexperiment)
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/labels/2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertNotContains('Testexperiment', $crawler->html());

        //we can't view a label that doesn't exist
        $crawler = $this->client->request('GET', '/labels/1000');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Kein Label unter dieser nummer!", $crawler->html());
    }

    public function testEdditLabell()
    {
        $this->logIn(['ROLE_ADMIN']);
        $this->client->request('GET', '/labels/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/labels/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['label[name]'] = 'test';
        $form['label[color]'] = '#FF2600';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/labels/1', 302);
        $crawler = $this->client->request('GET', '/labels/1');
        $this->assertContains("test", $crawler->html());

        //Did the label color got set?
        $crawler = $this->client->request('GET', '/experiment/1/devices');
        $this->assertContains("#FF2600", $crawler->html());

        //We can't edit a lebel that doesn't exist
        $crawler = $this->client->request('GET', '/labels/1000/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Kein Label unter dieser nummer!", $crawler->html());
    }

    public function testaddExperiment()
    {
        //Testgerät is not in Testlabel yet
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/labels/2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertNotContains('Testexperiment', $crawler->html());

        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[labels]'] = '[{"value":"Plattenkondensator"}, {"value":"Scheibe"},{"value":"Testlabel"} ]';
        //$form['label[color]'] = '#000000';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);
        $crawler = $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Plattenkondensator', $crawler->html());
        $this->assertContains('Scheibe', $crawler->html());
        $this->assertContains('Testlabel', $crawler->html());

        //now the Labelpage for "Testlabel" includes Experiment 1 (Testexperiment)
        //needs to be at least role demo to view labels
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/labels/2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
    }

    public function testAddDevice()
    {
        //Testdevice is not in Testlabel yet
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/labels/2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertNotContains('Testgerät', $crawler->html());

        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[labels]'] = '[{"value":"Plattenkondensator"}, {"value":"Scheibe"},{"value":"Testlabel"} ]';
        //$form['label[color]'] = '#000000';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);
        $crawler = $this->client->request('GET', '/device/1/experiments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Plattenkondensator', $crawler->html());
        $this->assertContains('Scheibe', $crawler->html());
        $this->assertContains('Testlabel', $crawler->html());

        //now the Labelpage for "Testlabel" includes Device 1 (Testgerät)
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/labels/2');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
    }

    public function testAddLabel()
    {
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/labels/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['label[name]'] = 'Neues Label';
        $form['label[color]'] = '#FF2601';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device', 302);

        //Now let's add an experiment to the label and see if the labelcolor matches!
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[labels]'] = '[{"value":"Plattenkondensator"}, {"value":"Scheibe"},{"value":"Neues Label"} ]';
        //$form['label[color]'] = '#000000';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);
        $crawler = $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Plattenkondensator', $crawler->html());
        $this->assertContains('Scheibe', $crawler->html());
        $this->assertContains('Neues Label', $crawler->html());
        $this->assertContains("#FF2601", $crawler->html());
    }

    public function testRemoveExperiment()
    {
        $this->logIn(['ROLE_ADMIN']);
        //at the beginning we have the Label "LabelzumEntfernen" which is in the Database conected to experiment 1 just to be removed here
        $crawler = $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertNotContains('Plattenkondensator', $crawler->html());
        $this->assertContains('LabelzumEntfernen', $crawler->html());

        //now edit the experiment
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[labels]'] = '[{"value":"Plattenkondensator"}]';
        //$form['label[color]'] = '#000000';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //and now it should be removed
        $crawler = $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Plattenkondensator', $crawler->html());
        $this->assertNotContains('Labelentfernt', $crawler->html());
    }

    public function testRemoveDevice()
    {
        $this->logIn(['ROLE_ADMIN']);
        //at the beginning we have the Label "LabelzumEntfernen" which is in the Database conected to Device 1 just to be removed here
        $crawler = $this->client->request('GET', '/device/1/experiments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertNotContains('Plattenkondensator', $crawler->html());
        $this->assertContains('LabelzumEntfernen', $crawler->html());

        //now edit the experiment
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[labels]'] = '[{"value":"Plattenkondensator"}]';
        //$form['label[color]'] = '#000000';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //and now it should be removed
        $crawler = $this->client->request('GET', '/device/1/experiments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Plattenkondensator', $crawler->html());
        $this->assertNotContains('Labelentfernt', $crawler->html());
    }

    public function testRemoveLabel()
    {
        //We can't remove a label that doesn't exist
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/labels/1000/delete');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Kein Label unter dieser nummer!", $crawler->html());

        //But we can remove labels independent if they contain devices or not
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/labels/1/delete');
        $this->assertResponseRedirects('/device', 302);
    }



    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
