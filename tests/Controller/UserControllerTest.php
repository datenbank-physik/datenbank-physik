<?php

namespace App\Tests\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserControllerTest extends WebTestCase
{

    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    //check that user creation is only possible for Admins
    public function testUserCreation()
    {
        $this->client->request('GET', '/user/create');
        //User create page should redirect to login
        $this->assertTrue(
            $this->client->getResponse()->isRedirect('/login')
        );

        $this->logIn(['ROLE_VLA']);
        $this->client->request('GET', '/user/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_DEMO']);
        $this->client->request('GET', '/user/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_USER']);
        $this->client->request('GET', '/user/create');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_ADMIN']);
        $this->client->request('GET', '/user/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
    //user creation has to follow rules!
    public function testUserCreate()
    {

        $this->logIn(['ROLE_ADMIN']);
        $this->client->request('GET', '/user/create');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'test';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_ADMIN';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/user', 302);

        //no user can be created twice
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'test';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_ADMIN';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        //error if no name
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = '';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_ADMIN';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        //error if user name already exists
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'user';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_ADMIN';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        //error if invalid mail
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'invalidmail';
        $form['user[email]'] = 'test_test_de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_ADMIN';
        $crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Dieser Wert ist keine gültige E-Mail-Adresse.', $crawler->filter('li')->text(null, true));

        //error if invalid password
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'invalidpw';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = 't';
        $form['user[roles]'] = 'ROLE_ADMIN';
        $crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Das Password muss mindestens 6 Zeichen lang sein', $crawler->html());

        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'invalidpw';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '';
        $form['user[roles]'] = 'ROLE_ADMIN';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());


        //create wit role vla
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'vla_user';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_VLA';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/user', 302);

        //create with role user
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'user_test';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_USER';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/user', 302);

        //create with role demo
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'demo_user';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_DEMO';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/user', 302);

        //Remove second user (we created one earlier / should exist anyways) and user 1000 (that should generally not exist)
        $link = $this->client->request('GET', '/user')->filterXPath('//td/div/button')->eq(1)->attr('value');
        $crawler = $this->client->request('GET', $link);
        $this->assertResponseRedirects('/user', 302);
        $crawler = $this->client->request('GET', '/user/1000/delete');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Dieser Nutzer existiert nicht", $crawler->html());
        //Admin (first user) shouldn't be deletable
        //$crawler = $this->client->request('GET', '/user/1/delete');
        //$this->assertEquals(500, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidRole()
    {
        //error if invalid role
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'invalidrole';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE12345';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
    }

    public function testUserEdit()
    {
        //only Admins get the user overview page
        $this->client->request('GET', '/user');
        //over view page should redirect to login
        $this->assertTrue(
            $this->client->getResponse()->isRedirect('/login')
        );

        $this->logIn(['ROLE_VLA']);
        $this->client->request('GET', '/user');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_DEMO']);
        $this->client->request('GET', '/user');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_USER']);
        $this->client->request('GET', '/user');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_ADMIN']);
        $this->client->request('GET', '/user');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //only Admins can edit an user by id
        //Note: This is tested with the id=1, that should always exist
        $this->client->restart();
        $this->client->request('GET', '/user/1/edit');
        //over view page should redirect to login
        $this->assertTrue(
            $this->client->getResponse()->isRedirect('/login')
        );

        $this->logIn(['ROLE_VLA']);
        $this->client->request('GET', '/user/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_DEMO']);
        $this->client->request('GET', '/user/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_USER']);
        $this->client->request('GET', '/user/1/edit');
        $this->assertEquals(403, $this->client->getResponse()->getStatusCode());


        $this->logIn(['ROLE_ADMIN']);
        $this->client->request('GET', '/user/1/edit');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //Every user has a profile where the account can be edited, but you need to follow the rules (unless you are a politcal anarchist, I guess)
        $this->client->restart();
        //we now need to actually log in
        //so of to create once again a temporary user
        //create with role demo
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/user/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'demo_user';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $form['user[roles]'] = 'ROLE_DEMO';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/user', 302);
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'demo_user';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/', 302);

        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/profil');
        $form = $crawler->selectButton('Speichern')->form();
        $form['profile[name]'] = 'test';
        $form['profile[email]'] = 'tes1t@test.de';
        $form['profile[password]'] = '1234567';
        $form['profile[oldPassword]'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/', 302);

        //no change in PW
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/profil');
        $form = $crawler->selectButton('Speichern')->form();
        $form['profile[name]'] = 'test';
        $form['profile[email]'] = 'test@test.de';
        //$form['profile[password]'] = '';
        $form['profile[oldPassword]'] = '1234567';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/', 302);

        //error if no name
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/profil');
        $form = $crawler->selectButton('Speichern')->form();
        $form['profile[name]'] = '';
        $form['profile[email]'] = 'test@test.de';
        $form['profile[password]'] = '123456';
        $form['profile[oldPassword]'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        //error if invalid mail
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/profil');
        $form = $crawler->selectButton('Speichern')->form();
        $form['profile[name]'] = 'test';
        $form['profile[email]'] = 'test_test.de';
        $form['profile[password]'] = '123456';
        $form['profile[oldPassword]'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertContains('Dieser Wert ist keine gültige E-Mail-Adresse.', $crawler->html());

        //error if invalid old password
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/profil');
        $form = $crawler->selectButton('Speichern')->form();
        $form['profile[name]'] = 'test';
        $form['profile[email]'] = 'test@test.de';
        $form['profile[password]'] = '123456';
        $form['profile[oldPassword]'] = '123456122344534532454325345';
        $crawler = $this->client->submit($form);
        $this->assertContains('Altes Password stimmt nicht mit dem aktuellen Password überein', $crawler->html());

        //error if invalid password
        $this->client->request('GET', '/profil');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/profil');
        $form = $crawler->selectButton('Speichern')->form();
        $form['profile[name]'] = 'test';
        $form['profile[email]'] = 'test@test.de';
        $form['profile[password]'] = '1';
        $form['profile[oldPassword]'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertContains('Das Password muss mindestens 6 Zeichen lang sein', $crawler->html());


        //As Admin you can edit any user via the /user/[id]/path
        $this->client->restart();
        //we now need to actually log in
        //valid login is valid
        $this->logIn(['ROLE_ADMIN']);


        $crawler = $this->client->request('GET', '/user/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'Administratot';
        $form['user[email]'] = 'tes1t@test.de';
        $form['user[password]'] = '1234567';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/user', 302);

        //no change in PW
        $crawler = $this->client->request('GET', '/user/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'Administrator';
        $form['user[email]'] = 'tes1t@test.de';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/user', 302);

        //error if no name
        $crawler = $this->client->request('GET', '/user/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = '';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        //error if invalid mail
        $crawler = $this->client->request('GET', '/user/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'Administrator';
        $form['user[email]'] = 'test_test.de';
        $form['user[password]'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertContains('Dieser Wert ist keine gültige E-Mail-Adresse.', $crawler->html());

        //error if invalid password
        $crawler = $this->client->request('GET', '/user/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'Administrator';
        $form['user[email]'] = 'test@test.de';
        $form['user[password]'] = '12';
        $crawler = $this->client->submit($form);
        $this->assertContains('Das Password muss mindestens 6 Zeichen lang sein', $crawler->html());

        //changing role
        $crawler = $this->client->request('GET', '/user/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['user[name]'] = 'test12345';
        $form['user[email]'] = 'tes1t@test.de';
        $form['user[password]'] = '1234567';
        $form['user[roles]'] = 'ROLE_VLA';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/user', 302);

        //we can't edit an user that doesn't exist
        $crawler = $this->client->request('GET', '/user/1000/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
