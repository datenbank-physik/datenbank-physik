<?php

namespace App\Tests\Controller;

use App\Controller\LocationController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class LocationControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowAll()
    {
        //should include the "Testort" which is by default in the Database
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/location');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Standorte', $crawler->html());
        $this->assertContains('Testort', $crawler->html());
        $this->assertContains('4711', $crawler->html());
        $this->assertContains('Testgebäude', $crawler->html());
    }

    public function testShowDetails()
    {
        //should include the "Testort" which is by default in the Database
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/location/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
       //locations tha don't exist can't be viewed
        $crawler = $this->client->request('GET', '/location/1000');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testCreate()
    {
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/location/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['location[name]'] = 'Mein Ort';
        $form['location[roomnumber]'] = '123';
        $form['location[building]'] = 'Mein Gebäude';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device', 302);

        //And now the location should be there
        $crawler = $this->client->request('GET', '/location');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Standorte', $crawler->html());
        $this->assertContains('Mein Ort', $crawler->html());
        $this->assertContains('123', $crawler->html());
        $this->assertContains('Mein Gebäude', $crawler->html());
    }

    public function testDelete()
    {
        $this->logIn(['ROLE_ADMIN']);
        //Locations which are still used can't be deleted!
        $crawler = $this->client->request('GET', '/location/1/delete');
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        //Now we create a temporary Location to add it to our default experiment so we can remove our default Location!
        $crawler = $this->client->request('GET', '/location/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['location[name]'] = 'Mein Ort';
        $form['location[roomnumber]'] = '123';
        $form['location[building]'] = 'Mein Gebäude';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device', 302);

        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[locationMappings][location]'] = '3';
        //$form['label[color]'] = '#000000';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //now we can delete
        $crawler = $this->client->request('GET', '/location/1/delete');
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //And now it should be gone
        $crawler = $this->client->request('GET', '/location');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Standorte', $crawler->html());
        //$this->assertNotContains('Testort', $crawler->html());

        //We can't delete a location that doesn't exist
        $crawler = $this->client->request('GET', '/location/1000/delete');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testEdit()
    {
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/location/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['location[name]'] = 'Kein Ort';
        $form['location[roomnumber]'] = '115';
        $form['location[building]'] = 'Mein Gebäude';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/location', 302);

        //And now the location should be changed
        $crawler = $this->client->request('GET', '/location');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Standorte', $crawler->html());
        $this->assertNotContains('Testort', $crawler->html());
        $this->assertNotContains('4711', $crawler->html());
        $this->assertNotContains('Testgebäude', $crawler->html());
        $this->assertContains('Kein Ort', $crawler->html());
        $this->assertContains('115', $crawler->html());
        $this->assertContains('Mein Gebäude', $crawler->html());

        //You can't edit an location that doesn't exist …
        $crawler = $this->client->request('GET', '/location/1000/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
