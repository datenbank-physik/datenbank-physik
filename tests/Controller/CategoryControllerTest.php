<?php

namespace App\Tests\Controller;

use App\Controller\CategoryController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class CategoryControllerTest extends WebTestCase
{

    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testCreate()
    {
        //only check as admin, other rights are denied and tested in AccessRightsTest
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/categorys/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['category[name]'] = 'Test';
        $form['category[token]'] = 'T';
        $form['category[parent]'] = '';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment', 302);
    }

    public function testEdit()
    {
        //only check as admins, other rights are denied and tested in AccessRightsTest
        $this->logIn(['ROLE_ADMIN']);
        //category 1 should exist
        $crawler = $this->client->request('GET', '/categorys/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['category[name]'] = 'Test12';
        $form['category[token]'] = 'A';
        $form['category[parent]'] = '';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/category/1', 302);

        //Categorys that don't exist can't be edited
        $crawler = $this->client->request('GET', '/categorys/1000/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testDelete()
    {
        //only check as admins, other rights are denied and tested in AccessRightsTest
        $this->logIn(['ROLE_ADMIN']);
        //Category 1 exist in TestFixture, Category 2 is subcategory

        //This will fail because of Subcategorys
        $crawler = $this->client->request('GET', '/categorys/1/delete');
        $this->assertContains('Die Kategorie hat mindestens eine Unterkategorie oder enthält ein Experiment.', $crawler->html());

        //But we can delete the subcategory (nr. 3 is empty)
        $crawler = $this->client->request('GET', '/categorys/3/delete');
        $this->assertResponseRedirects('/experiment', 302);
        //And we can verify that main category is stil not deletable because of the experiment that is in it
        $crawler = $this->client->request('GET', '/categorys/1/delete');
        $this->assertContains('Die Kategorie hat mindestens eine Unterkategorie oder enthält ein Experiment.', $crawler->html());

        //Categorys that don't exist can't be deleted
        $crawler = $this->client->request('GET', '/categorys/1000/delete');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
