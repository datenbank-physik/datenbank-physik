<?php

namespace App\Tests\Controller;

use App\Controller\DeviceController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DeviceControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }
    public function testCreate()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Name and Location aren't optional!
        $crawler = $this->client->request('GET', '/device/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[name]'] = '';
        $form['device[locationMappings][location]'] = '1';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());


        //Now for real!
        $crawler = $this->client->request('GET', '/device/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[name]'] = 'Testen Gerät';
        $form['device[locationMappings][location]'] = '1';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device', 302);

        $crawler = $this->client->request('GET', '/device/2/description');
        $this->assertResponseIsSuccessful();
        $this->assertContains('Testen Gerät', $crawler->html());
        $this->assertContains('Testort', $crawler->html());
    }

    //Missing Location should throw Invalid Argument Exception
    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateInvalid()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('GET', '/device/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[name]'] = 'Testen Gerät';
        $form['device[locationMappings][location]'] = '';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
    }

    public function testView()
    {
        //should include the "Testgerät" which is by default in the Database
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //if we don't find it
        $crawler = $this->client->request('GET', '/device/100/description');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testshowAll()
    {
        //should include the "Testgerät" which is by default in the Database
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
    public function testViewCommits()
    {
        //should include the "Testgerät" which is by default in the Database
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //if we don't find it
        $crawler = $this->client->request('GET', '/device/100/commits');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
       // $this->assertContains('Migration', $crawler->html());
    }

    public function testViewDescription()
    {
        //should include the "Testgerät" which is by default in the Database
        $this->client->restart();
        $this->logIn(['ROLE_VLA']);
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('42', $crawler->html()); //experiment Number

        //if we don't find it
        $crawler = $this->client->request('GET', '/device/100/description');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testEdit()
    {
 //first sign in and test the Gerät against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[name]'] = 'Geänderter Titel';
        $form['device[description]'] = 'Andere Beschreibung';
        $form['device[comment]'] = 'Anderer Kommentar';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Note: Comment and Description need Approval, they shouldn't be changed!
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Geänderter Titel', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertNotContains('Andere Beschreibung', $crawler->html());
        $this->assertNotContains('Anderer Kommentar', $crawler->html());

        //Now let's approve things!
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("<ins>Anderer Kommentar</ins>", $crawler->html()); //includes the diff-view
        $this->assertContains("<ins>Andere </ins>Beschreibung", $crawler->html());

        //we'll approve two times
        $link = $crawler
            ->filter('a:contains("Bestätigen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/device/1/commits');
        $link = $crawler
            ->filter('a:contains("Bestätigen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);

        //Now everything should be changed
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Geänderter Titel', $crawler->html());
        $this->assertNotContains('Kommentar dazu', $crawler->html());
        $this->assertNotContains('Die Beschreibung', $crawler->html());
        $this->assertContains('Andere Beschreibung', $crawler->html());
        $this->assertContains('Anderer Kommentar', $crawler->html());

        //we can't edit a device that doesn't exist
        $crawler = $this->client->request('GET', '/device/1000/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testDelete()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/delete');
        $this->assertResponseRedirects('/device/1', 302);

        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Möchte das Gerät löschen", $crawler->html());

        //we'll deny this at first
        $link = $crawler
            ->filter('a:contains("Ablehnen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertContains("abgelehnt", $crawler->html());

        //now we'll do it!
        $crawler = $this->client->request('GET', '/device/1/delete');
        $this->assertResponseRedirects('/device/1', 302);
        $crawler = $this->client->request('GET', '/device/1/commits');
        $this->assertContains("Möchte das Gerät löschen", $crawler->html());

        //we'll deny this at first
        $link = $crawler
            ->filter('a:contains("Bestätigen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $this->assertResponseRedirects('/device', 302);

        //you can't delete what doesn't exist
        $crawler = $this->client->request('GET', '/device/1/delete');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testViewDocuments()
    {
        //should include the "Testgerät" which is by default in the Database
        $this->client->restart();
        $this->logIn(['ROLE_VLA']);
        $crawler = $this->client->request('GET', '/device/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testViewExperiments()
    {
        //should include the "Testgerät" which is by default in the Database
        $this->client->restart();
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/device/1/experiments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('LabelzumEntfernen', $crawler->html());
        $this->assertContains('Testexperiment', $crawler->html());
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
