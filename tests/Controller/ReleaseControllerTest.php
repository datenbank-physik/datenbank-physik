<?php

namespace App\Tests\Controller;

use App\Controller\ReleaseController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReleaseControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowAll()
    {
        $this->logIn(['ROLE_ADMIN']);
        //First let's change an experiment and a device so we can view some changes!
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Geänderter Titel';
        $form['experiment[description]'] = 'Andere Beschreibung Exp';
        $form['experiment[comment]'] = 'Anderer Kommentar Exp';
        $form['experiment[category]'] = '2';
        $form['experiment[experimentNumber]'] = '3000';
        $form['experiment[preparationTime]'] = '300';
        $form['experiment[execution_time]'] = '300';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[name]'] = 'Geänderter Titel';
        $form['device[description]'] = 'Andere Beschreibung Gerät';
        $form['device[comment]'] = 'Anderer Kommentar Gerät';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        $crawler = $this->client->request('GET', '/releases');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Freigabe", $crawler->html());
        $this->assertContains("Erforderliche Freigabe bei Gerät", $crawler->html());
        $this->assertContains("Erforderliche Freigabe bei Experiment", $crawler->html());
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
