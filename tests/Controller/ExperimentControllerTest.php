<?php

namespace App\Tests\Controller;

use App\Controller\ExperimentController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ExperimentControllerTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }


    public function testEdit()
    {
        //first sign in and test the Experiment against the initial Values
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('1h', $crawler->html());
        $this->assertContains('1h', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Geänderter Titel';
        $form['experiment[description]'] = 'Andere Beschreibung';
        $form['experiment[comment]'] = 'Anderer Kommentar';
        $form['experiment[category]'] = '2';
        $form['experiment[experimentNumber]'] = '3000';
        $form['experiment[preparationTime]'] = '300';
        $form['experiment[execution_time]'] = '300';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //Note: Comment and Description need Approval, they shouldn't be changed!
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Geänderter Titel', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('Die Beschreibung', $crawler->html());
        $this->assertNotContains('Andere Beschreibung', $crawler->html());
        $this->assertNotContains('Anderer Kommentar', $crawler->html());
        $this->assertContains('5h', $crawler->html());
        $this->assertContains('5h', $crawler->html());

        //Now let's approve things!
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("<ins>Anderer </ins>Kommentar<del> dazu</del>", $crawler->html()); //includes the diff-view
        $this->assertContains("<del>Di</del><ins>Ander</ins>e Beschreibung", $crawler->html());

        //we'll approve two times
        $link = $crawler
            ->filter('a:contains("Bestätigen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $link = $crawler
            ->filter('a:contains("Bestätigen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);

        //Now everything should be changed
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Geänderter Titel', $crawler->html());
        $this->assertNotContains('Kommentar dazu', $crawler->html());
        $this->assertNotContains('Die Beschreibung', $crawler->html());
        $this->assertContains('Andere Beschreibung', $crawler->html());
        $this->assertContains('Anderer Kommentar', $crawler->html());
        $this->assertContains('5h', $crawler->html());
        $this->assertContains('5h', $crawler->html());


        //you can't edit what doesn't exist
        $crawler = $this->client->request('GET', '/experiment/100/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testViewCommits()
    {
        //should include the "Testexperiment" which is by default in the Database
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Migration', $crawler->html());

        //we can't view commits when experiment doesn't exist
        $crawler = $this->client->request('GET', '/experiment/1000/commits');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testAddComment()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('GET', '/experiment/1/comments');
        $form = $crawler->selectButton('Kommentieren')->form();
        $form['message'] = 'Neuer Kommentar';
        $crawler = $this->client->submit($form);
        $crawler = $this->client->request('GET', '/experiment/1/comments');
        $this->assertContains('Neuer Kommentar', $crawler->html());

        //you can't add a comment to something that doesn't exist
        $crawler = $this->client->request('GET', '/device/1000/comments');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testDeleteComment()
    {
        $this->client->restart();
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/experiment/1/comments');
        $this->assertContains("Ein Kommentar!", $crawler->html());
        $link = $crawler
        ->filter('a:contains("Löschen")')
        ->eq(0)
        ->link()
        ;
         $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/experiment/1/comments');
        $this->assertNotContains('Ein Kommntar!', $crawler->html());
    }

    public function testDeleteCommentviaURL()
    {
        $this->client->restart();
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/deleteComment/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        //what doesn't exist can't be deleted
        $crawler = $this->client->request('GET', '/deleteComment/1002323');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }


    public function testViewDevices()
    {
        //should include the "Testexperiment" which is by default in the Database
        $this->client->restart();
        $this->logIn(['ROLE_ADMIN']);
        $crawler = $this->client->request('GET', '/experiment/1/devices');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('LabelzumEntfernen', $crawler->html());
        $this->assertContains('Testgerät', $crawler->html());
    }

    public function testDelete()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/experiment/1/delete');
        $this->assertResponseRedirects('/experiment/1/description', 302);

        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertResponseIsSuccessful();
        $this->assertContains("Möchte das Experiment löschen", $crawler->html());

        //we'll deny this at first
        $link = $crawler
            ->filter('a:contains("Ablehnen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertContains("abgelehnt", $crawler->html());

        //now we'll do it!
        $crawler = $this->client->request('GET', '/experiment/1/delete');
        $this->assertResponseRedirects('/experiment/1/description', 302);
        $crawler = $this->client->request('GET', '/experiment/1/commits');
        $this->assertContains("Möchte das Experiment löschen", $crawler->html());

        //we'll deny this at first
        $link = $crawler
            ->filter('a:contains("Bestätigen")')
            ->eq(0)
            ->link()
        ;
        $crawler = $this->client->click($link);
        $this->assertResponseRedirects('/experiment', 302);

        //you can't delete what doesn't exist
        $crawler = $this->client->request('GET', '/experiment/1000/delete');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testViewComments()
    {
        //should include the "Testexperiment" which is by default in the Database
        $this->client->restart();
        $this->logIn(['ROLE_VLA']);
        $crawler = $this->client->request('GET', '/experiment/1/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Ein Kommentar', $crawler->html());
        $this->assertContains('Eine Antwort', $crawler->html());
        $this->assertContains('Ein weiterer Kommentar', $crawler->html());

        //no comments for this experiment
        $crawler = $this->client->request('GET', '/experiment/10000/comments');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testViewDocuments()
    {
        //should include the "Testexperiment" which is by default in the Database
        $this->client->restart();
        $this->logIn(['ROLE_VLA']);
        $crawler = $this->client->request('GET', '/experiment/1/documents');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }


    public function testAddExperiment()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Name and Number aren't optional!
        $crawler = $this->client->request('GET', '/experiment/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = '';
        $form['experiment[description]'] = 'Die Beschreibung';
        $form['experiment[comment]'] = 'Der Kommentar';
        $form['experiment[category]'] = '2';
        $form['experiment[experimentNumber]'] = '4711';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('GET', '/experiment/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Mein Experiment';
        $form['experiment[description]'] = 'Die Beschreibung';
        $form['experiment[comment]'] = 'Der Kommentar';
        $form['experiment[category]'] = '2';
        $form['experiment[experimentNumber]'] = '';
        $crawler = $this->client->submit($form);
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());

        //Now for real!
        $crawler = $this->client->request('GET', '/experiment/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Mein Experiment';
        $form['experiment[description]'] = 'Die Beschreibung';
        $form['experiment[comment]'] = 'Der Kommentar';
        $form['experiment[category]'] = '2';
        $form['experiment[experimentNumber]'] = '4711';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment', 302);

        $crawler = $this->client->request('GET', '/experiment/2/description');
        $this->assertResponseIsSuccessful();
        $this->assertContains('Mein Experiment', $crawler->html());
        $this->assertContains('Die Beschreibung', $crawler->html());
        $this->assertContains('Der Kommentar', $crawler->html());
        $this->assertContains('4711', $crawler->html());
    }

    public function testViewDescription()
    {

        //should include the "Testexperiment" which is by default in the Database
        $this->client->restart();
        $crawler = $this->client->request('GET', '/experiment/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());
        $this->assertContains('Kommentar dazu', $crawler->html());
        $this->assertContains('42', $crawler->html()); //experiment Number
    }


    public function testPrevAndShowNext()
    {
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Previous Experiment!
        $crawler = $this->client->request('GET', '/experiment/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Vorheriges-Experiment';
        $form['experiment[description]'] = 'Die Beschreibung';
        $form['experiment[comment]'] = 'Der Kommentar';
        $form['experiment[category]'] = '2';
        $form['experiment[experimentNumber]'] = '1';
        $form['experiment[status]'] = 'available';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Next Experiment!
        $crawler = $this->client->request('GET', '/experiment/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Nächstes-Experiment';
        $form['experiment[description]'] = 'Die Beschreibung';
        $form['experiment[comment]'] = 'Der Kommentar';
        $form['experiment[category]'] = '2';
        $form['experiment[experimentNumber]'] = '130';
        $form['experiment[status]'] = 'available';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Current Experiment!
        $crawler = $this->client->request('GET', '/experiment/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Aktuelles-Experiment';
        $form['experiment[description]'] = 'Die Beschreibung';
        $form['experiment[comment]'] = 'Der Kommentar';
        $form['experiment[category]'] = '2';
        $form['experiment[experimentNumber]'] = '10';
        $form['experiment[status]'] = 'available';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Now Start with our walk
        $crawler = $this->client->request('GET', '/experiment/5/description');
        $this->assertContains("Aktuelles-Experiment", $crawler->html());
        $this->assertContains("10", $crawler->html());
        //Next
        $crawler = $this->client->request('GET', '/experiment/next/T/10'); //We are on experiment 5 (with exp-Number 10) in category T

        $crawler = $this->client->followRedirect();
        $this->assertStringContainsString("Testexperiment", $crawler->html());
        $this->assertStringContainsString("42", $crawler->html());
        //Next
        $crawler = $this->client->request('GET', '/experiment/next/T/42'); //We are on experiment 1 with exp-Number 42  in category T

        $crawler = $this->client->followRedirect();
        $this->assertContains("Nächstes-Experiment", $crawler->html());
        $this->assertContains("130", $crawler->html());
        //And Back
        $crawler = $this->client->request('GET', '/experiment/previous/T/130'); //We are on experiment 4 with exp-Number 130 in category T

        $crawler = $this->client->followRedirect();
        $this->assertContains("Testexperiment", $crawler->html());
        $this->assertContains("42", $crawler->html());
        //And Back
        $crawler = $this->client->request('GET', '/experiment/previous/T/42'); //We are on experiment 1 with experiment Number 42 in category T

        $crawler = $this->client->followRedirect();
        //We started here
        $this->assertContains("Aktuelles-Experiment", $crawler->html());
        $this->assertContains("10", $crawler->html());
        //Now we go to the first
        $crawler = $this->client->request('GET', '/experiment/previous/T/10'); //We are on experiment 5 with exoeriment Number 10 in category T

        $crawler = $this->client->followRedirect();
        $this->assertContains("Vorheriges-Experiment", $crawler->html());
        $this->assertContains("1", $crawler->html());

        //and again for anonymous users
        $this->client->restart();

        //Start
        $crawler = $this->client->request('GET', '/experiment/5/description');
        $this->assertContains("Aktuelles-Experiment", $crawler->html());
        $this->assertContains("10", $crawler->html());
        //Next
        $crawler = $this->client->request('GET', '/experiment/next/T/10'); //We are on experiment 5 (with exp-Number 10) in category T

        $crawler = $this->client->followRedirect();
        $this->assertStringContainsString("Testexperiment", $crawler->html());
        $this->assertStringContainsString("42", $crawler->html());
        //Next
        $crawler = $this->client->request('GET', '/experiment/next/T/42'); //We are on experiment 1 with exp-Number 42  in category T

        $crawler = $this->client->followRedirect();
        $this->assertContains("Nächstes-Experiment", $crawler->html());
        $this->assertContains("130", $crawler->html());
        //And Back
        $crawler = $this->client->request('GET', '/experiment/previous/T/130'); //We are on experiment 4 with exp-Number 130 in category T

        $crawler = $this->client->followRedirect();
        $this->assertContains("Testexperiment", $crawler->html());
        $this->assertContains("42", $crawler->html());
        //And Back
        $crawler = $this->client->request('GET', '/experiment/previous/T/42'); //We are on experiment 1 with experiment Number 42 in category T

        $crawler = $this->client->followRedirect();
        //We started here
        $this->assertContains("Aktuelles-Experiment", $crawler->html());
        $this->assertContains("10", $crawler->html());
        //Now we go to the first
        $crawler = $this->client->request('GET', '/experiment/previous/T/10'); //We are on experiment 5 with exoeriment Number 10 in category T

        $crawler = $this->client->followRedirect();
        $this->assertContains("Vorheriges-Experiment", $crawler->html());
        $this->assertContains("1", $crawler->html());
    }

    public function testShowAllComments()
    {
        $this->client->restart();
        $this->logIn(['ROLE_VLA']);
        $crawler = $this->client->request('GET', '/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Experiment", $crawler->html());
        $this->assertContains("Nutzer", $crawler->html());
        $this->assertContains("Kommentar", $crawler->html());
        $this->assertContains("Datum", $crawler->html());
    }

    public function testfilterByCategory()
    {
        $this->client->restart();
        $crawler = $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Testexperiment", $crawler->html());

        //and now for privileged users
        $this->client->restart();
        $this->logIn(['ROLE_VLA']);
        $crawler = $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Testexperiment", $crawler->html());
    }


    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
