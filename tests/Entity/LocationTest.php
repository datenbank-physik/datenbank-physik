<?php

namespace App\Tests\Entity;

use App\Entity\Commit;
use App\Entity\Location;
use App\Entity\LocationMappings;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{

    public function testCommits()
    {
        $location = new Location();
        $commit = new Commit();
        $location->addCommit($commit);
        $this->assertSame($commit, $location->getCommits()->first());
        $location->removeCommit($commit);
        $this->assertEmpty($location->getCommits());
    }

    public function testLocationMappings()
    {
        $location = new Location();
        $locationMapping = new LocationMappings();
        $location->addLocationMapping($locationMapping);
        $this->assertSame($locationMapping, $location->getLocationMappings()->first());
        $location->removeLocationMapping($locationMapping);
        $this->assertEmpty($location->getLocationMappings());
    }


    public function testGetId()
    {
        $location = new Location();
        $this->assertNull($location->getId());
    }


    public function testName()
    {
        $location = new Location();
        $location->setName("Name");
        $this->assertSame("Name", $location->getName());
    }

    public function testRoomnumber()
    {
        $location = new Location();
        $location->setRoomnumber("Raumnummer");
        $this->assertSame("Raumnummer", $location->getRoomnumber());
    }

    public function testBuilding()
    {
        $location = new Location();
        $location->setBuilding("Gebäude");
        $this->assertSame("Gebäude", $location->getBuilding());
    }
}
