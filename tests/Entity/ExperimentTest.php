<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Commit;
use App\Entity\Document;
use App\Entity\Experiment;
use App\Entity\Label;
use App\Entity\Reservation;
use App\Entity\ReservationExperiment;
use PHPUnit\Framework\TestCase;

class ExperimentTest extends TestCase
{

    public function testReservationExperiment()
    {
        $experiment = new Experiment();
        $reservation = new ReservationExperiment();
        $experiment->addReservationExperiment($reservation);
        $this->assertSame($reservation, $experiment->getReservationExperiments()->first());
        $experiment->removeReservationExperiment($reservation);
        $this->assertEmpty($experiment->getReservationExperiments());
    }

    public function testPreparationTime()
    {
        $experiment = new Experiment();
        $experiment->setPreparationTime(100);
        $this->assertSame(100, $experiment->getPreparationTime());
    }





    public function testExperimentNumber()
    {
        $experiment = new Experiment();
        $experiment->setExperimentNumber(100);
        $this->assertSame(100, $experiment->getExperimentNumber());
    }

    public function testDocument()
    {
        $experiment = new Experiment();
        $document = new Document();
        $experiment->addDocument($document);
        $this->assertSame($document, $experiment->getDocuments()->first());
        $experiment->removeDocument($document);
        $this->assertEmpty($experiment->getDocuments());
    }

    public function testCategory()
    {
        $experiment = new Experiment();
        $category = new Category();
        $experiment->setCategory($category);
        $this->assertSame($category, $experiment->getCategory());
    }

    public function testExecutionTime()
    {
        $experiment = new Experiment();
        $experiment->setExecutionTime(100);
        $this->assertSame(100, $experiment->getExecutionTime());
    }

    public function testName()
    {
        $experiment = new Experiment();
        $experiment->setName("Experiment");
        $this->assertSame("Experiment", $experiment->getName());
    }

    public function testGetStatus()
    {
        $experiment = new Experiment();
        $experiment->setStatus("available");
        $this->assertSame("available", $experiment->getStatus());
    }

    public function testComments()
    {
        $experiment = new Experiment();
        $comment = new Comment();
        $experiment->addComment($comment);
        $this->assertSame($comment, $experiment->getComments()->first());
        $experiment->removeComment($comment);
        $this->assertEmpty($experiment->getComments());
    }

    public function testLabels()
    {
        $experiment = new Experiment();
        $label = new Label();
        $experiment->addLabel($label);
        $this->assertSame($label, $experiment->getLabels()->first());
        $experiment->removeLabel($label);
        $this->assertEmpty($experiment->getLabels());
    }





    public function testDescription()
    {
        $experiment = new Experiment();
        $experiment->setDescription("Beschreibung");
        $this->assertSame("Beschreibung", $experiment->getDescription());
    }


    public function testComment()
    {
        $experiment = new Experiment();
        $experiment->setComment("Kommentar");
        $this->assertSame("Kommentar", $experiment->getComment());
    }

    public function testSafetySigns()
    {
        $experiment = new Experiment();
        $experiment->setSafetySigns(["1","3","5"]);
        $this->assertSame(["1","3","5"], $experiment->getSafetySigns());
    }

    public function testCommits()
    {
        $experiment = new Experiment();
        $commit = new Commit();
        $experiment->addCommit($commit);
        $this->assertSame($commit, $experiment->getCommits()->first());
        $experiment->removeCommit($commit);
        $this->assertEmpty($experiment->getCommits());
    }

    public function testGetId()
    {
        $experiment = new Experiment();
        $this->assertNull($experiment->getId());
    }
}
