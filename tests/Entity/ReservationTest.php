<?php

namespace App\Tests\Entity;

use App\Entity\Experiment;
use App\Entity\Reservation;
use App\Entity\ReservationExperiment;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class ReservationTest extends TestCase
{

    public function testStatus()
    {
        $reservation = new Reservation();
        $reservation->setStatus(100);
        $this->assertEquals(100, $reservation->getStatus());
    }

    public function testLectureDate()
    {
        $reservation = new Reservation();
        $time = new \DateTime('now');
        $reservation->setLectureDate($time);
        $this->assertEquals($time, $reservation->getLectureDate());
    }

    public function testOrdertime()
    {
        $reservation = new Reservation();
        $time = new \DateTime('now');
        $reservation->setOrdertime($time);
        $this->assertEquals($time, $reservation->getOrdertime());
    }

    public function testName()
    {
        $reservation = new Reservation();
        $reservation->setName("Reserviert");
        $this->assertEquals("Reserviert", $reservation->getName());
    }

    public function testReservationExperiment()
    {
        $reservation = new Reservation();
        $experiment = new ReservationExperiment();
        $reservation->addReservationExperiment($experiment);
        $this->assertEquals($experiment, $reservation->getReservationExperiments()->first());
        $reservation->removeReservationExperiment($experiment);
        $this->assertEmpty($reservation->getReservationExperiments());
    }

    public function testComment()
    {
        $reservation = new Reservation();
        $reservation->setComment("Reserviert");
        $this->assertEquals("Reserviert", $reservation->getComment());
    }

    public function testUser()
    {
        $reservation = new Reservation();
        $user = new User();
        $reservation->setUser($user);
        $this->assertEquals($user, $reservation->getUser());
    }

    public function testGetId()
    {
        $reservation = new Reservation();
        $this->assertNull($reservation->getID());
    }
}
