<?php

namespace App\Tests\Entity;

use App\Entity\Comment;
use App\Entity\Experiment;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class CommentTest extends TestCase
{

    public function testSubComment()
    {
        $comment = new Comment();
        $subcomment = new Comment();
        $comment->addComment($subcomment);
        $this->assertSame($subcomment, $comment->getComments()->first());
        $comment->removeComment($subcomment);
        $this->assertEmpty($comment->getComments());
    }

    public function testGetMessage()
    {
        $comment = new Comment();
        $comment->setMessage("Nachricht");
        $this->assertSame("Nachricht", $comment->getMessage());
    }

    public function testGetId()
    {
        $comment = new Comment();
        $this->assertNull($comment->getId());
    }



    public function testTimestamp()
    {
        $comment = new Comment();
        $time = new \DateTime('now');
        $comment->setTimestamp($time);
        $this->assertSame($time, $comment->getTimestamp());
    }

    public function testGetUser()
    {
        $comment = new Comment();
        $user = new User();
        $comment->setUser($user);
        $this->assertSame($user, $comment->getUser());
    }

    public function testGetParentComment()
    {
        $comment = new Comment();
        $parent = new Comment();
        $comment->setParentComment($parent);
        $this->assertEquals($parent, $comment->getParentComment());
    }

    public function testExperiment()
    {
        $comment = new Comment();
        $experiment = new Experiment();
        $comment->setExperiment($experiment);
        $this->assertSame($experiment, $comment->getExperiment());
    }
}
