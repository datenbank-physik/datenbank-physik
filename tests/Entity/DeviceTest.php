<?php

namespace App\Tests\Entity;

use App\Entity\Commit;
use App\Entity\Device;
use App\Entity\Label;
use App\Entity\Document;
use App\Entity\LocationMappings;
use PhpParser\Comment\Doc;
use PHPUnit\Framework\TestCase;

class DeviceTest extends TestCase
{

    public function testDescription()
    {
        $device = new Device();
        $device->setDescription("Beschreibung");
        $this->assertSame("Beschreibung", $device->getDescription());
    }

    public function testLabel()
    {
        $device = new Device();
        $label = new Label();
        $device->addLabel($label);
        $this->assertSame($label, $device->getLabels()->first());
        $device->removeLabel($label);
            $this->assertEmpty($device->getLabels());
    }

    public function testInventorynumber()
    {
        $device = new Device();
        $device->setInventorynumber(100);
        $this->assertSame(100, $device->getInventorynumber());
    }

    public function testGetId()
    {
        $device = new Device();
        $this->assertNull($device->getId());
    }

    public function testIfpcode()
    {
        $device = new Device();
        $device->setIfpcode("100");
        $this->assertSame("100", $device->getIfpcode());
    }

    public function testLoaned()
    {
        $device = new Device();
        $device->setLoaned(100);
        $this->assertSame(100, $device->getLoaned());
    }

    public function testSetName()
    {
        $device = new Device();
        $device->setName("Name");
        $this->assertSame("Name", $device->getName());
    }

    public function testGetLocationMappings()
    {
        $device = new Device();
        $location = new LocationMappings();
        $device->setLocationMappings($location);
        $this->assertSame($location, $device->getLocationMappings());
    }


    public function testSupplier()
    {
        $device = new Device();
        $device->setSupplier("Hersteller");
        $this->assertSame("Hersteller", $device->getSupplier());
    }

    public function testDocuments()
    {
        $device = new Device();
        $document = new Document();
        $device->addDocument($document);
        $this->assertSame($document, $device->getDocuments()->first());
        $device->removeDocument($document);
        $this->assertEmpty($device->getDocuments());
    }


    public function testloanedTos()
    {
        $device = new Device();
        $loanedto = new Document();
        $device->setLoanedTo($loanedto);
        $this->assertSame($loanedto, $device->getLoanedTo());
    }

    public function testsetLabels()
    {
        $device = new Device();
        $label = new Label();
                $device->setLabels($label);
        $this->assertTrue(true);
    }

    public function testAmount()
    {
        $device = new Device();
        $device->setAmount(100);
        $this->assertSame(100, $device->getAmount());
    }



    public function testCommit()
    {
        $device = new Device();
        $commit = new Commit();
        $device->addCommit($commit);
        $this->assertSame($commit, $device->getCommits()->first());
        $device->removeCommit($commit);
        $this->assertEmpty($device->getCommits());
    }

    public function testComment()
    {
        $device = new Device();
        $device->setComment("Kommentar");
        $this->assertSame("Kommentar", $device->getComment());
    }



    public function testRepair()
    {
        $device = new Device();
        $device->setRepair(100);
        $this->assertSame(100, $device->getRepair());
    }




    public function testOrdernumber()
    {
        $device = new Device();
        $device->setOrdernumber("4711");
        $this->assertSame("4711", $device->getOrdernumber());
    }
}
