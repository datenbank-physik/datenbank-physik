<?php

namespace App\Tests\Entity;

use App\Entity\Experiment;
use App\Entity\Reservation;
use App\Entity\ReservationExperiment;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class ReservationExperimentTest extends TestCase
{

    public function testPinnedOn()
    {
        $reservation = new ReservationExperiment();
        $time = new DateTime('now');
        $reservation->setPinnedOn($time);
        $this->assertEquals($time, $reservation->getPinnedOn());
    }

    public function testStatus()
    {
        $reservation = new ReservationExperiment();
        $reservation->setStatus(1);
        $this->assertEquals(1, $reservation->getStatus());
    }

    public function testReservation()
    {
        $reservation = new ReservationExperiment();
        $reservation_Entity = new Reservation();
        $reservation->setReservation($reservation_Entity);
        $this->assertEquals($reservation_Entity, $reservation->getReservation());
    }

    public function testUser()
    {
        $reservation = new ReservationExperiment();
        $user = new User;
        $reservation->setUser($user);
        $this->assertSame($user, $reservation->getUser());
    }

    public function testGetId()
    {
        $reservation = new ReservationExperiment();
        $this->assertNull($reservation->getId());
    }

    public function testGetExperiment()
    {
        $reservation = new ReservationExperiment();
        $experiment = new Experiment();
        $reservation->setExperiment($experiment);
        $this->assertSame($experiment, $reservation->getExperiment());
    }

    public function testGetStatus()
    {
        $reservation = new ReservationExperiment();
        $reservation->setStatus(100);
        $this->assertSame(100, $reservation->getStatus());
    }
}
