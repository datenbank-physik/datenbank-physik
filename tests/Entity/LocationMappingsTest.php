<?php

namespace App\Tests\Entity;

use App\Entity\Device;
use App\Entity\Location;
use App\Entity\LocationMappings;
use PHPUnit\Framework\TestCase;

class LocationMappingsTest extends TestCase
{

    public function testShelf()
    {
        $locationMapping = new LocationMappings();
        $locationMapping->setShelf("Test");
        $this->assertEquals("Test", $locationMapping->getShelf());
    }

    public function testDevice()
    {
        $locationMapping = new LocationMappings();
        $device = new Device();
        $locationMapping->setDevice($device);
        $this->assertEquals($device, $locationMapping->getDevice());
    }

    public function testLocation()
    {
        $locationMapping = new LocationMappings();
        $location = new Location();
        $locationMapping->setLocation($location);
        $this->assertEquals($location, $locationMapping->getLocation());
    }

    public function testGetId()
    {
        $locationMapping = new LocationMappings();
        $this->assertNull($locationMapping->getId());
    }
}
