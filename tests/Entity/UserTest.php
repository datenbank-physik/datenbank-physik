<?php

namespace App\Tests\Entity;

use App\Entity\Comment;
use App\Entity\Commit;
use App\Entity\Reservation;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;

class UserTest extends TestCase
{

    public function testEmail()
    {
        $user = new User();
        $user->setEmail("test@test.de");
        $this->assertEquals("test@test.de", $user->getEmail());
    }

    public function testGetSalt()
    {
        //we don't have a salt here but the function should work anyways
        $user = new User();
        $this->assertNull($user->getSalt());
    }

    public function testComments()
    {
        $user = new User();
        $comment = new Comment();
        $comment->setMessage("Blub");
        $user->addComment($comment);
        $this->assertSame($comment, $user->getComments()->first());
        $this->assertEquals($comment->getMessage(), $user->getComments()->first()->getMessage());
        $user->removeComment($comment);
        $this->assertEmpty($user->getComments());
    }

    public function testName()
    {
        $user = new User();
        $user->setName("Hans");
        $this->assertEquals("Hans", $user->getName());
        $this->assertEquals("Hans", $user->getUsername());
    }

    public function testEraseCredentials()
    {
        $user = new User();
        $user->eraseCredentials();
        $this->assertTrue(true); //Test is successfull if it doen't throw an error
    }

    public function testPassword()
    {

            $user = new User();
            $user->setPassword("Geheim");
            $this->assertEquals("Geheim", $user->getPassword());
    }

    public function testOrder()
    {
        $user = new User();
        $order = new Reservation();
        $user->addOrder($order);

        $this->assertSame($order, $user->getOrders()->first());
        $user->removeOrder($order);
        $this->assertEmpty($user->getOrders());
    }


    public function testSetRoles()
    {
        $user = new User();
        $user->setRoles(["ROLE_TEST"]);
        $this->assertEquals(["ROLE_TEST"], $user->getRoles());
    }



    public function testExperimentCommits()
    {
        $user = new User();
        $commit = new Commit();
        $user->addExperimentCommit($commit);
        $this->assertSame($commit, $user->getExperimentCommits()->first());
        $user->removeExperimentCommit($commit);
        $this->assertEmpty($user->getExperimentCommits());
    }

    public function testConfirmedExperimentCommits()
    {
        $user = new User();
        $commit = new Commit();
        $user->addConfirmedExperimentCommit($commit);
        $this->assertSame($commit, $user->getConfirmedExperimentCommits()->first());
        $user->removeConfirmedExperimentCommit($commit);
        $this->assertEmpty($user->getConfirmedExperimentCommits());
    }



    public function testGetId()
    {
        //we don't have an id here but the function should work anyways
        $user = new User();
        $this->assertNull($user->getID());
    }
}
