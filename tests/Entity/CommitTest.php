<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Commit;
use App\Entity\Device;
use App\Entity\Experiment;
use App\Entity\Label;
use App\Entity\Location;
use App\Entity\User;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Extension\DependencyInjection\DependencyInjectionExtension;

class CommitTest extends TestCase
{

    public function testConfirmedBy()
    {
        $commit = new Commit();
        $user = new User();
        $commit->setConfirmedBy($user);
        $this->assertSame($user, $commit->getConfirmedBy());
    }

    public function testLocation()
    {
        $commit = new Commit();
        $location = new Location();
        $commit->setLocation($location);
        $this->assertSame($location, $commit->getLocation());
    }

    public function testData()
    {
        $commit = new Commit();
        $commit->setData(["array"]);
        $this->assertSame(["array"], $commit->getData());
    }

    public function testExperiment()
    {
        $commit = new Commit();
        $experiment = new Experiment();
        $commit->setExperiment($experiment);
        $this->assertSame($experiment, $commit->getExperiment());
    }

    public function testField()
    {
        $commit = new Commit();
        $commit->setField("Feld");
        $this->assertSame("Feld", $commit->getField());
    }

    public function testTimestamp()
    {
        $commit = new Commit();
        $time = new \DateTime("now");
        $commit->setTimestamp($time);
        $this->assertSame($time, $commit->getTimestamp());
    }

    public function testAuthor()
    {
        $commit = new Commit();
        $user = new User();
        $commit->setAuthor($user);
        $this->assertSame($user, $commit->getAuthor());
    }

    public function testDevice()
    {
        $commit = new Commit();
        $device = new Device();
        $commit->setDevice($device);
        $this->assertSame($device, $commit->getDevice());
    }

    public function testLabel()
    {
        $commit = new Commit();
        $label = new Label();
        $commit->setLabel($label);
        $this->assertSame($label, $commit->getLabel());
    }

    public function testRejected()
    {
        $commit = new Commit();
        $commit->setRejected(true);
        $this->assertTrue($commit->getRejected());
    }

    public function testCategory()
    {
        $commit = new Commit();
        $category = new Category();
        $commit->setCategory($category);
        $this->assertSame($category, $commit->getCategory());
    }

    public function testConfirmedAt()
    {
        $commit = new Commit();
        $time = new \DateTime("now");
        $commit->setConfirmedAt($time);
        $this->assertSame($time, $commit->getConfirmedAt());
    }

    public function testGetId()
    {
        $commit = new Commit();
        $this->assertNull($commit->getId());
    }
}
