<?php

namespace App\Tests\Entity;

use App\Entity\Device;
use App\Entity\Document;
use App\Entity\Experiment;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\File;

class DocumentTest extends TestCase
{

    public function testUpdatedAt()
    {
        $document = new Document();
        $document->setUpdatedAt("2020");
        $this->assertSame("2020", $document->getUpdatedAt());
    }

    public function testImageFile()
    {
        $document = new Document();
        $file = new File("test", false);
        $document->setImageFile($file);
        $this->assertSame($file, $document->getImageFile());
    }

    public function testIsMarked()
    {
        $document = new Document();
        $document->setMarked(true);
        $this->assertIsBool($document->isMarked());
    }

    public function testName()
    {
        $document = new Document();
        $document->setName("Name");
        $this->assertSame("Name", $document->getName());
    }

    public function testDevice()
    {
        $document = new Document();
        $device = new Device();
        $document->setDevice($device);
        $this->assertSame($device, $document->getDevice());
    }

    public function testComment()
    {
        $document = new Document();
        $document->setComment("Kommentar");
        $this->assertSame("Kommentar", $document->getComment());
    }

    public function testPosition()
    {
        $document = new Document();
        $document->setPosition(100);
        $this->assertSame(100, $document->getPosition());
    }

    public function testExperiment()
    {
        $document = new Document();
        $experiment = new Experiment();
        $document->setExperiment($experiment);
        $this->assertSame($experiment, $document->getExperiment());
    }

    public function testImageName()
    {
        $document = new Document();
        $document->setImageName("Name");
        $this->assertSame("Name", $document->getImageName());
    }

    public function testgetID()
    {
        $document = new Document();
        $this->assertEmpty($document->getId());
    }

    public function testTyp()
    {
        $document = new Document();
        $document->setTyp("Bild");
        $this->assertSame("Bild", $document->getTyp());
    }

    public function testImageSize()
    {
        $document = new Document();
        $document->setImageSize(100);
        $this->assertSame(100, $document->getImageSize());
    }
}
