<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Commit;
use App\Entity\Experiment;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase
{

    public function testToken()
    {
        $category = new Category();
        $category->setToken("T");
        $this->assertEquals("T", $category->getToken());
    }

    public function testParent()
    {
        $category = new Category();
        $parent = new Category();
        $category->setParent($parent);
        $this->assertEquals($parent, $category->getParent());
    }

    public function testGetExperiments()
    {
        $category = new Category();
        $experiment = new Experiment();
        $category->addExperiment($experiment);
        $this->assertEquals($experiment, $category->getExperiments()->first());
        $category->removeExperiment($experiment);
        $this->assertEmpty($category->getExperiments());
    }


    public function testName()
    {
        $category = new Category();
        $category->setName("Name");
        $this->assertEquals("Name", $category->getName());
    }

    public function testChildren()
    {
        $category = new Category();
        $child = new Category();
        $category->addChild($child);
        $this->assertEquals($child, $category->getChildren()->first());
        $category->removeChild($child);
        $this->assertEmpty($category->getChildren());
    }

    public function testGetId()
    {
        $category = new Category();
        $this->assertNull($category->getId());
    }


    public function testGetCommits()
    {
        $category = new Category();
        $commit = new Commit();
        $category->addCommit($commit);
        $this->assertEquals($commit, $category->getCommits()->first());
        $category->removeCommit($commit);
        $this->assertEmpty($category->getCommits());
    }
}
