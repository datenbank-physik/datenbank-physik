<?php

namespace App\Tests\Entity;

use App\Entity\Commit;
use App\Entity\Device;
use App\Entity\Experiment;
use App\Entity\Label;
use PHPUnit\Framework\TestCase;

class LabelTest extends TestCase
{

    public function testCommit()
    {
        $label = new Label();
        $commit = new Commit();
        $label->addCommit($commit);
        $this->assertSame($commit, $label->getCommits()->first());
        $label->removeCommit($commit);
        $this->assertEmpty($label->getCommits());
    }

    public function testDevice()
    {
        $label = new Label();
        $device = new Device();
        $label->addDevice($device);
        $this->assertSame($device, $label->getDevices()->first());
        $label->removeDevice($device);
        $this->assertEmpty($label->getDevices());
    }

    public function testExperiment()
    {
        $label = new Label();
        $experiment = new Experiment();
        $label->addExperiment($experiment);
        $this->assertSame($experiment, $label->getExperiments()->first());
        $label->removeExperiment($experiment);
        $this->assertEmpty($label->getExperiments());
    }

    public function testName()
    {
        $label = new Label();
        $label->setName("Name");
        $this->assertSame("Name", $label->getName());
    }



    public function testGetId()
    {
        $label = new Label();
        $this->assertNull($label->getId());
    }



    public function testColor()
    {
        $label = new Label();
        $label->setColor("Color");
        $this->assertSame("Color", $label->getColor());
    }
}
