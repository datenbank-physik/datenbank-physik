<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SystemTest extends WebTestCase
{

    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    //This Test covers a workflow of adding a new Device, adding it to new Label and then creating a new Experiment with
    //that Label. The Changes should be visible afterwards in the Commit-Page and the new Experiment is shown on the
    //Front Page. In the device and experiment listing both are shown accordingly.
    public function testWorkflowNewDeviceAndExperiment()
    {
        //New Label
        $this->client->restart();
        $this->logIn(['ROLE_ADMIN']);
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('GET', '/labels/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['label[name]'] = 'Neues Label';
        $form['label[color]'] = '#FF2601';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device', 302);

        //New Device
        $crawler = $this->client->request('GET', '/device/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[name]'] = 'Systemtestgerät';
        $form['device[labels]'] = '[{"value":"Neues Label"}]';
        $form['device[locationMappings][location]'] = '1';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device', 302);

        $crawler = $this->client->request('GET', '/experiment/create');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[name]'] = 'Systemtestexperiment';
        $form['experiment[description]'] = 'Die Beschreibung';
        $form['experiment[comment]'] = 'Der Kommentar';
        $form['experiment[category]'] = '2';
        $form['experiment[status]'] = 'available';
        $form['experiment[experimentNumber]'] = '4711';
        $form['experiment[labels]'] = '[{"value":"Neues Label"}]';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment', 302);

        //New Experiment is shown on the Welcome Page
        $crawler = $this->client->request('GET', '/');
        $this->assertContains("Systemtestexperiment", $crawler->html());

        //New Experiment is shown in the right category
        $crawler = $this->client->request('GET', '/experiment/category/1');
        $this->assertContains("Systemtestexperiment", $crawler->html());

        //New Device is shown in device overview
        $crawler = $this->client->request('GET', '/device');
        $this->assertContains("Systemtestgerät", $crawler->html());

        //and we can open the new Device
        $link = $link = $crawler
        ->filter('a:contains("Anzeigen")')
        ->eq(1) //second is new one
        ->link()
        ;
        $crawler = $this->client->click($link);
        $this->assertContains("Systemtestgerät", $crawler->html());
        $this->assertContains("Neues Label", $crawler->html());
        //go to experimente tab
        $link = $link = $crawler
        ->filter('a:contains("Experimente")')
        ->eq(1) //second link because we have the big "Experimente" button but we are searching for the tab
        ->link()
        ;
        $crawler = $this->client->click($link);
        $this->assertContains("Systemtestexperiment", $crawler->html());
        $this->assertContains("Neues Label", $crawler->html());

        //Now go to the new experiment
        $crawler = $this->client->request('GET', '/experiment/category/1');
        $this->assertContains("Systemtestexperiment", $crawler->html());
        //and we can open the new Experiment
        $link = $link = $crawler
        ->filter('a:contains("Anzeigen")')
        ->eq(1)
        ->link()
        ;
        $crawler = $this->client->click($link);
        $this->assertContains("Systemtestexperiment", $crawler->html());
        //go to experimente tab
        $link = $link = $crawler
        ->filter('a:contains("Geräte")')
        ->eq(0)
        ->link()
        ;
        $crawler = $this->client->click($link);
        $this->assertContains("Systemtestgerät", $crawler->html());
        $this->assertContains("Neues Label", $crawler->html());


        //everything is shown in the Changes Tab
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('GET', '/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Gerät: Systemtestgerät", $crawler->html());
        $this->assertContains("Experiment: Systemtestexperiment", $crawler->html());
    }

    //When Experiments are edited all edits show up on the Commit-Page, but only some on the release page
    public function testWorkflowEditExperiment()
    {
        //first sign
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Change experiment
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[description]'] = 'Andere Beschreibung';
        $form['experiment[preparationTime]'] = '10000';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //On the commit page, every change shows up!
        $crawler = $this->client->request('GET', '/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Experiment: Testexperiment", $crawler->html());
        $this->assertContains("Beschreibung", $crawler->html());
        $this->assertContains("Änderte die <strong>Vorbereitungszeit</strong> von <strong>60</strong> auf <strong>10000", $crawler->html());

        //But Not on the release Page
        $crawler = $this->client->request('GET', '/releases');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertNotContains("Vorbereitungszeit", $crawler->html());
        $this->assertNotContains("10000", $crawler->html());
        $this->assertContains("description", $crawler->html()); //shows up because we changed it and it needs approval!
    }

//When Devices are edited all edits show up on the Commit-Page, but only some on the release page
    public function testWorkflowEditDevice()
    {
        //first sign
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Change experiment
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[description]'] = 'Andere Beschreibung';
        $form['device[supplier]'] = 'Testhersteller';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //On the commit page, every change shows up!
        $crawler = $this->client->request('GET', '/commits');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains("Gerät: Testgerät", $crawler->html());
        $this->assertContains("Beschreibung", $crawler->html());
        $this->assertContains(" <strong>Hersteller</strong> <strong>Testhersteller</strong>", $crawler->html());

        //But Not on the release Page
        $crawler = $this->client->request('GET', '/releases');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertNotContains("supplier", $crawler->html());
        $this->assertNotContains("Testhersteller", $crawler->html());
        $this->assertContains("description", $crawler->html()); //shows up because we changed it and it needs approval
    }

    //Check that new Comments are shown on the Comment overview page
    public function testAddComment()
    {

        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        $crawler = $this->client->request('GET', '/experiment/1/comments');
        $form = $crawler->selectButton('Kommentieren')->form();
        $form['message'] = 'Neuer Kommentar';
        $crawler = $this->client->submit($form);
        $crawler = $this->client->request('GET', '/experiment/1/comments');
        $this->assertContains('Neuer Kommentar', $crawler->html());

        //And now the comment should show on the Overview Page
        $crawler = $this->client->request('GET', '/comments');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Neuer Kommentar', $crawler->html());
    }

    //unavailable Experiments should only be visible for certain roles. Tested here, since not really security relevant
    public function testAvailability()
    {
        $this->client->restart();
        //At first experiment is visible for everybody
        $crawler = $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());
        //New Experiment is shown on the Welcome Page
        $crawler = $this->client->request('GET', '/');
        $this->assertContains("Testexperiment", $crawler->html());


        //Now sign in and change status
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());

        //Now change status
        $crawler = $this->client->request('GET', '/experiment/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['experiment[status]'] = 'notavailable';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/experiment/1/description', 302);

        //we can still see the experiment
        $crawler = $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $crawler->html());

        //now "log out", we can't see the Experiment anymore
        $this->client->restart();
        $crawler = $this->client->request('GET', '/experiment/category/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertNotContains('Testexperiment', $crawler->html());
        //And it is no longer on the Welcome
        //New Experiment is shown on the Welcome Page
        $crawler = $this->client->request('GET', '/');
        $this->assertNotContains("Testexperiment", $crawler->html());
    }

    public function testSearch()
    {
        //Modify device to our need
        $this->client->restart();
        $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->clickLink('Login');
        $form = $crawler->selectButton('Login')->form();
        $form['name'] = 'Admin';
        $form['password'] = '123456';
        $crawler = $this->client->submit($form);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', '/device/1/description');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testgerät', $crawler->html());
        $this->assertContains('Keine Bemerkung vorhanden', $crawler->html());
        $this->assertContains('Beschreibung', $crawler->html());

        //Now change the values
        //Note: Not everything is changed as e.g. adding Labels is tested elsewhere
        $crawler = $this->client->request('GET', '/device/1/edit');
        $form = $crawler->selectButton('Speichern')->form();
        $form['device[ifpcode]'] = 'Blub';
        $form['device[inventorynumber]'] = '11880';
        $crawler = $this->client->submit($form);
        $this->assertResponseRedirects('/device/1', 302);

        //Search Experiments
        $crawler = $this->client->request('GET', '/search/experiments/Testexperiment');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testexperiment', $this->client->getResponse()->getContent());

        //Search Devices
        $this->logIn(["ROLE_ADMIN"]);
        $crawler = $this->client->request('GET', '/search/devices/Testger%C3%A4t');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testger\\u00e4t<\\', $this->client->getResponse()->getContent());

        //Search Devices (loaned)
        $this->logIn(["ROLE_ADMIN"]);
        $crawler = $this->client->request('GET', '/search/devices/Ausgeliehen');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Keine Ergebnisse', $this->client->getResponse()->getContent());


        //Search Devices (in repair)
        $this->logIn(["ROLE_ADMIN"]);
        $crawler = $this->client->request('GET', '/search/devices/Reparatur');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Keine Ergebnisse', $this->client->getResponse()->getContent());

        //Search Devices (ID)
        $this->logIn(["ROLE_ADMIN"]);
        $crawler = $this->client->request('GET', '/search/id/1');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testger\\u00e4t<\\', $this->client->getResponse()->getContent());

        //Search Devices (shelf)
        $this->logIn(["ROLE_ADMIN"]);
        $crawler = $this->client->request('GET', '/search/aftershelf/Testregal');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testger\\u00e4t<\\', $this->client->getResponse()->getContent());

        //Search Devices (IFPCode)
        $this->logIn(["ROLE_ADMIN"]);
        $crawler = $this->client->request('GET', '/search/ifpcode/Blub');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testger\\u00e4t<\\', $this->client->getResponse()->getContent());

        //Search Devices (Inventorynumber)
        $this->logIn(["ROLE_ADMIN"]);
        $crawler = $this->client->request('GET', '/search/inventorynumber/11880');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Testger\\u00e4t<\\', $this->client->getResponse()->getContent());
    }

    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
