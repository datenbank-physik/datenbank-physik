<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class PageStructureTest extends WebTestCase
{
    private $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testFrontPage()
    {
        $this->client->request('GET', '/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('html h1', 'Datenbank Physik');
        $crawler = $this->client->clickLink('Experimente');
        $this->assertResponseIsSuccessful();
        $this->client->request('GET', '/');
        $crawler = $this->client->clickLink('Login');
        $this->assertResponseIsSuccessful();
    }

    public function testVersionhistory()
    {
        $this->client->request('GET', '/version');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('html h1', 'Versionen');
    }

    public function testExperiments()
    {
        $this->client->request('GET', '/experiment');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('html h1', 'Experimente');
    }

    public function testDevices()
    {
        $this->client->request('GET', '/device');
        //Device page should redirect to login
        $this->assertTrue(
            $this->client->getResponse()->isRedirect('/login')
        );

        //Now we should see them
        $this->logIn(['ROLE_VLA']);
        $crawler = $this->client->request('GET', '/device');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('html h1', 'Inventar');
        $this->assertContains("Testgerät", $crawler->html());
    }


    private function logIn($role)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';
        $firewallContext = 'main';

        $token = new UsernamePasswordToken('Admin', null, $firewallName, $role);
        $session->set('_security_'.$firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
