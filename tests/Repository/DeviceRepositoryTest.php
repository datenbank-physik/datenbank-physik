<?php

namespace App\Tests\Repository;

use App\Repository\DeviceRepository;
use App\Entity\Device;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DeviceRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearch()
    {
        $devices = $this->entityManager
            ->getRepository(Device::class)
            ->search("Testgerät");

        $this->assertCount(1, $devices);

        $devices = $this->entityManager
            ->getRepository(Device::class)
            ->search("Kein-Testgerät");

        $this->assertCount(0, $devices);
    }

    public function testFindAllWithDocumentRelations()
    {
        $devices = $this->entityManager
            ->getRepository(Device::class)
            ->findAllWithDocumentRelations();

        $this->assertCount(1, $devices);
    }

    public function testFindWithDocumentRelations()
    {
        //Search for Testgerät
        $devices = $this->entityManager
            ->getRepository(Device::class)
            ->findWithDocumentRelations(1);

        $this->assertCount(1, $devices);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
