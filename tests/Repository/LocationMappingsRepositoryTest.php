<?php

namespace App\Tests\Repository;

use App\Entity\Location;
use App\Entity\LocationMappings;
use App\Repository\LocationMappingsRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LocationMappingsRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }
    public function testFindByShelf()
    {
        $devices = $this->entityManager
            ->getRepository(LocationMappings::class)
            ->FindByShelf("Testregal");

        $this->assertCount(1, $devices);

        $devices = $this->entityManager
            ->getRepository(LocationMappings::class)
            ->FindByShelf("Kein-Testregal"); //This one doesn't exist

        $this->assertCount(0, $devices);
    }
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
