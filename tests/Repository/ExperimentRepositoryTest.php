<?php

namespace App\Tests\Repository;

use App\Repository\DeviceRepository;
use App\Entity\Experiment;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ExperimentRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearch()
    {
        $experiments = $this->entityManager
            ->getRepository(Experiment::class)
            ->search("Testexperiment", false);

        $this->assertCount(1, $experiments);

        $experiments = $this->entityManager
            ->getRepository(Experiment::class)
            ->search("Kein-Testexperiment", false);

        $this->assertCount(0, $experiments);
    }

    public function testFindAllWithDocumentRelations()
    {
        $experiments = $this->entityManager
            ->getRepository(Experiment::class)
            ->findAllWithDocumentRelations();

        $this->assertCount(1, $experiments);
    }


    public function testFindforCategory()
    {
        //Testexperiment is in category 2
        $experiments = $this->entityManager
            ->getRepository(Experiment::class)
            ->findForCategory("2", true);

        $this->assertCount(1, $experiments);

        //Category 3 doesn't have experiments in it
        $experiments = $this->entityManager
            ->getRepository(Experiment::class)
            ->findForCategory("3", true);

        $this->assertCount(0, $experiments);
    }

    public function testfindPrevNext()
    {
        //We only have one experiment so only one result
           $experiments = $this->entityManager
                ->getRepository(Experiment::class)
                ->findPrevNext(0);

        $this->assertCount(1, $experiments);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
