<?php

namespace App\Tests\Repository;

use App\Repository\CategoryRepository;
use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryRepositoryTest extends KernelTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testGetSortedCategories()
    {
        $categories = $this->entityManager
            ->getRepository(Category::class)
            ->getSortedCategories();
        //tWe have three categories
        $this->assertCount(3, $categories);
    }

    public function testGetSortedCategoriesArray()
    {
        $categories = $this->entityManager
            ->getRepository(Category::class)
            ->getSortedCategoriesArray();
        //We have one main category
        $this->assertCount(1, $categories);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
