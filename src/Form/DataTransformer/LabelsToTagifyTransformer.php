<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Form\DataTransformer;

use App\Entity\Label;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class LabelsToTagifyTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms a label array in a concatenated string.
     *
     * @param  Collection|Label[] $labels
     * @return string
     */
    public function transform($labels)
    {
        if ($labels->isEmpty()) {
            return '';
        }
        $getLabelName = function (Label $label) {
            return $label->getName();
        };

        return implode(', ', array_map($getLabelName, $labels->toArray()));
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $labelsJson
     * @return Label[]|object[]|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($labelsJson)
    {
        $label_names = json_decode($labelsJson, true);
        if (is_array($label_names)) {
            /*$replace_value_with_name = function ($entry) {
                return $entry['value'];
            };
            $labelNames = array_map($replace_value_with_name, $labelNames);
            $labels = $this->entityManager
                ->getRepository(Label::class)
                ->findBy(['name' => $labelNames]);*/
            $labels = [];
            foreach ($label_names as $label_name) {
                $label = $this->entityManager->getRepository(Label::class)->findOneBy(['name' => $label_name['value']]);
                if ($label === null) {
                    $label = new Label();
                    $label->setName($label_name['value']);
                }
                array_push($labels, $label);
            }
            return $labels;
        }
        return [];
    }

    public function getAllAvailableLabels()
    {
        return $this->entityManager->getRepository(Label::class)->findAll();
    }
}
