<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Form;

use App\Entity\Category;
use App\Entity\Experiment;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExperimentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', TextareaType::class, [
                'required' => false
            ])
            ->add('comment', TextareaType::class, [
                'required' => false
            ])
            ->add('preparationTime')
            ->add('execution_time')
            ->add('status', ChoiceType::class, [
                'choices'  => [
                    'Verfügbar' => 'available',
                    'Nicht Verfügbar' => 'notavailable',
                    'Entwurf' => 'draft'
                ],
                'data' => $options['choice'],
            ])
            ->add('safety_signs', SignSelectorType::class, [
                'label' => 'Sicherheitszeichen',
                'multiple' => true,
                'expanded' => true,
                'choices'  => [
                    'W001' => 'W001',
                    'W002' => 'W002',
                    'W003' => 'W003',
                    'W004' => 'W004',
                    'W005' => 'W005',
                    'W006' => 'W006',
                    'W009' => 'W009',
                    'W010' => 'W010',
                    'W012' => 'W012',
                    'W016' => 'W016',
                    'W017' => 'W017',
                    'W021' => 'W021',
                    'W022' => 'W022',
                    'W023' => 'W023',
                    'W027' => 'W027',
                    'W028' => 'W028',
                    'W029' => 'W029',
                    'M001' => 'M001',
                    'M003' => 'M003',
                    'M004' => 'M004',
                    'M005' => 'M005',
                    'M007' => 'M007',
                    'M008' => 'M008',
                    'M009' => 'M009',
                    'M011' => 'M011',
                    'M013' => 'M013',
                    'M017' => 'M017',
                    'M026' => 'M026',
                ],
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => function ($category) {
                    return $category->getToken(). ': ' .$category->getName();
                },
                'choice_value' => 'id',
                'choices' => $options['availableCategories'],
                'group_by' => function ($value, $key, $index) {
                    return mb_substr($value->getToken(), 0, 1);
                },
            ])
            ->add('experimentNumber')
            ->add('labels', TagifyType::class, [
                'label' => 'Gerätelabels'
            ])
            ->add('documents', CollectionType::class, [
                'entry_type' => DocumentType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'attr' => [
                    'data-entry-label' => 'Document'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Experiment::class,
            'availableCategories' => Category::class,
            'choice' => 'draft'
        ]);
    }
}
