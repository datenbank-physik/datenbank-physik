<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200317150635 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, token VARCHAR(255) DEFAULT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, parent_comment_id INT DEFAULT NULL, experiment_id INT NOT NULL, user_id INT NOT NULL, message LONGTEXT NOT NULL, timestamp DATETIME NOT NULL, INDEX IDX_9474526CBF2AF943 (parent_comment_id), INDEX IDX_9474526CFF444C8 (experiment_id), INDEX IDX_9474526CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commit (id INT AUTO_INCREMENT NOT NULL, experiment_id INT DEFAULT NULL, author_id INT DEFAULT NULL, confirmed_by_id INT DEFAULT NULL, device_id INT DEFAULT NULL, location_id INT DEFAULT NULL, label_id INT DEFAULT NULL, category_id INT DEFAULT NULL, field VARCHAR(255) NOT NULL, data JSON DEFAULT NULL, timestamp DATETIME NOT NULL, confirmed_at DATETIME DEFAULT NULL, rejected TINYINT(1) DEFAULT NULL, INDEX IDX_4ED42EADFF444C8 (experiment_id), INDEX IDX_4ED42EADF675F31B (author_id), INDEX IDX_4ED42EAD6F45385D (confirmed_by_id), INDEX IDX_4ED42EAD94A4C7D4 (device_id), INDEX IDX_4ED42EAD64D218E (location_id), INDEX IDX_4ED42EAD33B92F39 (label_id), INDEX IDX_4ED42EAD12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device (id INT AUTO_INCREMENT NOT NULL, loaned_to_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, inventorynumber INT DEFAULT NULL, description LONGTEXT DEFAULT NULL, comment LONGTEXT DEFAULT NULL, amount INT NOT NULL, ordernumber VARCHAR(255) DEFAULT NULL, supplier VARCHAR(255) DEFAULT NULL, ifpcode VARCHAR(255) DEFAULT NULL, loaned INT NOT NULL, repair INT NOT NULL, UNIQUE INDEX UNIQ_92FB68ED3187113 (loaned_to_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device_label (device_id INT NOT NULL, label_id INT NOT NULL, INDEX IDX_C822B08F94A4C7D4 (device_id), INDEX IDX_C822B08F33B92F39 (label_id), PRIMARY KEY(device_id, label_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, experiment_id INT DEFAULT NULL, device_id INT DEFAULT NULL, image_name VARCHAR(255) DEFAULT NULL, image_size INT DEFAULT NULL, updated_at DATETIME DEFAULT NULL, comment LONGTEXT DEFAULT NULL, typ VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, marked TINYINT(1) DEFAULT \'0\' NOT NULL, position INT DEFAULT 1 NOT NULL, INDEX IDX_D8698A76FF444C8 (experiment_id), INDEX IDX_D8698A7694A4C7D4 (device_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE experiment (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, comment LONGTEXT DEFAULT NULL, preparation_time INT DEFAULT NULL, status VARCHAR(255) NOT NULL, execution_time INT DEFAULT NULL, safety_signs JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\', experiment_number INT NOT NULL, INDEX IDX_136F58B212469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE experiment_label (experiment_id INT NOT NULL, label_id INT NOT NULL, INDEX IDX_A6DB707CFF444C8 (experiment_id), INDEX IDX_A6DB707C33B92F39 (label_id), PRIMARY KEY(experiment_id, label_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE label (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, color VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, roomnumber VARCHAR(255) DEFAULT NULL, building VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location_mappings (id INT AUTO_INCREMENT NOT NULL, device_id INT NOT NULL, location_id INT NOT NULL, shelf VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_C77F61FB94A4C7D4 (device_id), INDEX IDX_C77F61FB64D218E (location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, ordertime DATETIME DEFAULT NULL, status INT NOT NULL, lecture_date DATETIME DEFAULT NULL, comment LONGTEXT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_42C84955A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation_experiment (id INT AUTO_INCREMENT NOT NULL, reservation_id INT DEFAULT NULL, experiment_id INT NOT NULL, user_id INT NOT NULL, status INT NOT NULL, pinned_on DATETIME NOT NULL, INDEX IDX_73C59106B83297E7 (reservation_id), INDEX IDX_73C59106FF444C8 (experiment_id), INDEX IDX_73C59106A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D6495E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CBF2AF943 FOREIGN KEY (parent_comment_id) REFERENCES comment (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CFF444C8 FOREIGN KEY (experiment_id) REFERENCES experiment (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EADFF444C8 FOREIGN KEY (experiment_id) REFERENCES experiment (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EADF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EAD6F45385D FOREIGN KEY (confirmed_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EAD94A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EAD64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EAD33B92F39 FOREIGN KEY (label_id) REFERENCES label (id)');
        $this->addSql('ALTER TABLE commit ADD CONSTRAINT FK_4ED42EAD12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68ED3187113 FOREIGN KEY (loaned_to_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE device_label ADD CONSTRAINT FK_C822B08F94A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device_label ADD CONSTRAINT FK_C822B08F33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A76FF444C8 FOREIGN KEY (experiment_id) REFERENCES experiment (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A7694A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE experiment ADD CONSTRAINT FK_136F58B212469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE experiment_label ADD CONSTRAINT FK_A6DB707CFF444C8 FOREIGN KEY (experiment_id) REFERENCES experiment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE experiment_label ADD CONSTRAINT FK_A6DB707C33B92F39 FOREIGN KEY (label_id) REFERENCES label (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE location_mappings ADD CONSTRAINT FK_C77F61FB94A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('ALTER TABLE location_mappings ADD CONSTRAINT FK_C77F61FB64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE reservation_experiment ADD CONSTRAINT FK_73C59106B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE reservation_experiment ADD CONSTRAINT FK_73C59106FF444C8 FOREIGN KEY (experiment_id) REFERENCES experiment (id)');
        $this->addSql('ALTER TABLE reservation_experiment ADD CONSTRAINT FK_73C59106A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EAD12469DE2');
        $this->addSql('ALTER TABLE experiment DROP FOREIGN KEY FK_136F58B212469DE2');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CBF2AF943');
        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EAD94A4C7D4');
        $this->addSql('ALTER TABLE device_label DROP FOREIGN KEY FK_C822B08F94A4C7D4');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A7694A4C7D4');
        $this->addSql('ALTER TABLE location_mappings DROP FOREIGN KEY FK_C77F61FB94A4C7D4');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68ED3187113');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CFF444C8');
        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EADFF444C8');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A76FF444C8');
        $this->addSql('ALTER TABLE experiment_label DROP FOREIGN KEY FK_A6DB707CFF444C8');
        $this->addSql('ALTER TABLE reservation_experiment DROP FOREIGN KEY FK_73C59106FF444C8');
        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EAD33B92F39');
        $this->addSql('ALTER TABLE device_label DROP FOREIGN KEY FK_C822B08F33B92F39');
        $this->addSql('ALTER TABLE experiment_label DROP FOREIGN KEY FK_A6DB707C33B92F39');
        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EAD64D218E');
        $this->addSql('ALTER TABLE location_mappings DROP FOREIGN KEY FK_C77F61FB64D218E');
        $this->addSql('ALTER TABLE reservation_experiment DROP FOREIGN KEY FK_73C59106B83297E7');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CA76ED395');
        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EADF675F31B');
        $this->addSql('ALTER TABLE commit DROP FOREIGN KEY FK_4ED42EAD6F45385D');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955A76ED395');
        $this->addSql('ALTER TABLE reservation_experiment DROP FOREIGN KEY FK_73C59106A76ED395');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE commit');
        $this->addSql('DROP TABLE device');
        $this->addSql('DROP TABLE device_label');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE experiment');
        $this->addSql('DROP TABLE experiment_label');
        $this->addSql('DROP TABLE label');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE location_mappings');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE reservation_experiment');
        $this->addSql('DROP TABLE user');
    }
}
