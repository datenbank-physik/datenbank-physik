<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $ordertime;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lectureDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReservationExperiment", mappedBy="reservation")
     */
    private $reservationExperiments;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function __construct()
    {
        $this->reservationExperiments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdertime(): ?\DateTimeInterface
    {
        return $this->ordertime;
    }

    public function setOrdertime(\DateTimeInterface $ordertime): self
    {
        $this->ordertime = $ordertime;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLectureDate(): ?\DateTimeInterface
    {
        return $this->lectureDate;
    }

    public function setLectureDate(\DateTimeInterface $lectureDate): self
    {
        $this->lectureDate = $lectureDate;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Collection|ReservationExperiment[]
     */
    public function getReservationExperiments(): Collection
    {
        return $this->reservationExperiments;
    }

    public function addReservationExperiment(ReservationExperiment $reservationExperiment): self
    {
        if (!$this->reservationExperiments->contains($reservationExperiment)) {
            $this->reservationExperiments[] = $reservationExperiment;
            $reservationExperiment->setReservation($this);
        }

        return $this;
    }

    public function removeReservationExperiment(ReservationExperiment $reservationExperiment): self
    {
        if ($this->reservationExperiments->contains($reservationExperiment)) {
            $this->reservationExperiments->removeElement($reservationExperiment);
            // set the owning side to null (unless already changed)
            if ($reservationExperiment->getReservation() === $this) {
                $reservationExperiment->setReservation(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
