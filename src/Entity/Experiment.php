<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExperimentRepository")
 * @Vich\Uploadable
 */
class Experiment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $preparationTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="experiments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="experiment", cascade={"persist", "remove"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="experiment", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Label", inversedBy="experiments", cascade={"persist"})
     */
    private $labels;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $execution_time;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $safety_signs = [];

    /**
     * @ORM\Column(type="integer")
     */
    private $experimentNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commit", mappedBy="experiment", orphanRemoval=true)
     */
    private $commits;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReservationExperiment", mappedBy="experiment", orphanRemoval=true)
     */
    private $reservationExperiments;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->labels = new ArrayCollection();
        $this->commits = new ArrayCollection();
        $this->reservationExperiments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getPreparationTime(): ?int
    {
        return $this->preparationTime;
    }

    public function setPreparationTime(?int $preparationTime): self
    {
        $this->preparationTime = $preparationTime;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $documents): self
    {
        if (!$this->documents->contains($documents)) {
            $this->documents[] = $documents;
            $documents->setExperiment($this);
        }

        return $this;
    }

    public function removeDocument(Document $documents): self
    {
        if ($this->documents->contains($documents)) {
            $this->documents->removeElement($documents);
            // set the owning side to null (unless already changed)
            if ($documents->getExperiment() === $this) {
                $documents->setExperiment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setExperiment($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getExperiment() === $this) {
                $comment->setExperiment(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|Label[]
     */
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    public function addLabel(Label $label): self
    {
        if (!$this->labels->contains($label)) {
            $this->labels[] = $label;
        }

        return $this;
    }

    public function removeLabel(Label $label): self
    {
        if ($this->labels->contains($label)) {
            $this->labels->removeElement($label);
        }

        return $this;
    }

    public function getExecutionTime(): ?int
    {
        return $this->execution_time;
    }

    public function setExecutionTime(?int $execution_time): self
    {
        $this->execution_time = $execution_time;

        return $this;
    }

    public function getSafetySigns(): ?array
    {
        return $this->safety_signs;
    }

    public function setSafetySigns(?array $safety_signs): self
    {
        $this->safety_signs = $safety_signs;

        return $this;
    }

    public function getExperimentNumber(): ?int
    {
        return $this->experimentNumber;
    }

    public function setExperimentNumber(int $experimentNumber): self
    {
        $this->experimentNumber = $experimentNumber;

        return $this;
    }

    /**
     * @return Collection|Commit[]
     */
    public function getCommits(): Collection
    {
        return $this->commits;
    }

    public function addCommit(Commit $commit): self
    {
        if (!$this->commits->contains($commit)) {
            $this->commits[] = $commit;
            $commit->setExperiment($this);
        }

        return $this;
    }

    public function removeCommit(Commit $commit): self
    {
        if ($this->commits->contains($commit)) {
            $this->commits->removeElement($commit);
            // set the owning side to null (unless already changed)
            if ($commit->getExperiment() === $this) {
                $commit->setExperiment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ReservationExperiment[]
     */
    public function getReservationExperiments(): Collection
    {
        return $this->reservationExperiments;
    }

    public function addReservationExperiment(ReservationExperiment $reservationExperiment): self
    {
        if (!$this->reservationExperiments->contains($reservationExperiment)) {
            $this->reservationExperiments[] = $reservationExperiment;
            $reservationExperiment->setExperiment($this);
        }

        return $this;
    }

    public function removeReservationExperiment(ReservationExperiment $reservationExperiment): self
    {
        if ($this->reservationExperiments->contains($reservationExperiment)) {
            $this->reservationExperiments->removeElement($reservationExperiment);
            // set the owning side to null (unless already changed)
            if ($reservationExperiment->getExperiment() === $this) {
                $reservationExperiment->setExperiment(null);
            }
        }

        return $this;
    }
}
