<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="Reservation", mappedBy="user", orphanRemoval=true)
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="user", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="Commit", mappedBy="author")
     */
    private $experimentCommits;

    /**
     * @ORM\OneToMany(targetEntity="Commit", mappedBy="confirmed_by")
     */
    private $confirmedExperimentCommits;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->experimentCommits = new ArrayCollection();
        $this->confirmedExperimentCommits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->name;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Reservation $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setUser($this);
        }

        return $this;
    }

    public function removeOrder(Reservation $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getUser() === $this) {
                $order->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commit[]
     */
    public function getExperimentCommits(): Collection
    {
        return $this->experimentCommits;
    }

    public function addExperimentCommit(Commit $experimentCommit): self
    {
        if (!$this->experimentCommits->contains($experimentCommit)) {
            $this->experimentCommits[] = $experimentCommit;
            $experimentCommit->setAuthor($this);
        }

        return $this;
    }

    public function removeExperimentCommit(Commit $experimentCommit): self
    {
        if ($this->experimentCommits->contains($experimentCommit)) {
            $this->experimentCommits->removeElement($experimentCommit);
            // set the owning side to null (unless already changed)
            if ($experimentCommit->getAuthor() === $this) {
                $experimentCommit->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commit[]
     */
    public function getConfirmedExperimentCommits(): Collection
    {
        return $this->confirmedExperimentCommits;
    }

    public function addConfirmedExperimentCommit(Commit $confirmedExperimentCommit): self
    {
        if (!$this->confirmedExperimentCommits->contains($confirmedExperimentCommit)) {
            $this->confirmedExperimentCommits[] = $confirmedExperimentCommit;
            $confirmedExperimentCommit->setConfirmedBy($this);
        }

        return $this;
    }

    public function removeConfirmedExperimentCommit(Commit $confirmedExperimentCommit): self
    {
        if ($this->confirmedExperimentCommits->contains($confirmedExperimentCommit)) {
            $this->confirmedExperimentCommits->removeElement($confirmedExperimentCommit);
            // set the owning side to null (unless already changed)
            if ($confirmedExperimentCommit->getConfirmedBy() === $this) {
                $confirmedExperimentCommit->setConfirmedBy(null);
            }
        }

        return $this;
    }
}
