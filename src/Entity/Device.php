<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 */
class Device
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $inventorynumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ordernumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $supplier;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\LocationMappings", mappedBy="device", cascade={"persist", "remove"})
     */
    private $locationMappings;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ifpcode;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="device", cascade={"persist", "remove"})
     */
    private $documents;

    /**
     * @ORM\Column(type="integer")
     */
    private $loaned;

    /**
     * @ORM\Column(type="integer")
     */
    private $repair;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Label", inversedBy="devices", cascade={"persist"})
     */
    private $labels;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     */
    private $loanedTo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commit", mappedBy="device", orphanRemoval=true)
     */
    private $commits;


    public function __construct()
    {
        $this->documents = new ArrayCollection();
        $this->labels = new ArrayCollection();
        $this->commits = new ArrayCollection();
    }

    public function setLabels($labels): self
    {
        return $this;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInventorynumber(): ?int
    {
        return $this->inventorynumber;
    }

    public function setInventorynumber(?int $inventorynumber): self
    {
        $this->inventorynumber = $inventorynumber;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getOrdernumber(): ?string
    {
        return $this->ordernumber;
    }

    public function setOrdernumber(?string $ordernumber): self
    {
        $this->ordernumber = $ordernumber;

        return $this;
    }

    public function getSupplier(): ?string
    {
        return $this->supplier;
    }

    public function setSupplier(?string $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getLocationMappings(): ?LocationMappings
    {
        return $this->locationMappings;
    }

    public function setLocationMappings(LocationMappings $locationMappings): self
    {
        $this->locationMappings = $locationMappings;

        // set the owning side of the relation if necessary
        if ($locationMappings->getDevice() !== $this) {
            $locationMappings->setDevice($this);
        }

        return $this;
    }

    public function getIfpcode(): ?string
    {
        return $this->ifpcode;
    }

    public function setIfpcode(?string $ifpcode): self
    {
        $this->ifpcode = $ifpcode;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $documents): self
    {
        if (!$this->documents->contains($documents)) {
            $this->documents[] = $documents;
            $documents->setDevice($this);
        }

        return $this;
    }

    public function removeDocument(Document $documents): self
    {
        if ($this->documents->contains($documents)) {
            $this->documents->removeElement($documents);
            // set the owning side to null (unless already changed)
            if ($documents->getDevice() === $this) {
                $documents->setDevice(null);
            }
        }

        return $this;
    }

    public function getLoaned(): ?int
    {
        return $this->loaned;
    }

    public function setLoaned(int $loaned): self
    {
        $this->loaned = $loaned;

        return $this;
    }

    public function getRepair(): ?int
    {
        return $this->repair;
    }

    public function setRepair(int $repair): self
    {
        $this->repair = $repair;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Label[]
     */
    public function getLabels(): Collection
    {
        return $this->labels;
    }

    public function addLabel(Label $label): self
    {
        if (!$this->labels->contains($label)) {
            $this->labels[] = $label;
        }

        return $this;
    }

    public function removeLabel(Label $label): self
    {
        if ($this->labels->contains($label)) {
            $this->labels->removeElement($label);
        }

        return $this;
    }

    public function getLoanedTo(): ?Document
    {
        return $this->loanedTo;
    }

    public function setLoanedTo(?Document $loanedTo): self
    {
        $this->loanedTo = $loanedTo;

        return $this;
    }

    /**
     * @return Collection|Commit[]
     */
    public function getCommits(): Collection
    {
        return $this->commits;
    }

    public function addCommit(Commit $commit): self
    {
        if (!$this->commits->contains($commit)) {
            $this->commits[] = $commit;
            $commit->setDevice($this);
        }

        return $this;
    }

    public function removeCommit(Commit $commit): self
    {
        if ($this->commits->contains($commit)) {
            $this->commits->removeElement($commit);
            // set the owning side to null (unless already changed)
            if ($commit->getDevice() === $this) {
                $commit->setDevice(null);
            }
        }

        return $this;
    }
}
