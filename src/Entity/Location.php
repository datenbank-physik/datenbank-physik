<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 */
class Location
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $roomnumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $building;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LocationMappings", mappedBy="location")
     */
    private $locationMappings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commit", mappedBy="location", orphanRemoval=true)
     */
    private $commits;

    public function __construct()
    {
        $this->locationMappings = new ArrayCollection();
        $this->commits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRoomnumber(): ?string
    {
        return $this->roomnumber;
    }

    public function setRoomnumber(?string $roomnumber): self
    {
        $this->roomnumber = $roomnumber;

        return $this;
    }

    public function getBuilding(): ?string
    {
        return $this->building;
    }

    public function setBuilding(string $building): self
    {
        $this->building = $building;

        return $this;
    }

    /**
     * @return Collection|LocationMappings[]
     */
    public function getLocationMappings(): Collection
    {
        return $this->locationMappings;
    }

    public function addLocationMapping(LocationMappings $locationMapping): self
    {
        if (!$this->locationMappings->contains($locationMapping)) {
            $this->locationMappings[] = $locationMapping;
            $locationMapping->setLocation($this);
        }

        return $this;
    }

    public function removeLocationMapping(LocationMappings $locationMapping): self
    {
        if ($this->locationMappings->contains($locationMapping)) {
            $this->locationMappings->removeElement($locationMapping);
            // set the owning side to null (unless already changed)
            if ($locationMapping->getLocation() === $this) {
                $locationMapping->setLocation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commit[]
     */
    public function getCommits(): Collection
    {
        return $this->commits;
    }

    public function addCommit(Commit $commit): self
    {
        if (!$this->commits->contains($commit)) {
            $this->commits[] = $commit;
            $commit->setLocation($this);
        }

        return $this;
    }

    public function removeCommit(Commit $commit): self
    {
        if ($this->commits->contains($commit)) {
            $this->commits->removeElement($commit);
            // set the owning side to null (unless already changed)
            if ($commit->getLocation() === $this) {
                $commit->setLocation(null);
            }
        }

        return $this;
    }
}
