<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Twig;

use cogpowered\FineDiff\Diff;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CommitExtension extends AbstractExtension
{
    //Note: Interaction with Template needs to be covered in Front End Test
    /**
     * @codeCoverageIgnore
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('diff', [$this, 'diff']),
            new TwigFunction('array_diff', [$this, 'arrayDiff']),
        ];
    }

    public function diff($old, $new)
    {
        $diff = new Diff;
        return $diff->render($old, $new);
    }

    public function arrayDiff($a, $b)
    {
        return array_diff($a, $b);
    }
}
