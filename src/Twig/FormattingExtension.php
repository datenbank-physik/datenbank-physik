<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Twig;

use ChrisKonnertz\BBCode\BBCode;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

//Note: Class covers visual styling elements for entered Text. Automatic Testing not useful, covered in Front End Tests
/**
 * @codeCoverageIgnore
 */

class FormattingExtension extends AbstractExtension
{
    //Note: Interaction with Template needs to be covered in Front End Test
    /**
     * @codeCoverageIgnore
     */
    public function getFilters()
    {
        return [
            new TwigFilter(
                'bbcode',
                [$this, 'bbcode'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    //Note: Interaction with Template needs to be covered in Front End Test
    /**
     * @codeCoverageIgnore
     */
    public function bbcode($text)
    {
        $bbcode = new BBCode();
        $bbcode->addTag('math', function ($tag, &$html, $openingTag) {
            if ($tag->opening) {
                return '\[';
            } else {
                return '\]';
            }
        });
        $bbcode->addTag('warning', function ($tag, &$html, $openingTag) {
            if ($tag->opening) {
                return '<div class="warning">';
            } else {
                return '</div>';
            }
        });
        $bbcode->addTag('info', function ($tag, &$html, $openingTag) {
            if ($tag->opening) {
                return '<div class="info">';
            } else {
                return '</div>';
            }
        });
        $rendered = $bbcode->render($text);
        return $rendered;
    }
}
