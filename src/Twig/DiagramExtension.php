<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Twig;

use ChrisKonnertz\BBCode\BBCode;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class DiagramExtension extends AbstractExtension
{
    //Note: Interaction with Template needs to be covered in Front End Test
    /**
     * @codeCoverageIgnore
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('duration_clock_minutes', [$this, 'durationClockMinutes']),
            new TwigFunction('duration_clock_hours', [$this, 'durationClockHours']),
        ];
    }

    //Note: Interaction with Template needs to be covered in Front End Test
    /**
     * @codeCoverageIgnore
     */
    public function getFilters()
    {
        return [
            new TwigFilter(
                'minuteInTime',
                [$this, 'minuteInTime'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    //Note: Interaction with Template needs to be covered in Front End Test
    /**
     * @codeCoverageIgnore
     */
    public function minuteInTime($var)
    {
        $res = '';
        if (($var % 60) != 0) {
            $res = $res.($var % 60).'min';
        }
        $var = floor($var/60);
        if (($var % 24) != 0) {
            $res = ($var % 24).'h ';
        }
        $var = floor($var/24);
        if ($var >= 1) {
            $res = $var.'d '.$res;
        }
        return $res;
    }

    private function isTrue($var)
    {
        if ($var) {
            return '1';
        } else {
            return '0';
        }
    }

    public function durationClockHours(float $cx, float $cy, float $duration, float $radius)
    {
        $duration = $duration % 720;
        $duration_h = floor($duration / 60) * 0.5236;
        return $this->isTrue($duration >= 360)
            .' 1 '
            .(sin($duration_h)*$radius+$cx)
            .' '
            .(-cos($duration_h)*$radius+$cy);
    }

    public function durationClockMinutes(bool $arc, bool $back, float $cx, float $cy, float $duration, float $radius)
    {
        if ($arc) {
            $res =  $this->isTrue($duration%60 >= 30).' '.$this->isTrue(!$back).' ';
        } else {
            $res = '';
        }
        if ($back) {
            return $res.$cx.' '.($cy-$radius);
        } else {
            return $res
            .(sin($duration*0.1047)*$radius+$cx)
            .' '
            .(-cos($duration*0.1047)*$radius+$cy);
        }
    }
}
