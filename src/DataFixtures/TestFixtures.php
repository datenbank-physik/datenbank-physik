<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Commit;
use App\Entity\Device;
use App\Entity\Document;
use App\Entity\Experiment;
use App\Entity\Label;
use App\Entity\Location;
use App\Entity\LocationMappings;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TestFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail("test@test.de");
        $user->setName("Admin");
        $password = $this->encoder->encodePassword($user, '123456');
        $user->setPassword($password);
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $user2 = new User();
        $user2->setEmail("test@test.de");
        $user2->setName("User");
        $password2 = $this->encoder->encodePassword($user, '123456');
        $user2->setPassword($password2);
        $user2->setRoles(['ROLE_USER']);
        $manager->persist($user2);

        $user3 = new User();
        $user3->setEmail("test@test.de");
        $user3->setName("Demo");
        $password3 = $this->encoder->encodePassword($user, '123456');
        $user3->setPassword($password3);
        $user3->setRoles(['ROLE_DEMO']);
        $manager->persist($user3);

        $user4 = new User();
        $user4->setEmail("test@test.de");
        $user4->setName("VLA");
        $password4 = $this->encoder->encodePassword($user, '123456');
        $user4->setPassword($password4);
        $user4->setRoles(['ROLE_VLA']);
        $manager->persist($user4);


        $category1 = new Category();
        $category1->setName("Test");
        $category1->setParent(null);
        $category1->setToken("T");
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setName("Test-Sub");
        $category2->setParent($category1);
        $category2->setToken("T");
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setName("To-Delete");
        $category3->setParent($category1);
        $category3->setToken("TD");
        $manager->persist($category3);


        $label = new Label();
        $label->setName("LabelzumEntfernen");

        $label2 = new Label();
        $label2->setName("Testlabel");

        $location = new Location();
        $location->setName("Testort");
        $location->setBuilding("Testgebäude");
        $location->setRoomnumber("4711");

        $locationMapping = new LocationMappings();
        $locationMapping->setLocation($location);
        $locationMapping->setShelf("Testregal");

        $device = new Device();
        $device->setName("Testgerät");
        $device->setDescription("Beschreibung");
        $device->addLabel($label);
        $device->setAmount(13);
        $device->setLoaned(0);
        $device->setRepair(0);
        $device->setLocationMappings($locationMapping);
        $locationMapping->setDevice($device);

        $experiment = new Experiment();
        $experiment->setName("Testexperiment");
        $experiment->setExperimentNumber(42);
        $experiment->setComment("Kommentar dazu");
        $experiment->setCategory($category2);
        $experiment->setDescription("Die Beschreibung");
        $experiment->setExecutionTime(100);
        $experiment->setPreparationTime(60);
        $experiment->setStatus("available");
        $experiment->addLabel($label);

        $commit = new Commit();
        $commit->setExperiment($experiment);
        $commit->setTimestamp(new \DateTime('now'));
        $commit->setAuthor($user);
        $commit->setField("Migration");
        $commit->setConfirmedAt(new \DateTime('now'));
        $commit->setConfirmedBy($user);
        $manager->persist($commit);



        $comment1 = new Comment();
        $comment1->setUser($user);
        $comment1->setTimestamp(new \DateTime('now'));
        $comment1->setMessage("Ein Kommentar!");
        $comment1->setExperiment($experiment);
        $comment2 = new Comment();
        $comment2->setUser($user);
        $comment2->setTimestamp(new \DateTime('now'));
        $comment2->setMessage("Eine Antwort!");
        $comment2->setParentComment($comment1);
        $comment2->setExperiment($experiment);
        $comment3 = new Comment();
        $comment3->setUser($user);
        $comment3->setTimestamp(new \DateTime('now'));
        $comment3->setMessage("Ein weiterer Kommentar!");
        $comment3->setExperiment($experiment);

        $experiment->addComment($comment1);
        $experiment->addComment($comment2);
        $experiment->addComment($comment3);
        $manager->persist($experiment);
        $manager->persist($device);
        $manager->persist($location);
        $manager->persist($locationMapping);
        $manager->persist($label);
        $manager->persist($label2);
        $manager->persist($comment1);
        $manager->persist($comment2);
        $manager->persist($comment3);

        $document = new Document();
        $manager->persist($document);

        $manager->flush();
    }
}
