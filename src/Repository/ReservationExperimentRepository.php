<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Repository;

use App\Entity\Reservation;
use App\Entity\ReservationExperiment;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ReservationExperiment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReservationExperiment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReservationExperiment[]    findAll()
 * @method ReservationExperiment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationExperimentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReservationExperiment::class);
    }

    public function getTopReservations()
    {
        return $this->createQueryBuilder('r')
            ->select('count(r.id) as count, r as reservation')
            ->leftJoin('r.experiment', 'e')
            ->groupBy('r.experiment')
            ->orderBy('count')
            ->setMaxResults(20)
            ->getQuery()
            ->getResult();
    }

    public function getReservations(User $user)
    {
        return $this->createQueryBuilder('r')
            ->addSelect('e')
            ->where('r.status = 0')
            ->andWhere('r.user = :user')
            ->leftJoin('r.experiment', 'e')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return ReservationExperiment[] Returns an array of ReservationExperiment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReservationExperiment
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
