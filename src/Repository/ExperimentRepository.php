<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Repository;

use App\Entity\Experiment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Experiment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Experiment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Experiment[]    findAll()
 * @method Experiment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExperimentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Experiment::class);
    }

    public function findAllWithDocumentRelations()
    {
        return $this->createQueryBuilder('e')
            ->addSelect('d')
            ->addSelect('c')
            ->leftJoin('e.documents', 'd')
            ->leftJoin('e.category', 'c')
            ->orderBy('length(c.token)')
            ->addOrderBy('c.token', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //Note: This function can't be tested automatically
    /**
     * @codeCoverageIgnore
     */
    public function findWithDocumentRelations($value)
    {
        return $this->createQueryBuilder('e')
            ->addSelect('d')
            ->leftJoin('e.documents', 'd')
            ->where('e.id = :val')
            ->setParameter('val', $value)
            ->orderBy('length(c.token)')
            ->addOrderBy('c.token', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Returns all experiments for the given Category numbers.
     *
     * @param int[] $value
     * @param boolean $showOnlyAvailable
     * @return Experiment[]|null
     */
    public function findForCategory($value, $showOnlyAvailable)
    {
        $query = $this->createQueryBuilder('e')
            ->where('e.category IN (:val)')
            ->leftJoin('e.category', 'c')
            ->setParameter('val', $value);

        if ($showOnlyAvailable) {
            $query->andWhere('e.status = \'available\'');
        }

        return $query->orderBy('length(c.token)')
            ->addOrderBy('c.token', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $value
     * @param boolean $showOnlyAvailable
     * @return mixed Returns an array of Experiment objects
     */
    public function search($value, $showOnlyAvailable)
    {
        $query = $this->createQueryBuilder('e')
            ->leftJoin('e.category', 'c')
            ->andWhere('e.name LIKE :val')
            ->orWhere('e.description LIKE :val')
            ->orWhere('e.comment LIKE :val')
            ->orWhere('concat(c.token, \'.\', e.experimentNumber) LIKE :val')
            ->setParameter('val', $value);

        if ($showOnlyAvailable) {
            $query->andWhere('e.status = \'available\'');
        }

        return $query->orderBy('c.token', 'ASC')
        ->getQuery()
        ->getResult()
        ;
    }

    public function findPrevNext($showOnlyAvailable)
    {
        $query=$this->createQueryBuilder('e')
            ->addSelect('c.token')
            ->addSelect('e.experimentNumber')
            ->innerJoin('e.category', 'c');

        if ($showOnlyAvailable) {
            $query->andWhere('e.status = \'available\'');
        }

            return $query->orderBy('length(c.token)')
            ->addOrderBy('c.token', 'ASC')
            ->addOrderBy('e.experimentNumber', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findLatest($limit)
    {
        return $this->createQueryBuilder('e')
            ->where('e.id IS NOT NULL')
            ->andWhere('e.status LIKE :val')
            ->setParameter('val', 'available')
            ->addOrderBy('e.id', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }
}
