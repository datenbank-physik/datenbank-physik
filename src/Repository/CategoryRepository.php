<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function getSortedCategories()
    {
        $rootCategories = $this->createQueryBuilder('c')
            ->orderBy('c.token', 'ASC');

        return $rootCategories->getQuery()->getResult();
    }

    public function getSortedCategoriesArray()
    {
        $rootCategories = $this->createQueryBuilder('c')
            ->addSelect('cc')
            ->where('c.parent IS NULL')
            ->leftJoin('c.children', 'cc')
            ->orderBy('c.token', 'ASC');

        $objects = $rootCategories->getQuery()->getResult();
        $array = $this->recursiveSort($this->buildArray($objects));

        return $array;
    }

    private function buildArray(array $categories)
    {
        $array = [];

        foreach ($categories as $category) {
            $array[] = [
                'id' => $category->getId(),
                'name' => $category->getName(),
                'token' => $category->getToken(),
                'children' => $this->buildArray($category->getChildren()->getValues()),
            ];
        }

        return $array;
    }

    private function recursiveSort(array $categories)
    {
        foreach ($categories as &$item) {
            if (!array_key_exists('children', $item)) {
                return $categories;
            }
            usort($item['children'], function ($a, $b) {
                return strnatcmp($a['token'], $b['token']);
            });
            $item['children'] = $this->recursiveSort($item['children']);
        }
        return $categories;
    }
}
