<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfileType;
use App\Form\ProfilType;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * Route that show all Users
     *
     * @return Response
     */
    public function showAll()
    {
        $this->denyAccessUnlessGranted('ROLE_USER_ADMIN');

        $user = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render('table.html.twig', [
            'user' => $user,
            'main_title' => "Nutzer",
            'page_buttons' => [["Neuer Nutzer", "user_create", "\icons\ic_person_add_24px.svg"]],
            'buttons' => [["Bearbeiten", "user_edit"],
                ["Löschen", "user_delete"]],
        ]);
    }

    /**
     * Route that handles the creation of a form for creating new Users as well as handling the response
     * from the form.
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder Uses DependecyInjection
     * @return RedirectResponse|Response
     */
    public function create(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->denyAccessUnlessGranted('ROLE_USER_ADMIN');

        $user  = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $user->setRoles([$form->get('roles')->getData()]);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user');
        }
        return $this->render(
            'form/default.html.twig',
            [
                'form' => $form->createView(),
                'main_title' => 'Neuen Nutzer anlegen',
                'page_buttons' => [["Nutzerübersicht", "user", "\icons\ic_group_32px.svg"]]
            ]
        );
    }

    /**
     * Route that handles the creation of a form for editing existing Users as well as handling the response
     * from the form.
     *
     * @param Request $request Request with the id of a user as 'id' parameter
     * @return RedirectResponse|Response
     */
    public function edit(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->denyAccessUnlessGranted('ROLE_USER_ADMIN');

        $id = $request->attributes->get('id');

        /** @var User|null $user */
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Dieser Nutzer existiert nicht!');
        }

        $form = $this->createForm(UserType::class, $user, [
            'role' => $user->getRoles()[0]
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            if ($form->get('password')->getData()) {
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('password')->getData()
                    )
                );
            }
            $user->setRoles([$form->get('roles')->getData()]);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user');
        }

        return $this->render('form/default.html.twig', [
            'form' => $form->createView(),
            'main_title' => 'Nutzer bearbeiten',
            'page_buttons' => [["Nutzerübersicht", "user", "\icons\ic_group_32px.svg"]],
        ]);
    }

    /**
     * Route that handles the deletion of the User with the matching id.
     *
     * @param Request $request Request with the id of a user as 'id' parameter
     * @return RedirectResponse
     */
    public function delete(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER_ADMIN');

        $id = $request->attributes->get('id');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Dieser Nutzer existiert nicht!');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute('user');
    }

    /**
     * Route that handles the creation of a form to allow users to edit their own profile.
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function editProfile(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(ProfileType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            if ($form->get('password')->getData()) {
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('password')->getData()
                    )
                );
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('form/default.html.twig', [
            'form' => $form->createView(),
            'main_title' => 'Profil bearbeiten',
            'page_buttons' => [["Abbrechen", "index"]],
        ]);
    }
}
