<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Commit;
use App\Entity\Experiment;
use App\Entity\Label;
use App\Form\ExperimentType;
use App\Repository\CategoryRepository;
use App\Repository\ExperimentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Service\ChangesetGenerator;

class ExperimentController extends AbstractController
{
    /**
     * Route to a view which shows all Experiments and a Categories Sidebar.
     *
     * @return Response
     */
    public function showAll(Request $request, SessionInterface $session)
    {
        $categoryId = $request->attributes->get('categoryId');
        if ($categoryId) {
            $experiments = $this->filterBy($categoryId);
        } else {
            $experiments = [];
        }
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepository->getSortedCategoriesArray();

        return $this->render('experiment/list.html.twig', [
            'items' => $experiments,
            'type' => 'experiment',
            'categories' => $categories,
            'main_title' => "Experimente",
            'page_buttons' => [],
            'listView' => $session->get('viewOption', false),
            'categoryId' => $categoryId
        ]);
    }

    private function getCommits(Experiment $experiment)
    {
        $commits = $experiment->getCommits();
        if ($experiment->getStatus() !== 'available') {
            $this->denyAccessUnlessGranted('ROLE_SHOW_UNAVAILABLE');
        }
        $em = $this->getDoctrine();
        // The objects id's must be evaluated
        $convertIdToObject = function ($data) use ($em) {
            return $em->getRepository(Label::class)->find($data);
        };
        foreach ($commits as $commit) {
            switch ($commit->getField()) {
                case 'labels':
                    $data = $commit->getData();
                    $retained = array_intersect($data[0], $data[1]);
                    $removed = array_diff($data[0], $data[1]);
                    $added = array_diff($data[1], $data[0]);
                    $commit->setData([array_map($convertIdToObject, $retained),
                        array_map($convertIdToObject, $removed),
                        array_map($convertIdToObject, $added)]);
            }
        }
        return $commits;
    }

    /**
     * Route that handles the creation of a form for creating new experiments as well as handling the response
     * from the form.
     *
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_ADD');

        $experiment = new Experiment();
        $form = $this->createForm(ExperimentType::class, $experiment, [
            'availableCategories' => $this->getAvailableCategories()
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $experiment = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($experiment);
            $experiment_commit = new Commit();
            $experiment_commit->setExperiment($experiment);
            $experiment_commit->setField('!create');
            $experiment_commit->setTimestamp(new \DateTime('now'));
            $experiment_commit->setAuthor($this->getUser());
            $entityManager->persist($experiment_commit);
            $entityManager->flush();

            return $this->redirectToRoute('experiments');
        }

        return $this->render('form/experiment_form.html.twig', [
            'form' => $form->createView(),
            'main_title' => 'Neues Experiment erstellen',
            'page_buttons' => [],
        ]);
    }

    /**
     * Route that handles the creation of a form for editing existing experiments as well as handling the response
     * from the form.
     *
     * @param Request $request Request with the id of an experiment as 'id' parameter
     * @param ChangesetGenerator $cc
     * @return RedirectResponse|Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(Request $request, ChangesetGenerator $cc)
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_EDIT');

        $id = $request->attributes->get('id');
        $categoryId = $request->attributes->get('categoryId');

        /** @var Experiment|null $experiment */
        $experiment = $this->getDoctrine()
            ->getRepository(Experiment::class)
            ->find($id);

        if (!$experiment) {
            throw $this->createNotFoundException('Keine Experiment unter dieser Nummer!');
        }

        $old_labels = $experiment->getLabels()->toArray();

        //Document Handling is tested in Front End Test
        // @codeCoverageIgnoreStart
        $originalFiles = new ArrayCollection();
        foreach ($experiment->getDocuments() as $file) {
            $originalFiles->add($file);
        }
        // @codeCoverageIgnoreEnd

        $form = $this->createForm(ExperimentType::class, $experiment, [
            'availableCategories' => $this->getAvailableCategories(),
            'choice' => $experiment->getStatus(),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $experiment = $form->getData();
            /** @var EntityManager $entityManager */
            $entityManager = $this->getDoctrine()->getManager();

            //Document Handling is tested in Front End Test
            // @codeCoverageIgnoreStart
            foreach ($originalFiles as $file) {
                if (false === $experiment->getDocuments()->contains($file)) {
                    $file->setExperiment(null);
                    $entityManager->remove($file);
                }
            }
            // @codeCoverageIgnoreEnd

            $uow = $entityManager->getUnitOfWork();
            $uow->computeChangeSets();
            $changeset = $uow->getEntityChangeSet($experiment);
            $entityManager->persist($experiment);
            $entityManager->flush();
            //$changeset = $cc->diffDoctrineObject($entityManager, $experiment);

            // Reserve content if the field must be confirmed
            if (array_key_exists('description', $changeset)) {
                $experiment->setDescription($changeset['description'][0]);
            }
            if (array_key_exists('comment', $changeset)) {
                $experiment->setComment($changeset['comment'][0]);
            }

            $entityManager->persist($experiment);
            $entityManager->flush();


            $new_labels = $experiment->getLabels()->toArray();
            // Computes all changes and saves them in an experiment commit
            $convertToId = function ($a) {
                return $a->getId();
            };
            $label_change = [array_map($convertToId, $old_labels), array_map($convertToId, $new_labels)];
            if ($label_change[0] != $label_change[1]) {
                $changeset['labels'] = $label_change;
            }

            foreach ($changeset as $field => $value) {
                $experiment_commit = new Commit();
                $experiment_commit->setExperiment($experiment);
                $experiment_commit->setField($field);
                switch ($field) {
                    case "category":
                        $experiment_commit->setData([$value[0]->getId(), $value[1]->getId()]);
                        break;
                    default:
                        $experiment_commit->setData($value);
                }
                if ($field !== 'description' && $field !== 'comment') {
                    $experiment_commit->setConfirmedBy($this->getUser());
                }
                $experiment_commit->setTimestamp(new \DateTime('now'));
                $experiment_commit->setAuthor($this->getUser());
                $entityManager->persist($experiment_commit);
            }
            $entityManager->flush();

            return $this->redirectToRoute('experiment_view_description', [
                'id' => $id
            ]);
        }

        return $this->render('form/experiment_form.html.twig', [
            'id' => $id,
            'form' => $form->createView(),
            'categoryId' => $categoryId,
            'main_title' => 'Experiment bearbeiten',
            'page_buttons' => [],
        ]);
    }

    /**
     * Route that handles the deletion of the experiment with the matching id.
     *
     * @param Request $request Request with the id of an experiment as 'id' paramter
     * @return RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_EDIT');

        $id = $request->attributes->get('id');
        $categoryId = $request->attributes->get('categoryId');

        /** @var Experiment|null $experiment */
        $experiment = $this->getDoctrine()
            ->getRepository(Experiment::class)
            ->find($id);

        if (!$experiment) {
            throw $this->createNotFoundException('Keine Experiment unter dieser Nummer!');
        }

        $experiment->setStatus('notavailable');

        $experiment_commit = new Commit();
        $experiment_commit->setExperiment($experiment);
        $experiment_commit->setField('!delete');
        $experiment_commit->setTimestamp(new \DateTime('now'));
        $experiment_commit->setAuthor($this->getUser());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($experiment_commit);
        $entityManager->flush();

        return $this->redirectToRoute('experiment_view_description', [
            'id' => $experiment->getId()
        ]);
    }

    /**
     * Route that returns a pre rendert twig template with all requested experiments as json.
     *
     * @param Request $request
     * @return Response Json encoded already rendered twig template
     * @codeCoverageIgnore
     */
    public function filterByCategory(Request $request, SessionInterface $session)
    {
        $categoryId = $request->attributes->get('categoryId');
        $pictureView = $session->get('viewOption', false);

        $experiments = $this->filterBy($categoryId);

        if ($pictureView) {
            $template = $this->renderView('preview_cards.html.twig', [
                'type' => 'experiment',
                'items' => $experiments,
                'class' => 'experiments',
                'categoryId' => $categoryId
            ]);
        } else {
            $template = $this->renderView('list_entries.html.twig', [
                'type' => 'experiment',
                'items' => $experiments,
                'class' => 'experiments',
                'categoryId' => $categoryId
            ]);
        }

        $json = json_encode($template);
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function filterBy($categoryId)
    {
        /** @var Category */
        $currentCategorie = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($categoryId);

        $allChildren = new ArrayCollection();
        $allChildren->add($categoryId);
        $categorieChilds = $currentCategorie->getChildren();
        if ($categorieChilds->toArray()) {
            $this->getAllChildrenCategories($categorieChilds, $allChildren);
        }

        if ($this->isGranted('ROLE_SHOW_UNAVAILABLE')) {
            /** @var ExperimentRepository $experimentRepository */
            $experimentRepository = $this->getDoctrine()->getRepository(Experiment::class);
            $experiments = $experimentRepository->findForCategory($allChildren->toArray(), false);
        } else {
            /** @var ExperimentRepository $experimentRepository */
            $experimentRepository = $this->getDoctrine()->getRepository(Experiment::class);
            $experiments = $experimentRepository->findForCategory($allChildren->toArray(), true);
        }

        return $experiments;
    }

    /**
     * Returns all children categories for a list of given Categories.
     *
     * @param Collection $categorys
     * @param ArrayCollection $array
     */
    public function getAllChildrenCategories(Collection $categorys, ArrayCollection $array)
    {
        foreach ($categorys as $children) {
            if ($children->getChildren()) {
                $this->getAllChildrenCategories($children->getChildren(), $array);
            }
            $array->add($children->getId());
        }
    }

    /**
     * Returns all Categories that have no children
     *
     * @param array $categories
     * @param ArrayCollection $array
     */
    public function getAllEndCategories($categories, ArrayCollection $array)
    {
        foreach ($categories as $children) {
            if ($children->getChildren()->toArray()) {
                $this->getAllEndCategories($children->getChildren(), $array);
            } else {
                $array->add($children);
            }
        }
    }

    /**
     * Returns all Categories that are available to be chosen as a Category for Experiments
     *
     * @return ArrayCollection Contains Category Objects
     */
    public function getAvailableCategories()
    {
        $rootCategories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findBy([
                'parent' => null,
            ]);

        $allEnds = new ArrayCollection();
        $this->getAllEndCategories($rootCategories, $allEnds);

        $iterator = $allEnds->getIterator();

        $iterator->uasort(function ($a, $b) {
            return strnatcmp($a->getToken(), $b->getToken());
        });

        $allEnds = new ArrayCollection(iterator_to_array($iterator));
        return $allEnds;
    }

    public function addComment(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_COMMENT');

        $id = $request->request->get('experiment');
        $parent = $request->request->get('parent', 0);

        /** @var Experiment|null $experiment */
        $experiment = $this->getDoctrine()
            ->getRepository(Experiment::class)
            ->find($id);

        if (!$experiment) {
            throw $this->createNotFoundException('Keine Experiment unter dieser Nummer!');
        }

        /** @var Comment|null $parentComment */
        $parentComment = $this->getDoctrine()
            ->getRepository(Comment::class)
            ->find($parent);

        $comment = new Comment();
        $comment->setMessage($request->request->get('message'));
        $comment->setTimestamp(new \DateTime('now'));
        $comment->setParentComment($parentComment);
        $comment->setUser($this->getUser());
        $experiment->addComment($comment);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($comment);
        $entityManager->flush();

        $template = $this->renderView('experiment/view/comment/base.html.twig', [
            'item' => $experiment,
            'item.comments' => $experiment->getComments()
        ]);

        $json = json_encode($template);
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function deleteComment(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_COMMENT_DELETE');

        $id = $request->attributes->get('id');

        /** @var Comment|null $comment */
        $comment = $this->getDoctrine()
            ->getRepository(Comment::class)
            ->find($id);

        if (!$comment) {
            throw $this->createNotFoundException('Keine Kommentar unter dieser Nummer!');
        }

        /** @var Experiment $experiment */
        $experiment = $comment->getExperiment();
        $experimentId = $experiment->getId();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($comment);
        $entityManager->flush();

        /** @var Experiment|null $experiment */
        $experiment = $this->getDoctrine()
            ->getRepository(Experiment::class)
            ->find($experimentId);

        if (!$experiment) {
            throw $this->createNotFoundException('Keine Experiment unter dieser Nummer!');
        }

        $template = $this->renderView('experiment/view/comment/base.html.twig', [
            'item' => $experiment,
            'item.comments' => $experiment->getComments()
        ]);

        $json = json_encode($template);
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    private function getExperiment(Request $request)
    {
        $id = $request->attributes->get('id');
        return $this->getDoctrine()
            ->getRepository(Experiment::class)
            ->find($id);
    }

    private function view(Request $request, $template)
    {
        /** @var Experiment|null $experiment */
        $experiment = $this->getExperiment($request);

        if (!$experiment) {
            throw $this->createNotFoundException('Keine Experiment unter dieser Nummer!');
        }

        if ($experiment->getStatus() !== 'available') {
            $this->denyAccessUnlessGranted('ROLE_SHOW_UNAVAILABLE');
        }

        return $this->render($template, [
            'item' => $experiment
        ]);
    }

    public function viewDescription(Request $request)
    {
        return $this->view($request, 'experiment/view/description.html.twig');
    }

    public function viewDevices(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_DEVICES_SHOW');
        return $this->view($request, 'experiment/view/devices.html.twig');
    }

    public function viewComments(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_COMMENT');
        return $this->view($request, 'experiment/view/comments.html.twig');
    }

    public function viewDocuments(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_DOCUMENTS');
        return $this->view($request, 'experiment/view/documents.html.twig');
    }

    public function viewCommits(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_EDIT');
        /** @var Experiment|null $experiment */
        $experiment = $this->getExperiment($request);

        if (!$experiment) {
            throw $this->createNotFoundException('Keine Experiment unter dieser Nummer!');
        }

        return $this->render('experiment/view/commits.html.twig', [
            'item' => $experiment,
            'commits' => $this->getCommits($experiment)
        ]);
    }

    public function getPrevNext($currentID, $token, $showOnlyAvailable)
    {
        /** @var ExperimentRepository $experimentRepository */
        $experimentRepository = $this->getDoctrine()->getRepository(Experiment::class);
        $experimentQuery = $experimentRepository->findPrevNext($showOnlyAvailable);

        $max = sizeof($experimentQuery);
        $prev = $next = $experimentQuery[0]["experimentNumber"];

        for ($i = 0; $i < $max; $i++) {
            if ($experimentQuery[$i]["experimentNumber"] == $currentID && $experimentQuery[$i]["token"] === $token) {
                if ($i != 0) {
                    $prev = $experimentQuery[$i-1][0]->getId();
                }
                if ($i != $max-1) {
                    $next = $experimentQuery[$i+1][0]->getId();
                }
            }
        }
        return array(
            "prev" => $prev,
            "next"    => $next);
    }

    public function showNext(Request $request)
    {
        if ($this->isGranted('ROLE_SHOW_UNAVAILABLE')) {
            $showOnlyAvailable=true;
        } else {
            $showOnlyAvailable=false;
        }

        $id = $request->attributes->get('id');
        $token = $request->attributes->get('token');

        $next = $this->getPrevNext($id, $token, $showOnlyAvailable)["next"];

        return $this->redirectToRoute('experiment_view_description', [
            'id' => $next
        ]);
    }

    public function showPrevious(Request $request)
    {
        if ($this->isGranted('ROLE_SHOW_UNAVAILABLE')) {
            $showOnlyAvailable=false;
        } else {
            $showOnlyAvailable=true;
        }

        $id = $request->attributes->get('id');
        $token = $request->attributes->get('token');

        $prev = $this->getPrevNext($id, $token, $showOnlyAvailable)["prev"];

        return $this->redirectToRoute('experiment_view_description', [
            'id' => $prev
        ]);
    }

    public function showAllComments()
    {
        $this->denyAccessUnlessGranted('ROLE_EXPERIMENT_COMMENT_ALL');
        $comments = $this->getDoctrine()
            ->getRepository(Comment::class)
            ->findBy(
                [],
                ['timestamp' => 'DESC']
            );

        return $this->render('table_comments.html.twig', [
            'comments' => $comments
        ]);
    }
}
