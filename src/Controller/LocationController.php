<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Location;
use App\Form\LocationType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LocationController extends FormController
{
    /**
     * Route that shows all information for a given Location
     *
     * @param Request $request Request with the id of a Location as 'id' parameter
     * @return Response
     */
    public function showDetails(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LOCATION_SHOW');

        $id = $request->attributes->get('id');

        $location = $this->getDoctrine()
            ->getRepository(Location::class)
            ->find($id);

        if (!$location) {
            throw $this->createNotFoundException('Kein Standort unter dieser nummer!');
        }

        return $this->render('view.html.twig', [
            'view' => $location,
        ]);
    }

    /**
     * Route that shows all locations
     *
     * @return Response
     */
    public function showAll(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_LOCATION_SHOW');

        $locations = $this->getDoctrine()->getRepository(Location::class)->findAll();
        return $this->render('table_location.html.twig', [
            'locations' => $locations,
            'main_title' => 'Standorte',
            'page_buttons' => [['Standort hinzufügen','location_create','/icons/ic_add_location_24px.svg']],
            'buttons' => [["Bearbeiten", "location_edit"],
                ["Löschen", "location_delete"]],
        ]);
    }

    /**
     * Route that handles the creation of a form for creating new Location as well as handling the response
     * from the form.
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function create(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LOCATION_ADD');

        $location = new Location();
        $form = $this->createForm(LocationType::class, $location);
        $redirectURL = $this->generateUrl('devices');

        return $this->handleForm($request, $form, [
            'site_title' => 'Neuen Standort erstellen',
            'page_buttons' => [],
            'redirectURL' => $redirectURL,
        ]);
    }

    /**
     * Route that handles the creation of a form for editing existing Location as well as handling the response
     * from the form.
     *
     * @param Request $request Request with the id of a Location as 'id' parameter
     * @return RedirectResponse|Response
     */
    public function edit(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LOCATION_EDIT');

        $id = $request->attributes->get('id');
        $location = $this->getDoctrine()
            ->getRepository(Location::class)
            ->find($id);

        if (!$location) {
            throw $this->createNotFoundException('Kein Standort unter dieser nummer!');
        }

        $form = $this->createForm(LocationType::class, $location);
        $redirectURL = $this->generateUrl('locations');

        return $this->handleForm($request, $form, [
            'site_title' => 'Standort bearbeiten',
            'page_buttons' => [["Standortübersicht", "locations", "\icons\ic_location_24px.svg"]],
            'redirectURL' => $redirectURL,
        ]);
    }

    /**
     * Route that handles the deletion of the Location with the matching id.
     *
     * @param Request $request Request with the id of a Location as 'id' parameter
     * @return RedirectResponse
     */
    public function delete(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LOCATION_DELETE');

        $id = $request->attributes->get('id');

        $location = $this->getDoctrine()
            ->getRepository(Location::class)
            ->find($id);

        if (!$location) {
            throw $this->createNotFoundException('Kein Standort unter dieser nummer!');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($location);
        $entityManager->flush();

        return $this->redirectToRoute('locations');
    }
}
