<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{
    /**
     * Handles common form operations.
     * This renders the page with the given form if no this is not a submitted form.
     * If the form is submitted the data from the form will get persisted.
     *
     * Currently this does not support custom data modification.
     *
     * @param Request $request
     * @param FormInterface $form The Form that will get displayed
     * @param array $options Options 'site_title', 'page_buttons' and 'redirectURL' are required
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handleForm(Request $request, FormInterface $form, array $options)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            //$this->removeFiles();
            $entityManager->persist($formData);
            $entityManager->flush();

            return $this->redirect($options['redirectURL']);
        }

        return $this->render('form/default.html.twig', [
            'form' => $form->createView(),
            'main_title' => $options['site_title'],
            'page_buttons' => $options['page_buttons'],
        ]);
    }

   /* public function removeFiles()
    {
        foreach ($originalFiles as $file) {
            if (false === $experiment->getDocuments()->contains($file)) {
                $file->setExperiment(null);
                $entityManager->remove($file);
            }
        }
    }

    public function saveCurrentFiles()
    {
        $originalFiles = new ArrayCollection();

        foreach ($experiment->getDocuments() as $file) {
            $originalFiles->add($file);
        }
    }*/
}
