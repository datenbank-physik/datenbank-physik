<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Experiment;
use App\Entity\Reservation;
use App\Entity\ReservationExperiment;
use App\Entity\User;
use App\Repository\ReservationExperimentRepository;
use App\Repository\ReservationRepository;
use App\Service\Mailer;
use Psr\Container\ContainerInterface;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ReservationController extends AbstractController
{

    public function remember(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_RESERVE_BOOK');
        $id = $request->attributes->get('id');
        /** @var Experiment|null $experiment */
        $experiment = $this->getDoctrine()
            ->getRepository(Experiment::class)
            ->find($id);

        if (!$experiment) {
            throw $this->createNotFoundException('Kein Experiment unter dieser nummer!');
        }

        $reservation = new ReservationExperiment();
        $reservation->setExperiment($experiment);
        $reservation->setUser($this->getUser());
        $reservation->setStatus(0);
        $reservation->setPinnedOn(new \DateTime('now'));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($reservation);
        $entityManager->flush();

        return $this->render('welcome.html.twig', []);
    }

    // Note: Depends on JavaScript, therefor tested in Front End Testplan
    /**
     * @codeCoverageIgnore
     */
    public function book(Request $request, Swift_Mailer $mailer)
    {
        $this->denyAccessUnlessGranted('ROLE_RESERVE_BOOK');
        /** @var User $user */
        $user = $this->getUser();
        $lectureDate = new \DateTime($request->get('lectureDate'));
        $comment = $request->get('comment');
        $remembered = $this->getDoctrine()
            ->getRepository(ReservationExperiment::class)
            ->findBy(['status' => '0', 'user' => $user]);
        if ($request->get('lesson') === 'new') {
            /** @var Reservation $reservation */
            $reservation = new Reservation();
            $reservation->setUser($user);
            $reservation->setName($request->get('name'));
            if (empty($remembered)) {
                // Only register lesson
                $reservation->setStatus(0);
            } else {
                // Reserve experiment
                $reservation->setStatus(1);
            }
            $reservation->setLectureDate($lectureDate);
            $reservation->setComment($comment);
        } else {
            /** @var Reservation $booked */
            $reservation = $this->getDoctrine()
                ->getRepository(Reservation::class)
                ->find($request->get('lesson'));
            // Canceled reservations can be made active by new experiments
            $reservation->setStatus(1);
        }
        $reservation->setOrdertime(new \DateTime('now'));
        foreach ($remembered as $exp) {
            $reservation->addReservationExperiment($exp);
            $exp->setStatus(1);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($reservation);
        $entityManager->flush();
        $message = (new \Swift_Message('Neue Vorlesung "'.$reservation->getName().'"'))
            ->setFrom(['vla-db@physik.tu-darmstadt.de' => $this->getUser()->getName()])
            ->setTo([$user->getEmail(), 'vla@physik.tu-darmstadt.de' => 'A name'])
            ->setBody(
                $this->renderView(
                    'reservation/mail/book.html.twig',
                    ['name' => $user->getName(), 'reservation' => $reservation]
                ),
                'text/html'
            )
        ;
        $mailer->send($message);

        return $this->redirectToRoute('book_view_private', []);
    }

    // Note: Depends on JavaScript, therefor tested in Front End Testplan
    /**
     * @codeCoverageIgnore
     */
    public function cancel(Request $request, Swift_Mailer $mailer)
    {
        $this->denyAccessUnlessGranted('ROLE_RESERVE_BOOK');
        $id = $request->get('id');
        /** @var Reservation $reservation */
        $reservation = $this->getDoctrine()
            ->getRepository(Reservation::class)
            ->find($id);
        $reservation->setStatus(2);
        $experiments = $reservation->getReservationExperiments();
        foreach ($experiments as $exp) {
            $exp->setStatus(2);
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($reservation);
        $entityManager->flush();

        $user = $this->getUser();
        $message = (new \Swift_Message('Vorlesung "'.$reservation->getName().'" storniert'))
            ->setFrom(['vla-db@physik.tu-darmstadt.de' => $user->getName()])
            ->setTo([$user->getEmail(), 'vla@physik.tu-darmstadt.de' => 'A name'])
            ->setBody(
                $this->renderView(
                    'reservation/mail/cancel.html.twig',
                    ['name' => $user->getName(), 'reservation' => $reservation]
                ),
                'text/html'
            )
        ;
        $mailer->send($message);

        return $this->redirectToRoute('reservation_detail', ['id' => $id]);
    }

    // Note: Depends on JavaScript, therefor tested in Front End Testplan
    /**
     * @codeCoverageIgnore
     */
    public function cancelExperiment(Request $request, Swift_Mailer $mailer)
    {
        $this->denyAccessUnlessGranted('ROLE_RESERVE_BOOK');
        $id = $request->get('id');
        /** @var ReservationExperiment|null $experiment */
        $experiment = $this->getDoctrine()
            ->getRepository(ReservationExperiment::class)
            ->find($id);

        if (!$experiment) {
            throw $this->createNotFoundException('Keine Experiment unter dieser Nummer!');
        }

        $entityManager = $this->getDoctrine()->getManager();

        if ($experiment->getStatus() == 0) {
            $entityManager->remove($experiment);
            $entityManager->flush();
            return $this->redirectToRoute('book_view_private', []);
        }
        $experiment->setStatus(2);
        $entityManager->persist($experiment);
        $entityManager->flush();
        $user = $this->getUser();
        $message = (new \Swift_Message('Experiment "'.$experiment->getExperiment()->getName().'" wurde storniert'))
            ->setFrom(['vla-db@physik.tu-darmstadt.de' => $user->getName()])
            ->setTo([$user->getEmail(), 'vla@physik.tu-darmstadt.de' => 'A name'])
            ->setBody(
                $this->renderView(
                    'reservation/mail/cancel.html.twig',
                    ['name' => $user->getName(), 'reservation' => $experiment->getReservation()]
                ),
                'text/html'
            )
        ;
        $mailer->send($message);

        return $this->redirectToRoute('reservation_detail', ['id' => $experiment->getReservation()->getId()]);
    }

    // Note: Depends on JavaScript, therefor tested in Front End Testplan
    /**
     * @codeCoverageIgnore
     */
    public function bookViewPrivate(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_RESERVE_BOOK');
        $user = $this->getUser();
        /** @var ReservationExperimentRepository $repo */
        $repo = $this->getDoctrine()->getRepository(ReservationExperiment::class);
        $remembered = $repo->getReservations($user);

        /** @var Reservation $booked */
        $reservations = $this->getDoctrine()
            ->getRepository(Reservation::class)
            ->findBy(['user' => $user]);
        return $this->render('reservation/list/private.html.twig', [
            'remembered' => $remembered,
            'reservations' => $reservations,
            ]);
    }

    // Note: Depends on JavaScript, therefor tested in Front End Testplan
    /**
     * @codeCoverageIgnore
     */
    public function bookViewAll(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_RESERVE_VIEWALL');
        /** @var Reservation $booked_all_user */
        $booked_all_user = $this->getDoctrine()
            ->getRepository(Reservation::class)
            ->findBy(['status' => '1']);
        return $this->render('reservation/list/all.html.twig', ['booked_all_user' => $booked_all_user]);
    }

    // Note: Depends on JavaScript, therefor tested in Front End Testplan
    /**
     * @codeCoverageIgnore
     */
    public function bookViewStatistics(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_RESERVE_STATISTIC');
        /** @var ReservationExperimentRepository $top_reservations */
        $top_reservations = $this->getDoctrine()
            ->getRepository(ReservationExperiment::class);

        $top_reservations = $top_reservations->getTopReservations();
        if ($top_reservations && end($top_reservations)['count'] > 0) {
            $count_multiplicator = 100 / end($top_reservations)['count'];
        } else {
            $count_multiplicator = 0;
        }


        return $this->render('reservation/list/statistics.html.twig', [
            'top_reservations' => $top_reservations,
            'count_multiplicator' => $count_multiplicator
        ]);
    }

    // Note: Depends on JavaScript, therefor tested in Front End Testplan
    /**
     * @codeCoverageIgnore
     */
    public function reservationDetail(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_RESERVE_BOOK');
        $user = $this->getUser();
        $id = $request->attributes->get('id');
        /** @var Reservation $remembered */
        $reservation = $this->getDoctrine()
            ->getRepository(Reservation::class)
            ->find($id);

        if (!$reservation) {
            throw $this->createNotFoundException('Keine Reservierung unter dieser Nummer!');
        }

        return $this->render('reservation/view.html.twig', ['reservation' => $reservation]);
    }
}
