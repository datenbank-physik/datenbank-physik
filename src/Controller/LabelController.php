<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Label;
use App\Form\LabelType;
use Symfony\Component\HttpFoundation\Request;

class LabelController extends FormController
{
    /**
     * Route that shows the details for a requested Label.
     *
     * @param Request $request Request with the id of a Label as 'id' parameter
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showDetails(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LABEL_SHOW');

        $id = $request->attributes->get('id');

        $label = $this->getDoctrine()
            ->getRepository(Label::class)
            ->find($id);

        if (!$label) {
            throw $this->createNotFoundException('Kein Label unter dieser nummer!');
        }

        return $this->render('label/view.html.twig', [
            'label' => $label,
        ]);
    }

    /**
     * Route that handles the creation of a form for creating new Labels as well as handling the response
     * from the form.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LABEL_ADD');

        $label = new Label();
        $form = $this->createForm(LabelType::class, $label);
        $redirectURL = $this->generateUrl('devices');

        return $this->handleForm($request, $form, [
            'site_title' => 'Neues Label erstellen',
            'page_buttons' => [],
            'redirectURL' => $redirectURL,
        ]);
    }

    /**
     * Route that handles the creation of a form for editing existing Labels as well as handling the response
     * from the form.
     *
     * @param Request $request Request with the id of a Label as 'id' parameter
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LABEL_EDIT');

        $id = $request->attributes->get('id');

        $label = $this->getDoctrine()
            ->getRepository(Label::class)
            ->find($id);

        if (!$label) {
            throw $this->createNotFoundException('Kein Label unter dieser nummer!');
        }

        $form = $this->createForm(LabelType::class, $label);
        $redirectURL = $this->generateUrl('label_show', ['id' => $id]);

        return $this->handleForm($request, $form, [
            'site_title' => 'Label bearbeiten',
            'page_buttons' => [],
            'redirectURL' => $redirectURL,
        ]);
    }

    /**
     * Route that handles the deletion of the label with the matching id.
     *
     * @param Request $request Request with the id of a Label as 'id' parameter
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_LABEL_DELETE');

        $id = $request->attributes->get('id');

        $label = $this->getDoctrine()
            ->getRepository(Label::class)
            ->find($id);

        if (!$label) {
            throw $this->createNotFoundException('Kein Label unter dieser nummer!');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($label);
        $entityManager->flush();

        return $this->redirectToRoute('devices');
    }
}
