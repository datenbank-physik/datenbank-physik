<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Commit;
use App\Entity\Device;
use App\Entity\Label;
use App\Form\DeviceType;
use App\Repository\DeviceRepository;
use App\Service\ChangesetGenerator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class DeviceController extends AbstractController
{
    /**
     * Route that shows all Devices
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAll(SessionInterface $session)
    {
        $this->denyAccessUnlessGranted('ROLE_DEVICES_SHOW');

        /** @var DeviceRepository $deviceRepository */
        $deviceRepository = $this->getDoctrine()->getRepository(Device::class);
        $devices = $deviceRepository->findAllWithDocumentRelations();
        return $this->render('device/list.html.twig', [
            'items' => $devices,
            'type' => 'device',
            'main_title' => "Inventar",
            'listView' => $session->get('viewOption', false),
            'page_buttons' => [],
        ]);
    }

    /**
     * Route that handles the creation of a form for creating new Device as well as handling the response
     * from the form.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_DEVICES_ADD');

        $device = new Device();
        $device->setLoaned(0);
        $device->setRepair(0);
        $device->setAmount(1);
        $form = $this->createForm(DeviceType::class, $device);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $device = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($device);
            $device_commit = new Commit();
            $device_commit->setDevice($device);
            $device_commit->setField('!create');
            $device_commit->setTimestamp(new \DateTime('now'));
            $device_commit->setAuthor($this->getUser());
            $entityManager->persist($device_commit);
            $entityManager->flush();

            return $this->redirectToRoute('devices');
        }

        return $this->render('form/device_form.html.twig', [
            'form' => $form->createView(),
            'main_title' => 'Neues Gerät erstellen',
            'page_buttons' => [],
        ]);
    }

    /**
     * Route that handles the creation of a form for editing existing Devices as well as handling the response
     * from the form.
     *
     * @param Request $request Request with the id of a device as 'id' parameter
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, ChangesetGenerator $cc)
    {
        $this->denyAccessUnlessGranted('ROLE_DEVICES_EDIT');

        $id = $request->attributes->get('id');

        /** @var Device|null $device */
        $device = $this->getDoctrine()
            ->getRepository(Device::class)
            ->find($id);

        if (!$device) {
            throw $this->createNotFoundException('Keine Geräte unter dieser Nummer!');
        }

        $old_labels = $device->getLabels()->toArray();

        //Document Handling is tested in Front End Test
        // @codeCoverageIgnoreStart
        $originalFiles = new ArrayCollection();
        foreach ($device->getDocuments() as $file) {
            $originalFiles->add($file);
        }
        // @codeCoverageIgnoreEnd

        $form = $this->createForm(DeviceType::class, $device);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $device = $form->getData();
            /** @var EntityManager $entityManager */
            $entityManager = $this->getDoctrine()->getManager();

            //Document Handling is tested in Front End Test
            // @codeCoverageIgnoreStart
            foreach ($originalFiles as $file) {
                if (false === $device->getDocuments()->contains($file)) {
                    $file->setExperiment(null);
                    $entityManager->remove($file);
                }
            }
            // @codeCoverageIgnoreEnd

            $uow = $entityManager->getUnitOfWork();
            $uow->computeChangeSets();
            $changeset = $uow->getEntityChangeSet($device);
            $entityManager->persist($device);
            $entityManager->flush();
            //$changeset = $cc->diffDoctrineObject($entityManager, $device);

            // Reserve content if the field must be confirmed
            if (array_key_exists('description', $changeset)) {
                $device->setDescription($changeset['description'][0]);
            }
            if (array_key_exists('comment', $changeset)) {
                $device->setComment($changeset['comment'][0]);
            }

            $entityManager->persist($device);
            $entityManager->flush();


            $new_labels = $device->getLabels()->toArray();
            // Computes all changes and saves them in an experiment commit
            $convertToId = function ($a) {
                return $a->getId();
            };
            $label_change = [array_map($convertToId, $old_labels), array_map($convertToId, $new_labels)];
            if ($label_change[0] != $label_change[1]) {
                $changeset['labels'] = $label_change;
            }

            foreach ($changeset as $field => $value) {
                $commit = new Commit();
                $commit->setDevice($device);
                $commit->setField($field);
                switch ($field) {
                    case "category":
                        $commit->setData([$value[0]->getId(), $value[1]->getId()]);
                        break;
                    default:
                        $commit->setData($value);
                }
                if ($field !== 'description' && $field !== 'comment') {
                    $commit->setConfirmedBy($this->getUser());
                }
                $commit->setTimestamp(new \DateTime('now'));
                $commit->setAuthor($this->getUser());
                $entityManager->persist($commit);
            }
            $entityManager->flush();

            return $this->redirectToRoute('device_show', ['id' => $id]);
        }

        return $this->render('form/device_form.html.twig', [
            'id' => $id,
            'form' => $form->createView(),
            'main_title' => 'Gerät bearbeiten',
            'page_buttons' => [],
        ]);
    }

    /**
     * Route that handles the deletion of the Device with the matching id.
     *
     * @param Request $request Request with the id of a device as 'id' parameter
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_DEVICES_EDIT');

        $id = $request->attributes->get('id');

        /** @var Device|null $device */
        $device = $this->getDoctrine()
            ->getRepository(Device::class)
            ->find($id);

        if (!$device) {
            throw $this->createNotFoundException('Keine Geräte unter dieser Nummer!');
        }

        $commit = new Commit();
        $commit->setDevice($device);
        $commit->setField('!delete');
        $commit->setTimestamp(new \DateTime('now'));
        $commit->setAuthor($this->getUser());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($commit);
        $entityManager->flush();

        return $this->redirectToRoute('device_show', ['id' => $device->getId()]);
    }

    private function getCommits($device)
    {
        $em = $this->getDoctrine();

        /** @var Device|null $device */
        if ($device instanceof Device) {
            $commits = $device->getCommits();
        } else {
            die();
        }
        // The objects id's must be evaluated
        $convertIdToObject = function ($data) use ($em) {
            return $em->getRepository(Label::class)->find($data);
        };
        foreach ($commits as $commit) {
            switch ($commit->getField()) {
                case 'labels':
                    $data = $commit->getData();
                    $retained = array_intersect($data[0], $data[1]);
                    $removed = array_diff($data[0], $data[1]);
                    $added = array_diff($data[1], $data[0]);
                    $commit->setData([array_map($convertIdToObject, $retained),
                        array_map($convertIdToObject, $removed),
                        array_map($convertIdToObject, $added)]);
            }
        }
        return $commits;
    }

    private function getDevice(Request $request)
    {
        $id = $request->attributes->get('id');
        return $this->getDoctrine()
            ->getRepository(Device::class)
            ->find($id);
    }

    private function view(Request $request, $template)
    {
        $this->denyAccessUnlessGranted('ROLE_DEVICES_SHOW');
        /** @var Device|null $device */
        $device = $this->getDevice($request);

        if (!$device) {
            throw $this->createNotFoundException('Keine Geräte unter dieser Nummer!');
        }

        return $this->render($template, [
            'item' => $device
        ]);
    }

    public function viewDescription(Request $request)
    {
        return $this->view($request, 'device/view/description.html.twig');
    }

    public function viewExperiments(Request $request)
    {
        return $this->view($request, 'device/view/experiments.html.twig');
    }

    public function viewDocuments(Request $request)
    {
        return $this->view($request, 'device/view/documents.html.twig');
    }

    public function viewCommits(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_DEVICES_EDIT');
        /** @var Device|null $device */
        $device = $this->getDevice($request);

        if (!$device) {
            throw $this->createNotFoundException('Keine Geräte unter dieser Nummer!');
        }

        $commits = $this->getCommits($device);
        return $this->render('device/view/commits.html.twig', [
            'item' => $device,
            'commits' => $commits
        ]);
    }
}
