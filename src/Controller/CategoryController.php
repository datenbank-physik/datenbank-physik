<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends FormController
{
    /**
     * Route that handles the creation of a form for creating new Category as well as handling the response
     * from the form.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_CATEGORY_ADD');

        $category = new Category();
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepository->getSortedCategories();
        $form = $this->createForm(CategoryType::class, $category, [
            'allCategories' => $categories
        ]);
        $redirectURL = $this->generateUrl('experiments');

        return $this->handleForm($request, $form, [
            'site_title' => 'Neue Kategorie erstellen',
            'page_buttons' => [],
            'redirectURL' => $redirectURL,
        ]);
    }

    /**
     * Route that handles the creation of a form for editing existing Category as well as handling the response
     * from the form.
     *
     * @param Request $request Request with the id of a category as 'id' parameter
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, $error = [])
    {
        $this->denyAccessUnlessGranted('ROLE_CATEGORY_EDIT');

        $id = $request->attributes->get('id');
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $categories = $categoryRepository->getSortedCategories();
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($id);

        if (!$category) {
            throw $this->createNotFoundException('Keine Kategorie unter dieser Nummer!');
        }

        $categories = array_filter($categories, function ($value) use ($category) {
            return $value != $category;
        });
        $form = $this->createForm(CategoryType::class, $category, [
            'allCategories' => $categories
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            //$this->removeFiles();
            $entityManager->persist($formData);
            $entityManager->flush();

            return $this->redirectToRoute('experiment_showCategory', ['categoryId' => $id]);
        }

        return $this->render('form/default.html.twig', [
            'id' => $id,
            'form' => $form->createView(),
            'main_title' => 'Kategorie bearbeiten',
            'errors' => $error,
            'page_buttons' => [],
        ]);
    }

    /**
     * Route that handles the deletion of the Category with the matching id.
     *
     * @param Request $request Request with the id of a category as 'id' parameter
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_CATEGORY_DELETE');

        $id = $request->attributes->get('id');

        /** @var Category|null $category */
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($id);

        if (!$category) {
            throw $this->createNotFoundException('Keine Kategorie unter dieser Nummer!');
        }


        if (count($category->getChildren()) > 0 || count($category->getExperiments()) > 0) {
            $data = [
                'type' => 'delete_error',
                'title' => 'Löschen',
                'message' => 'Die Kategorie hat mindestens eine Unterkategorie oder enthält ein Experiment.',
            ];
            $response = $this->forward('App\Controller\CategoryController::edit', [
                'request' => $request,
                'error' => $data
            ]);
            return $response;
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($category);
        $entityManager->flush();

        return $this->redirectToRoute('experiments');
    }
}
