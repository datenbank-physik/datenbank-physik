<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Commit;
use App\Entity\Device;
use App\Entity\Experiment;
use App\Entity\Label;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

// Selects a static page to show
class CommitController extends AbstractController
{
    public function approveCommit(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ALL_EDIT');
        $id = $request->attributes->get('id');

        /** @var Commit|null $commit */
        $commit = $this->getDoctrine()
            ->getRepository(Commit::class)
            ->find($id);

        if (!$commit) {
            throw $this->createNotFoundException('Diese Änderung existiert nicht!');
        }

        $commit->setConfirmedAt(new \DateTime('now'));
        $commit->setConfirmedBy($this->getUser());
        $commit->setRejected(false);

        $type = "";
        $item = null;
        if ($commit->getExperiment()) {
            /** @var Experiment $item */
            $item = $commit->getExperiment();
            $type = "experiments";
        }
        if ($commit->getDevice()) {
            /** @var Device $item */
            $item = $commit->getDevice();
            $type = "devices";
        }
        if ($item === null) {
            return $this->redirectToRoute('index');
        }
        switch ($commit->getField()) {
            case '!delete':
                $this->denyAccessUnlessGranted('ROLE_DEVICES_DELETE');
                $entityManager = $this->getDoctrine()->getManager();
                // Nobody knows why to delete documents before
                if ($commit->getDevice()) {
                    foreach ($item->getDocuments() as $doc) {
                        $entityManager->remove($doc);
                    }
                }
                $entityManager->flush();
                $entityManager->remove($item);
                $entityManager->flush();
                return $this->redirectToRoute($type);
                break;
            case 'description':
                $item->setDescription($commit->getData()[1]);
                break;
            case 'comment':
                $item->setComment($commit->getData()[1]);
                break;
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($commit);
        $entityManager->persist($item);
        $entityManager->flush();
        if ($commit->getExperiment()) {
            /** @var Experiment $experiment */
            $experiment = $commit->getExperiment();
            /** @var Category $category */
            $category = $experiment->getCategory();
            return $this->redirectToRoute('experiment_show', [
                'id' => $experiment->getId(),
                'categoryId' => $category->getId()
            ]);
        } else {
            /** @var Device $device */
            $device = $commit->getDevice();
            return $this->redirectToRoute('device_show', ['id' => $device->getId()]);
        }
    }

    public function rejectCommit(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ALL_EDIT');

        $id = $request->attributes->get('id');

        /** @var Commit|null $commit */
        $commit = $this->getDoctrine()
            ->getRepository(Commit::class)
            ->find($id);

        if (!$commit) {
            throw $this->createNotFoundException('Diese Änderung existiert nicht!');
        }

        $commit->setConfirmedAt(new \DateTime('now'));
        $commit->setConfirmedBy($this->getUser());
        $commit->setRejected(true);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($commit);
        $entityManager->flush();
        if ($commit->getExperiment()) {
            /** @var Experiment $experiment */
            $experiment = $commit->getExperiment();
            /** @var Category $category */
            $category = $experiment->getCategory();
            return $this->redirectToRoute('experiment_show', [
                'id' => $experiment->getId(),
                'categoryId' => $category->getId()
            ]);
        } else {
            /** @var Device $device */
            $device = $commit->getDevice();
            return $this->redirectToRoute('device_show', ['id' => $device->getId()]);
        }
    }

    public function showAll(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ALL_EDIT');
        $em = $this->getDoctrine();

        $commits = $em->getRepository(Commit::class)->findAll();
        // The objects id's must be evaluated
        $convertIdToObject = function ($data) use ($em) {
            return $em->getRepository(Label::class)->find($data);
        };
        foreach ($commits as $commit) {
            switch ($commit->getField()) {
                case 'labels':
                    $data = $commit->getData();
                    $retained = array_intersect($data[0], $data[1]);
                    $removed = array_diff($data[0], $data[1]);
                    $added = array_diff($data[1], $data[0]);
                    $commit->setData([array_map($convertIdToObject, $retained),
                        array_map($convertIdToObject, $removed),
                        array_map($convertIdToObject, $added)]);
            }
        }
        return $this->render('commit/list.html.twig', ['commits' => $commits]);
    }
}
