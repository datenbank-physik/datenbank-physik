<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Commit;
use App\Entity\Device;
use App\Entity\Experiment;
use App\Entity\Label;
use App\Entity\Location;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReleaseController extends AbstractController
{
    /**
     * @throws Exception
     */
    public function showAll(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ALL_EDIT');
        $data = new ArrayCollection();
        /** @var Commit[] $commits */
        $commits = $this->getDoctrine()->getRepository(Commit::class)->findBy(['confirmed_by' => null]);
        foreach ($commits as $commit) {
            $row = [];
            if ($commit->getExperiment()) {
                /** @var Experiment $experiment */
                $experiment = $commit->getExperiment();
                /** @var Category $category */
                $category = $experiment->getCategory();
                $row = [
                    'Datum' => $commit->getTimestamp(),
                    'Typ' => 'Experiment',
                    'Feld' => $commit->getField(),
                    'ID' => $experiment->getId(),
                    'CategoryId' => $category->getId(),
                    'Info' => 'Erforderliche Freigabe bei Experiment: ' . $experiment->getName(),
                ];
            } elseif ($commit->getDevice()) {
                /** @var Device $device */
                $device = $commit->getDevice();
                $row = [
                    'Datum' => $commit->getTimestamp(),
                    'Typ' => 'Gerät',
                    'Feld' => $commit->getField(),
                    'ID' => $device->getId(),
                    'Info' => 'Erforderliche Freigabe bei Gerät: ' . $device->getName(),
                ];
            }
            $data->add($row);
        }

        $iterator = $data->getIterator();
        $iterator->uasort(function ($a, $b) {
            return $a['Datum'] <= $b['Datum'] ? 1 : -1;
        });
        $notifications = iterator_to_array($iterator);

        return $this->render('release.html.twig', [
            'notifications' => $notifications,
            'main_title' => 'Freigabe',
            'buttons' => [
                'Gerät' => [["Zeige Änderung", "device_view_commits"]],
                'Experiment' => [["Zeige Änderung", "experiment_view_commits"]],
            ]
        ]);
    }
}
