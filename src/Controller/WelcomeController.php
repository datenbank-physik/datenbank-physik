<?php
//    datenbank-physik
//    Copyright (C) 2020  Christian Seybert, Jan-Philipp Rogge, Jacob Benz, Alexander Hartmann, Sarah Schirmacher
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Device;
use App\Entity\Experiment;
use App\Entity\Label;
use App\Entity\Location;
use App\Entity\LocationMappings;
use App\Entity\User;
use App\Repository\DeviceRepository;
use App\Repository\ExperimentRepository;
use App\Repository\LocationMappingsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

// Selects a static page to show
class WelcomeController extends AbstractController
{
    public function welcome()
    {
        $limit = 5;
        /** @var ExperimentRepository $experimentRepository */
        $experimentRepository = $this->getDoctrine()->getRepository(Experiment::class);
        $experimentQuery = $experimentRepository->findLatest($limit);

        return $this->render('welcome.html.twig', [
            'items' => $experimentQuery,
        ]);
    }

    //Note: This delivers Front End code, so needs to be tested in Front End Test
    /**
     * @codeCoverageIgnore
     */
    public function bbcodePreview(Request $request)
    {
        $content = $request->get('content');
        return $this->render('form/bbcode_preview.html.twig', ['content'=>$content]);
    }

    public function version()
    {
        return $this->render('version.html.twig', []);
    }

    //Note: Changing view needs to be tested in Front End test
    /**
     * @codeCoverageIgnore
     */
    public function changeView(Request $request, SessionInterface $session)
    {
        $option = json_decode($request->getContent(), true)['listView'];
        $session->set('viewOption', $option);

        return new Response('True');
    }

    public function search(Request $request, SessionInterface $session)
    {
        $searchQuery = $request->attributes->get('query');
        $selectedQuery = $request->attributes->get('selected');

        $searchQuery = str_replace('#', '/', $searchQuery);

        $template = '';
        $doctrine = $this->getDoctrine();

        //Switching View is tested in Front End Test
        // @codeCoverageIgnoreStart

        if ($session->get('viewOption', false)) {
            $renderOption = 'preview_cards.html.twig';
        } else {
            $renderOption = 'list_entries.html.twig';
        }
        // @codeCoverageIgnoreEnd

        if (($selectedQuery === 'all' || $selectedQuery === 'devices' || $selectedQuery === 'experiments') &&
           strpos($searchQuery, 'Reparatur') === false && strpos($searchQuery, 'Ausgeliehen') === false) {
        } else {
            $deviceRepo = $doctrine->getRepository(Device::class);
            $devices = [];
            if ($selectedQuery === 'ifpcode') {
                $devices =  $deviceRepo->findBy(['ifpcode' => $searchQuery]);
            } elseif ($selectedQuery === 'inventorynumber') {
                $devices =  $deviceRepo->findBy(['inventorynumber' => $searchQuery]);
            } elseif ($selectedQuery === 'id') {
                $devices = $deviceRepo->findBy(['id' => $searchQuery]);
                //Not used anymore, but available as stub if VLA wishes feature back
                // @codeCoverageIgnoreStart
            } elseif ($selectedQuery === 'afterlocation') {
                $devices = $deviceRepo->findBy(['ifpcode' => $searchQuery]);
                // @codeCoverageIgnoreEnd
            } elseif ($selectedQuery === 'aftershelf') {
                /** @var LocationMappingsRepository $mappings */
                $mappings = $this->getDoctrine()
                    ->getRepository(LocationMappings::class);
                $resultMappings = $mappings->findByShelf('%'.$searchQuery.'%');
                foreach ($resultMappings as $mapping) {
                    $devices[] = $mapping->getDevice();
                }
            } elseif (strpos($searchQuery, 'Reparatur') !== false) {
                $devices = $deviceRepo->findBy(['repair' => 1]);
            } elseif (strpos($searchQuery, 'Ausgeliehen') !== false) {
                $devices = $deviceRepo->findBy(['loaned' => 1]);
            }

            if ($devices) {
                $template .= $this->renderView($renderOption, [
                    'type' => 'device',
                    'items' => $devices,
                    'class' => 'devices'
                ]);
            }
        }

        if ($selectedQuery === 'all' || $selectedQuery === 'experiments') {
            /** @var ExperimentRepository $experimentRepo */
            $experimentRepo = $doctrine->getRepository(Experiment::class);
            $showOnlyAvailable = true;
            if ($this->isGranted('ROLE_SHOW_UNAVAILABLE')) {
                $showOnlyAvailable = false;
            }
            $resultsExperiments = $experimentRepo->search('%'.$searchQuery.'%', $showOnlyAvailable);
            if ($resultsExperiments) {
                $template = '<h2 class="search">Experimente</h2>';
                $template .= $this->renderView($renderOption, [
                    'type' => 'experiment',
                    'items' => $resultsExperiments,
                    'class' => 'experiments'
                ]);
            }
        }

        $hasAccess = $this->isGranted('ROLE_DEVICES_SHOW');
        if ($hasAccess) {
            if (($selectedQuery === 'all' || $selectedQuery === 'devices') &&
                strpos($searchQuery, 'Reparatur') === false &&
                strpos($searchQuery, 'Ausgeliehen') === false) {
                /** @var DeviceRepository $deviceRepo */
                $deviceRepo = $doctrine->getRepository(Device::class);
                $resultsDevices = $deviceRepo->search('%'.$searchQuery.'%');
                if ($resultsDevices) {
                    $template .= '<h2 class="search">Geräte</h2>';
                    $template .= $this->renderView($renderOption, [
                        'type' => 'device',
                        'items' => $resultsDevices,
                        'class' => 'devices'
                    ]);
                }
            }
        }

        if (!$template) {
            $template = '<h2 class="search">Keine Ergebnisse</h2>';
        }

        $json = json_encode($template);
        $response = new Response($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
